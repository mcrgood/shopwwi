

var SiteUrl = "http://www.shopwwi.net";
var CdnUrl = "http://www.shopwwi.net";
require.config({
    baseUrl: CdnUrl + '/data/resource',
    paths: {
        // 插件
        'css': 'js/requirejs/plugins/css.min',
        'text': 'js/requirejs/plugins/text.min',
        'async': 'js/requirejs/plugins/async.min',
        // 插件

        'jquery': 'js/jquery.min',
        'jquery-ui': 'js/jquery-ui/jquery.ui', // 旧版
        'jquery-ui.zh-CN': 'js/jquery-ui/i18n/zh-CN',
        'jquery.datetimepicker': 'js/jquery.datetimepicker/jquery.datetimepicker.full',
        // 组件
        'layer': 'js/layer/layer'
    },
    shim: {
        'jquery-ui.zh-CN': {
            deps: ['jquery-ui']
        },
        'jquery-ui': {
            deps: ['css!js/jquery-ui/themes/ui-lightness/jquery.ui.css']
        },
        'jquery.datetimepicker': {
            deps: ['jquery', 'css!js/jquery.datetimepicker/jquery.datetimepicker.css']
        },

        'layer': {
            deps: [
                'css!js/layer/skin/layer.css'
            ]
        }
    }
});