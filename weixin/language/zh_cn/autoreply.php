<?php
defined('InByShopWWI') or exit('Access Invalid!');

$lang['ar_pls_choose_reply'] = '请选择回复素材';
$lang['ar_subreply_set_succ'] = '关注回复设置成功！';
$lang['ar_subreply_set_failed'] = '关注回复设置失败';
$lang['ar_autoreply_set_succ'] = '自动回复设置成功！';
$lang['ar_autoreply_set_failed'] = '自动回复设置失败';
$lang['ar_subject_must_write'] = '主题必须填写';
$lang['ar_keyword_must_write'] = '关键词必须填写';
$lang['ar_add_kwreply_succ'] = '新增关键词自动回复成功！';
$lang['ar_add_kwreply_failed'] = '新增关键词自动回复失败';
$lang['ar_edit_kwreply_succ'] = '编辑关键词自动回复成功！';
$lang['ar_edit_kwreply_failed'] = '编辑关键词自动回复失败';
$lang['ar_del_kwreply_succ'] = '删除关键词自动回复成功！';
$lang['ar_del_kwreply_failed'] = '删除关键词自动回复失败';
$lang['ar_type_must_choose'] = '类型必须选择';
$lang['ar_add_reply_succ'] = '新增回复素材成功！';
$lang['ar_add_reply_failed'] = '新增回复素材失败';
$lang['ar_edit_reply_succ'] = '编辑回复素材成功！';
$lang['ar_edit_reply_failed'] = '编辑回复素材失败';
$lang['ar_del_reply_succ'] = '删除回复素材成功！';
$lang['ar_del_reply_failed'] = '删除回复素材失败';

$lang['ar_sub_autoreply'] = '关注自动回复';
$lang['ar_msg_autoreply'] = '消息自动回复';
$lang['ar_kw_autoreply'] = '关键词自动回复';
$lang['ar_reply'] = '素材';
$lang['ar_pls_choose_reply_-'] = '-请选择回复素材-';
$lang['ar_subject'] = '主题';
$lang['ar_rule_name'] = '规则名称';
$lang['ar_keyword'] = '关键词';
$lang['ar_addtime'] = '添加时间';
$lang['ar_del_kwautoreply_confirm'] = '确定要删除该关键词自动回复吗？';
$lang['ar_kw_autoreply_add'] = '添加关键词自动回复';
$lang['ar_kw_autoreply_edit'] = '编辑关键词自动回复';
$lang['ar_name'] = '名称';
$lang['ar_name_must_write'] = '名称必须填写';
$lang['ar_reply_must_choose'] = '素材必须选择';
$lang['ar_reply_manage'] = '回复素材管理';
$lang['ar_type'] = '类型';
$lang['ar_no_record'] = '暂无相关记录';
$lang['ar_reply_del_confirm'] = '确定要删除该回复素材吗？';
$lang['ar_pure_text'] = '纯文字';
$lang['ar_image_text'] = '图文';
$lang['ar_reply_add'] = '添加回复素材';
$lang['ar_reply_edit'] = '编辑回复素材';
$lang['ar_intro'] = '说明';
$lang['ar_response'] = '回复';
$lang['ar_add_group'] = '增加一组';
$lang['ar_title'] = '标题';
$lang['ar_desc'] = '描述';
$lang['ar_image'] = '图片';
$lang['ar_url'] = '链接';
$lang['ar_di'] = '第';
$lang['ar_zu'] = '组';