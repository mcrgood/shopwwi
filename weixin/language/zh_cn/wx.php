<?php
defined('InByShopWWI') or exit('Access Invalid!');

$lang['wx_account_name_must_write'] = '公众账号名称必须填写';
$lang['wx_ori_id_must_write'] = '原始ID必须填写';
$lang['wx_account_must_write'] = '微信号必须填写';
$lang['wx_token_must_write'] = 'TOKEN必须填写';
$lang['wx_appid_must_write'] = 'APPID必须填写';
$lang['wx_appsecret_must_write'] = 'APPSECRET必须填写';
$lang['wx_account_add_succ'] = '公众账号添加成功！';
$lang['wx_account_add_failed'] = '公众账号添加失败';
$lang['wx_account_edit_succ'] = '公众账号编辑成功！';
$lang['wx_account_edit_failed'] = '公众账号编辑失败';
$lang['wx_account_del_succ'] = '公众账号删除成功！';
$lang['wx_account_del_failed'] = '公众账号删除失败';

$lang['wx_account_list'] = '公众账号列表';
$lang['wx_account_add'] = '添加公众账号';
$lang['wx_account_edit'] = '编辑公众账号';
$lang['wx_account_name'] = '公众账号名称';
$lang['wx_name'] = '名称';
$lang['wx_ori_id'] = '原始ID';
$lang['wx_account'] = '微信号';
$lang['wx_login_email'] = '登陆邮箱';
$lang['wx_province'] = '省份';
$lang['wx_city'] = '城市';
$lang['wx_no_record'] = '暂无相关记录';
$lang['wx_function_manage'] = '功能管理';
$lang['wx_account_del_confirm'] = '确认删除该公众账号信息吗？';