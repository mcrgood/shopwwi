<?php
defined('InByShopWWI') or exit('Access Invalid!');

$lang['menu_far_request_failed'] = '远程请求失败';
$lang['menu_publish_succ'] = '自定义菜单发布成功！';
$lang['menu_publish_failed'] = '自定义菜单发布失败';
$lang['menu_cannot_get_menu_info'] = '未能获取自定义菜单信息';
$lang['menu_subject_must_write'] = '主题必须填写';
$lang['menu_type_must_choose'] = '类型必须选择';
$lang['menu_code_must_write'] = '标识码必须填写';
$lang['menu_click_reply_add_succ'] = '新增点击事件回复成功！';
$lang['menu_click_reply_add_failed'] = '新增点击事件回复失败';
$lang['menu_click_reply_edit_succ'] = '编辑点击事件回复成功！';
$lang['menu_click_reply_edit_failed'] = '编辑点击事件回复失败';
$lang['menu_click_reply_del_succ'] = '删除点击事件回复成功！';
$lang['menu_click_reply_del_failed'] = '删除点击事件回复失败';
$lang['menu_manage'] = '自定义菜单管理';
$lang['menu_click_reply_manage'] = '点击事件回复管理';
$lang['menu_di'] = '第';
$lang['menu_one'] = '一';
$lang['menu_two'] = '二';
$lang['menu_three'] = '三';
$lang['menu_button'] = '按钮';
$lang['menu_title'] = '标题';
$lang['menu_type'] = '类型';
$lang['menu_include_child_menu'] = '包含子菜单';
$lang['menu_click_event'] = '点击事件';
$lang['menu_website_browse'] = '网页浏览';
$lang['menu_click_reply'] = '点击回复';
$lang['menu_child_menu'] = '子菜单';
$lang['menu_publish'] = '发布';
$lang['menu_del_succ'] = '自定义菜单删除成功！';
$lang['menu_del_failed'] = '自定义菜单删除失败';
$lang['menu_subject'] = '主题';
$lang['menu_add_time'] = '添加时间';
$lang['menu_pure_text'] = '纯文字';
$lang['menu_image_text'] = '图文';
$lang['menu_click_reply_del_confirm'] ='确定要删除该点击事件回复吗？';
$lang['menu_no_record'] ='暂无相关记录';
$lang['menu_click_reply_add'] ='添加点击事件回复';
$lang['menu_click_reply_edit'] ='编辑点击事件回复';
$lang['menu_click_reply_list'] ='点击事件回复列表';
$lang['menu_code'] ='标识码';
$lang['menu_intro'] ='说明';
$lang['menu_add_one_group'] ='增加一组';
$lang['menu_reply'] ='回复';
$lang['menu_zu'] ='组';
$lang['menu_desc'] ='描述';
$lang['menu_pic'] ='图片';
$lang['menu_url'] ='链接';