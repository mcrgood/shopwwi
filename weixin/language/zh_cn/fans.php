<?php
defined('InByShopWWI') or exit('Access Invalid!');

$lang['fans_far_request_failed'] = '远程请求失败';
$lang['fans_pls_choose_group'] = '请选择分组';

$lang['fans_fans_manage'] = '粉丝管理';
$lang['fans_group_manage'] = '分组管理';
$lang['fans_nickname'] = '昵称';
$lang['fans_sex'] = '性别';
$lang['fans_area'] = '地区';
$lang['fans_sub_time'] = '关注时间';
$lang['fans_male'] = '男';
$lang['fans_female'] = '女';
$lang['fans_send_msg'] = '发信息';
$lang['fans_change_group'] = '修改分组';
$lang['fans_no_record'] = '暂无相关记录';
$lang['fans_now_updateing_pls_wait'] = '正在更新请稍后...';
$lang['fans_update_fans_list'] = '更新粉丝列表';
$lang['fans_update_fans_list_failed'] = '粉丝列表更新失败';
$lang['fans_group_name'] = '分组名称';
$lang['fans_num'] = '粉丝数';
$lang['fans_change'] = '更改';
$lang['fans_get_group_info_failed'] = '获取分组信息失败';
$lang['fans_group_add'] = '新增分组';
$lang['fans_group_add_succ'] = '新增分组成功！';
$lang['fans_group_add_failed'] = '新增分组失败';
$lang['fans_group_name_edit_succ'] = '编辑分组名称成功！';
$lang['fans_group_name_edit_failed'] = '编辑分组名称失败';
$lang['fans_saving'] = '保存中...';
$lang['fans_group_edit'] = '修改分组';
$lang['fans_group_setting'] = '分组设置';
$lang['fans_pls_choose_group_-'] = '-请选择分组-';
$lang['fans_change_fans_group_failed'] = '修改粉丝分组失败';
$lang['fans_msg_content'] = '消息内容';
$lang['fans_send'] = '发送';
$lang['fans_send_content_cannot_be_null'] = '发送的消息不能为空';
$lang['fans_send_msg_succ'] = '消息发送成功！';
$lang['fans_send_msg_failed'] = '消息发送失败';