<?php
defined('InByShopWWI') or exit('Access Invalid!');

//活动
$lang['nc_weixin_wall_activity'] 			= '微信墙活动';
$lang['nc_weixin_wall_activity_name'] 		= '活动名称';
$lang['nc_weixin_wall_activity_template'] 	= '活动模板';
$lang['nc_weixin_wall_activity_content']	= '活动内容';
$lang['nc_weixin_wall_add_time']			= '发布时间';
$lang['nc_weixin_wall_activity_name_is_not_null']	= '活动名称不能为空';
$lang['nc_weixin_wall_activity_template_is_not_null']	= '活动模板不能为空';
$lang['nc_weixin_wall_activity_template_format_is_wrong']	= '活动格式不正确';
$lang['nc_weixin_wall_activity_is_not_exists']	= '活动不存在';
$lang['nc_weixin_wall_add_activity_succ']	= '添加活动成功';
$lang['nc_weixin_wall_add_activity_fail']	= '添加活动失败';
$lang['nc_weixin_wall_edit_activity_succ']	= '编辑活动成功';
$lang['nc_weixin_wall_edit_activity_fail']	= '编辑活动失败';
$lang['nc_weixin_wall_delete_activity_succ']= '删除活动成功';
$lang['nc_weixin_wall_delete_activity_fail']= '删除活动失败';
$lang['nc_weixin_wall_prize_is_exists']		= '没有中奖人';
$lang['nc_weixin_wall_lottery_fail']		= '抽奖失败';
$lang['nc_weixin_wall_activity_green']		= '清新绿色';
$lang['nc_weixin_wall_activity_pink']		= '粉红佳人';
$lang['nc_weixin_wall_activity_black']		= '经典黑色';
$lang['nc_weixin_wall_activity_blue']		= '简约蓝色';
$lang['nc_weixin_wall_activity_orange']		= '炫彩橙色';
$lang['nc_weixin_wall_activity_red']		= '喜庆红色';
$lang['nc_weixin_wall_activity_prize']		= '抽奖';
$lang['nc_weixin_wall_activity_joinin_succ']= '谢谢您参与本次活动';
$lang['nc_weixin_wall_activity_rank']		= '排行榜';
$lang['nc_weixin_confirm_delete_item']		= '确定删除该选项?';


//消息
$lang['nc_weixin_wall_message_is_not_exists']	= '消息不存在';
$lang['nc_weixin_wall_message_audit_succ']		= '审核成功';
$lang['nc_weixin_wall_message_audit_fail']		= '审核失败';
$lang['nc_weixin_wall_message_delete_succ']		= '删除成功';
$lang['nc_weixin_wall_message_delete_fail']		= '删除失败';
$lang['nc_weixin_wall_message_view_message']	= '查看消息';
$lang['nc_weixin_wall_message_view_prize']		= '查看获奖者';
$lang['nc_weixin_wall_message_nick_name']		= '昵称';
$lang['nc_weixin_wall_message_account_pic']		= '头像';
$lang['nc_weixin_wall_message_message']			= '消息内容';
$lang['nc_weixin_wall_message_state']			= '消息状态';
$lang['nc_weixin_wall_message_time']			= '消息时间';
$lang['nc_weixin_wall_message_audit']			= '审核';
$lang['nc_weixin_wall_message_wait_audit']		= '待审核';
$lang['nc_weixin_wall_message_yes_audit']		= '审核通过';
$lang['nc_weixin_wall_message_no_audit']		= '审核不通过';
$lang['nc_weixin_wall_message_audit_succ']		= '审核成功';
$lang['nc_weixin_wall_message_audit_fail']		= '审核失败';



//投票
$lang['nc_weixin_wall_vote_activity_is_not_exists']	= '投票活动不存在';
$lang['nc_weixin_wall_vote_publish_succ']	= '发布投票成功';
$lang['nc_weixin_wall_vote_publish_fail']	= '发布投票失败';
$lang['nc_weixin_wall_vote_edit_succ']		= '编辑投票成功';
$lang['nc_weixin_wall_vote_edit_fail']		= '编辑投票失败';
$lang['nc_weixin_wall_vote_delete_succ']	= '删除投票成功';
$lang['nc_weixin_wall_vote_delete_fail']	= '删除投票失败';
$lang['nc_weixin_wall_vote_is_not_exists']	= '投票不存在';
$lang['nc_weixin_wall_vote_name']			= '投票名称';
$lang['nc_weixin_wall_vote_template']		= '投票模板';
$lang['nc_weixin_wall_vote_item_name']		= '投票选项';
$lang['nc_weixin_wall_vote_content']		= '投票描述';
$lang['nc_weixin_wall_vote_name_is_not_null'] = '投票名称不能为空';
$lang['nc_weixin_wall_vote_activity_name']	= '活动名称';
$lang['nc_weixin_wall_vote_activity_name_is_not_null']	= '活动名称不能为空';
$lang['nc_weixin_wall_vote_show']			= '显示';
$lang['nc_weixin_wall_vote_item_view_add']	= '查看添加投票选项';
$lang['nc_weixin_wall_vote_item_name']		= '标识';
$lang['nc_weixin_wall_vote_item_color']		= '选项条颜色';
$lang['nc_weixin_wall_vote_item_content']	= '内容';
$lang['nc_weixin_wall_vote_item_num']		= '投票数';
$lang['nc_weixin_wall_vote_item_name_is_not_null']		= '标识不能为空';
$lang['nc_weixin_wall_vote_item_content_is_not_null']	= '内容不能为空';
$lang['nc_weixin_wall_vote_item_add_succ']	= '添加选项成功';
$lang['nc_weixin_wall_vote_item_add_fail']	= '添加选项失败';
$lang['nc_weixin_wall_vote_item_edit_succ']	= '编辑选项成功';
$lang['nc_weixin_wall_vote_item_edit_fail']	= '编辑选项失败';
$lang['nc_weixin_wall_vote_item_delete_succ']	= '删除选项成功';
$lang['nc_weixin_wall_vote_item_delete_fail']	= '删除选项失败';
$lang['nc_weixin_wall_vote_item_joinin_succ']	= '谢谢参与本次投票';
$lang['nc_weixin_wall_vote_item_valid']		= '请填写一位标识作为选项';
$lang['nc_weixin_wall_vote']				= '微信墙投票';

//抽奖
$lang['nc_weixin_wall_lottery']				= '微信墙抽奖';
$lang['nc_weixin_wall_activity_name']		= '活动名称';
$lang['nc_weixin_wall_activity_user_name']	= '中奖人';
$lang['nc_weixin_wall_activity_lottery_time'] = '抽奖时间';
$lang['nc_weixin_wall_activity_publish_time'] = '发布时间';
$lang['nc_weixin_wall_activity_vote_item_is_null'] = '该投票活动无人投票';


//墙信息
$lang['nc_weixin_wall_show_title']	= '微信上墙|微信墙|微信大屏幕';
$lang['nc_weixin_wall_show_account']= '添加微信公众帐号';
$lang['nc_weixin_wall_show_send']	= '发送';
$lang['nc_weixin_wall_your_talk']	= '您想说的话，即可上墙！';
$lang['nc_weixin_wall_show_prize']	= '获奖者';
$lang['nc_weixin_wall_show_home']	= '首页';
$lang['nc_weixin_wall_show_rank']	= '排行';
$lang['nc_weixin_wall_show_lottery']= '抽奖';
$lang['nc_weixin_wall_show_style']	= '风格';
$lang['nc_weixin_wall_show_member_rank']	= '微信上墙排行';
$lang['nc_weixin_wall_your_vote']	= '选项，即可进行投票';
$lang['nc_weixin_wall_lottery_area']= '摇奖区';
$lang['nc_weixin_wall_join_in_lottery_person']= '参与抽奖人数';
$lang['nc_weixin_wall_show_person'] = '人';
$lang['nc_weixin_wall_show_start_lottery'] = '开始摇奖';
$lang['nc_weixin_wall_show_winning_menu']  = '获奖名单';
$lang['nc_weixin_wall_show_winning_num']  = '获奖人数s';
