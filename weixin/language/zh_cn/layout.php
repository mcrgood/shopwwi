<?php
defined('InByShopWWI') or exit('Access Invalid!');
/**
 * 页面文字
 */
$lang['nc_admincp']				= '管理中心'; 
$lang['nc_loca']				= '您的位置'; 
$lang['nc_skin_peeler']			= '换肤';
$lang['nc_default_style']		= '默认风格';
$lang['nc_mac_style']			= 'Mac风格';
$lang['nc_sitemap']				= '管理地图';
$lang['nc_admin_navigation']	= '管理中心导航';
$lang['nc_update_cache_succ']	= '商城缓存更新成功';
$lang['nc_update_cache_again']	= '请重新刷新页面进行缓存更新';
$lang['nc_hello']				= '您好';
$lang['nc_logout']				= '安全退出';
$lang['nc_homepage']			= '商城首页';
$lang['nc_updatestat_qk']		= '更新信用统计';
$lang['nc_modifypw']			= '修改密码';
$lang['nc_update_cache']		= '更新站点缓存';
$lang['nc_favorite']			= '收藏';
$lang['nc_to_favorite']			= '请按 Ctrl+D 键添加到收藏夹';
$lang['nc_backup_db']			= '备份恢复数据';
$lang['nc_shopwwi_message']		= 'ShopWWI系统消息';
$lang['nc_auto_redirect']		= '若不选择将自动跳转';
$lang['nc_assign_right']		= '<br>您不具备进行该操作的权限<br><br>';
$lang['cur_location']			= '当前位置';
$lang['nc_sysoperationlog']		= '系统操作日志';

/**
 * 导航文字
 */
$lang['nc_console']				= '控制台';
$lang['nc_normal_handle']		= '常用操作';
$lang['nc_welcome_page']		= '欢迎页面';
$lang['nc_aboutus']				= '关于我们';
$lang['nc_config']				= '设置';
$lang['nc_web_set']				= '站点设置';
$lang['nc_web_account_syn']		= '账号同步';
$lang['nc_upload_set']			= '上传设置';
$lang['nc_seo_set']				= 'SEO设置';
$lang['nc_seo_url_rewrite']		= '伪静态';
$lang['nc_message_set']			= '消息通知';
$lang['nc_area_config']			= '查看地区';
$lang['nc_pay_method']			= '支付方式';

$lang['nc_logisticstools']		= '物流工具';
$lang['nc_shipping_address']	= '发货地址库';
$lang['nc_operation_reason']	= '预设操作原因';


$lang['nc_goods']				= '商品';
$lang['nc_class_manage']		= '分类管理';
$lang['nc_brand_manage']		= '品牌管理';
$lang['nc_goods_manage']		= '商品管理';
$lang['nc_type_manage']			= '类型管理';
$lang['nc_spec_manage']			= '规格管理';
$lang['nc_album_manage']		= '图片空间';
$lang['nc_brand_class']			= '品牌分类';
$lang['nc_goods_consult']		= '商品咨询';
$lang['nc_images_manage']		= '图片管理';

$lang['nc_store_set']			= '基本设置';
$lang['nc_store_customer_service']		= '在线客服';
$lang['nc_member']				= '会员';
$lang['nc_member_manage']		= '会员管理';
$lang['nc_member_feedback']		= '会员留言';
$lang['nc_limit_manage']		= '权限设置';
$lang['nc_admin_express_set']	= '快递公司';
$lang['nc_admin_perform_opt']	= '性能优化';
$lang['nc_admin_clear_cache']	= '清理缓存';
$lang['nc_admin_log']			= '操作日志';
$lang['nc_admin_res_position']	= '推荐位';
$lang['nc_member_notice']		= '会员通知';
$lang['nc_member_predepositmanage']		= '预存款';
$lang['nc_member_predepositlog']		= '预存款明细';
$lang['nc_trade']				= '交易';
$lang['nc_order_manage']		= '订单管理';
$lang['nc_order_view']			= '订单查看';
$lang['nc_order_receivedpay']			= '订单收款';


$lang['nc_order_refundmanage']		= '退款管理';
$lang['nc_order_refundaudit']		= '退款审核';
$lang['nc_order_refundpay']			= '退款支付';
$lang['nc_order_refundreceived']	= '退货收货';


$lang['nc_order_deliverymanage']= '发货管理';
$lang['nc_website']				= '网站';
$lang['nc_website_setting']		= '功能模块';
$lang['nc_article_class']		= '文章分类';
$lang['nc_article_manage']		= '文章管理';
$lang['nc_navigation']			= '页面导航';
$lang['nc_db']					= '数据库管理';
$lang['nc_updatestat']			= '更新统计';
$lang['nc_consult_manage']		= '咨询管理';
$lang['nc_goods_evaluate']		= '评价管理';
$lang['nc_goods_evaluatestat']		= '评价统计';
$lang['nc_adv_manage']			= '广告管理';
$lang['nc_share_link']			= '分享链接';
$lang['nc_tool']				= '工具';
$lang['nc_operation_set'] 		= '基本设置';
$lang['nc_flea_index_class_manage'] = '首页分类管理';
$lang['nc_flea_seo_manage'] 	= 'SEO设置';
$lang['nc_flea_class_manage'] 	= '分类管理';
$lang['nc_flea_area_manage'] 	= '地区管理';
$lang['nc_mobile_adset'] 		= '广告设置';
$lang['nc_mobile_catepic'] 		= '分类图片设置';
$lang['nc_mobile_feedback'] 	= '意见反馈';
$lang['nc_mobile_update_cache'] = '更新缓存';
$lang['nc_web_index']			= '首页管理';
$lang['nc_product_recd']		= '商品推荐类型';
$lang['nc_microshop_comment_manage']	= '评论管理';
$lang['nc_microshop_adv_manage']		= '广告管理';
$lang['nc_cms_navigation_manage']		= '导航管理';
$lang['nc_cms_index_manage']			= '首页管理';
$lang['nc_cms_tag_manage']				= '标签管理';
$lang['nc_cms_article_manage']			= '文章管理';
$lang['nc_cms_article_class']			= '文章分类';
$lang['nc_cms_comment_manage']			= '评论管理';

/**
 * 页面中的常用文字
 */
$lang['nc_class']			= '分类';
$lang['nc_list']			= '列表';
$lang['nc_add_sub_class']	= '新增下级';
$lang['nc_set']				= '设置';
$lang['nc_recommendopen']	= '开启推荐';
$lang['nc_recommendclose']	= '取消推荐';
$lang['nc_applylog']		= '申请日志';
?>
