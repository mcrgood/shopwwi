<?php
defined('InByShopWWI') or exit('Access Invalid!');

$lang['nc_site_setting'] = '站点设置';
$lang['nc_site_name']	 = '站点名称';
$lang['nc_site_keyword'] = '站点关键字';
$lang['nc_site_url']	 = '商城网址';	
$lang['nc_site_content'] = '站点描述';
$lang['nc_site_name_is_not_null']	 = '站点名称不能为空';
$lang['nc_site_keyword_is_not_null'] = '站点关键字不能为空';
$lang['nc_site_url_is_not_null'] = '商城网址不能为空';
$lang['nc_site_url_is_not_format'] = '商城地址格式不正确';

$lang['nc_basic_article']	= '系统文章';
$lang['nc_basic_title']	= '标题';
$lang['nc_basic_publish_time'] = '发布时间';
$lang['nc_basic_content']	= '内容';

$lang['nc_mall_news']		= '商城动态';
$lang['nc_article_title']	= '文章标题';
$lang['nc_article_publish_time'] = '发布时间';
$lang['nc_article_content'] = '文章内容';
$lang['nc_article_title_is_not_null'] = '文章标题不能为空';

$lang['nc_adv_manage']	= '广告管理';
$lang['nc_adv_name']	= '广告名称';
$lang['nc_adv_sort']	= '广告排序';	
$lang['nc_adv_state']	= '广告显示';
$lang['nc_adv_pic']		= '广告图片';
$lang['nc_adv_open']	= '开启';
$lang['nc_adv_close']	= '关闭';
$lang['nc_adv_name_is_not_null'] = '广告名称不能为空';
$lang['nc_adv_sort_is_not_null'] = '广告排序不能为空';
$lang['nc_adv_sort_is_not_number'] = '广告排序必须是数字';
$lang['nc_adv_sort_range'] = '广告排序数字在0-100';
$lang['nc_adv_pic_is_not_null'] = '图片不能为空';
$lang['nc_adv_pic_is_not_format'] = '图片格式不正确';