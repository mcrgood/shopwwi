<?php
$env_items = array();
$dirfile_items = array(
		array('type' => 'dir', 'path' => 'data/cache'),
		array('type' => 'dir', 'path' => 'data/cache/adv'),
		array('type' => 'dir', 'path' => 'data/cache/fields'),
		array('type' => 'dir', 'path' => 'data/cache/index'),
		array('type' => 'dir', 'path' => 'data/cache/rec_position'),
		array('type' => 'file', 'path' => 'data/resource/clickswf/adv_click.xml'),
		array('type' => 'dir', 'path' => 'data/resource/phpqrcode/temp'),
		array('type' => 'dir', 'path' => 'data/session'),
		array('type' => 'dir', 'path' => 'data/upload'),
		array('type' => 'dir', 'path' => 'data/upload/adv'),
		array('type' => 'dir', 'path' => 'data/upload/article'),
		array('type' => 'dir', 'path' => 'data/upload/avatar'),
		array('type' => 'dir', 'path' => 'data/upload/brand'),
		array('type' => 'dir', 'path' => 'data/upload/common'),
		array('type' => 'dir', 'path' => 'data/upload/editor'),
		array('type' => 'dir', 'path' => 'data/upload/goods'),
		array('type' => 'dir', 'path' => 'data/upload/login'),
		array('type' => 'dir', 'path' => 'data/upload/rec_position'),
		array('type' => 'dir', 'path' => 'data/upload/refund'),
		array('type' => 'dir', 'path' => 'data/upload/spec'),
		array('type' => 'dir', 'path' => 'data/upload/watermark'),
		array('type' => 'dir', 'path' => 'install'),
		array('type' => 'dir', 'path' => 'sql_back'),
);

$func_items = array(
		array('name' => 'mysql_connect'),
		array('name' => 'fsockopen'),
		array('name' => 'gethostbyname'),
		array('name' => 'file_get_contents'),
		array('name' => 'mb_convert_encoding'),
		array('name' => 'json_encode'),
);