<?php defined('InByShopWWI') or exit('Access Invalid!');?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="keywords" content="<?php echo $output['site']['seo_keyword'];?>" />
<meta name="description" content="<?php echo $output['site']['seo_content'];?>" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
<meta name="full-screen" content="yes">
<meta name="format-detection" content="telephone=no">
<meta name="format-detection" content="address=no">
<meta name="apple-mobile-web-app-status-bar-style" content="black" />
<meta name="apple-mobile-web-app-capable" content="yes">
<title><?php echo $output['site']['site_name'];?></title>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.mobile.js"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/swipe.js"></script>
<?php if($_GET['op']!='index'){?>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/list.js"></script>
<?php }?>
<script type="text/javascript">
	var SITE_URL = '<?php echo SHOP_SITE_URL;?>';
	var wx_id = '<?php echo $output['site']['wx_id'];?>';
	$(function (){
		//点击快速导航
	      $(".quick-nav").click(function (){
	        if($(".quick_nav_pannel").is(":hidden")){
	          $(".quick_nav_pannel").show();
	        }else {
	          $(".quick_nav_pannel").hide();
	        }
	      });
	 });
</script>
<link href="<?php echo TEMPLATES_URL; ?>/css/mall.css" rel="stylesheet">
<link href="<?php echo TEMPLATES_URL; ?>/css/main.css" rel="stylesheet">
</head>
<body>
<?php
	require_once($tpl_file);
?>
</body>
</html>
