<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title><?php echo $lang['nc_weixin_wall_show_title'];?></title>
<link href="<?php echo TEMPLATES_URL; ?>/css/wall_<?php if(isset($_GET['style'])){ echo trim($_GET['style']);}else{ echo 'brown';}?>.css" rel="stylesheet" class="theme">
<link href="<?php echo TEMPLATES_URL; ?>/css/wall_<?php echo $output['vote']['vote_template_id'];?>.css" rel="stylesheet">
<link href="<?php echo TEMPLATES_URL; ?>/css/default_wall.css" rel="stylesheet">
<link href="<?php echo TEMPLATES_URL; ?>/css/style_vote.css" rel="stylesheet">
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.js"></script>
</head>
<body>
	<?php require_once($tpl_file);?>
</body>
</html>
