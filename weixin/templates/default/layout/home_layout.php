<?php defined('InByShopWWI') or exit('Access Invalid!');?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>ShopWWI微信公众账号管理系统</title>
<meta name="author" content="ShopWWI">
<meta name="copyright" content="ShopWWI Inc. All Rights Reserved">
<link rel="Shortcut Icon" href="https://res.wx.qq.com/mpres/htmledition/images/favicon195d31.ico">
<link href="<?php echo TEMPLATES_URL; ?>/css/base199134.css" type="text/css" rel="stylesheet">
<link href="<?php echo TEMPLATES_URL; ?>/css/lib195d33.css" type="text/css" rel="stylesheet">
<link href="<?php echo TEMPLATES_URL; ?>/css/page_index195d33.css" rel="stylesheet">
<link href="<?php echo TEMPLATES_URL; ?>/css/layout.css" rel="stylesheet">
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.js"></script>
<style>
.menu .menu_title{ cursor:default;}
</style>
</head>
<body>
<div class="nc_head">
  <div class="inner">
    <div class="logo"> <a title="ShopWWI微信公众账号管理系统" href="<?php echo SHOP_SITE_URL; ?>"></a> </div>
    <div class="user-entry"> <span><a class="nickname" href="index.php?act=dashboard&wx_id=<?php echo $output['wx_info']['wx_id']; ?>"><?php echo $output['wx_info']['wx_name']; ?></a></span> <span class="line">|</span> <span><a id="logout" href="index.php?act=login&op=logout">退出</a> </span> </div>
  </div>
</div>
<div class="nc_layout">
  <div class="nc_warp side_left">
    <div class="nc_side">
      <div class="nc_side_menu">
        <dl id="nc_menu_manage" class="menu no_line">
          <dt class="menu_title"> <i class="icon1"></i>基本管理</dt>
          <dd class="item <?php if($_GET['act'] == 'msg'){ ?>selected<?php } ?>"> <a href="index.php?act=msg&op=msg_manage&wx_id=<?php echo intval($_GET['wx_id']); ?>">消息管理</a> </dd>
          <dd class="item <?php if($_GET['act'] == 'menu' && $_GET['op'] == 'menu_manage'){ ?>selected<?php } ?>"> <a href="index.php?act=menu&op=menu_manage&wx_id=<?php echo intval($_GET['wx_id']); ?>">自定义菜单管理</a> </dd>
          <dd class="item <?php if($_GET['act'] == 'menu' && ($_GET['op'] == 'click_reply_manage' || $_GET['op'] == 'click_reply_add' || $_GET['op'] == 'click_reply_edit')){ ?>selected<?php } ?>"> <a href="index.php?act=menu&op=click_reply_manage&wx_id=<?php echo intval($_GET['wx_id']); ?>">点击事件回复管理</a> </dd>
        </dl>
        <dl id="nc_menu_reply" class="menu">
          <dt class="menu_title"> <i class="icon2"></i>自动回复</dt>
          <dd class="item <?php if($_GET['act'] == 'autoreply' && $_GET['op'] == 'sub_reply'){ ?>selected<?php } ?>"> <a href="index.php?act=autoreply&op=sub_reply&wx_id=<?php echo intval($_GET['wx_id']); ?>">关注自动回复</a> </dd>
          <dd class="item <?php if($_GET['act'] == 'autoreply' && $_GET['op'] == 'msg_reply'){ ?>selected<?php } ?>"> <a href="index.php?act=autoreply&op=msg_reply&wx_id=<?php echo intval($_GET['wx_id']); ?>">消息自动回复</a> </dd>
          <dd class="item <?php if($_GET['act'] == 'autoreply' && ($_GET['op'] == 'keyword_reply' || $_GET['op'] == 'keyword_reply_add' || $_GET['op'] == 'keyword_reply_edit')){ ?>selected<?php } ?>"> <a href="index.php?act=autoreply&op=keyword_reply&wx_id=<?php echo intval($_GET['wx_id']); ?>">关键词自动回复</a> </dd>
          <dd class="item <?php if($_GET['act'] == 'autoreply' && ($_GET['op'] == 'reply_manage' || $_GET['op'] == 'reply_add' || $_GET['op'] == 'reply_edit')){ ?>selected<?php } ?>"> <a href="index.php?act=autoreply&op=reply_manage&wx_id=<?php echo intval($_GET['wx_id']); ?>">回复素材管理</a> </dd>
        </dl>
         <dl id="nc_menu_manage" class="menu">
          <dt class="menu_title"> <i class="icon3"></i>微用户管理CRM</dt>
          <dd class="item <?php if($_GET['act'] == 'fans'){ ?>selected<?php } ?>"> <a href="index.php?act=fans&op=fans_list&wx_id=<?php echo intval($_GET['wx_id']); ?>">粉丝管理</a> </dd>
          <dd class="item  <?php if($_GET['act'] == 'fans_activity'){ ?>selected<?php } ?>"> <a href="index.php?act=fans_activity&op=index&wx_id=<?php echo intval($_GET['wx_id']); ?>">粉丝行为统计</a> </dd>
          <dd class="item  <?php if($_GET['act'] == 'group_message'){ ?>selected<?php } ?>"> <a href="index.php?act=group_message&op=index&wx_id=<?php echo intval($_GET['wx_id']); ?>">群发消息</a> </dd>
          <dd class="item  <?php if($_GET['act'] == 'recongition'){ ?>selected<?php } ?>"> <a href="index.php?act=recongition&op=index&wx_id=<?php echo intval($_GET['wx_id']); ?>">渠道管理</a> </dd>
          <dd class="item  <?php if($_GET['act'] == 'service'){ ?>selected<?php } ?>"> <a href="index.php?act=service&op=index&wx_id=<?php echo intval($_GET['wx_id']); ?>">人工客服</a> </dd>
        </dl>
        <dl id="nc_menu_wall" class="menu">
          <dt class="menu_title"> <i class="icon3"></i>微信墙</dt>
          <dd class="item <?php if($output['sign'] == 'activity'){ echo 'selected';}?>"><a href="?act=activity&op=list&wx_id=<?php echo intval($_GET['wx_id']);?>">微信墙活动</a></dd>
          <dd class="item <?php if($output['sign'] == 'vote'){ echo 'selected';}?>"><a href="?act=activity&op=votelist&wx_id=<?php echo intval($_GET['wx_id']);?>">微信墙投票</a></dd>
        </dl>
       <dl id="nc_menu_wall" class="menu">
          <dt class="menu_title"> <i class="icon3"></i>微调研</dt>
          <dd class="item <?php if($_GET['act'] == 'research'){ ?>selected<?php } ?>"><a href="index.php?act=research&op=list&wx_id=<?php echo intval($_GET['wx_id']);?>">微调研活动</a></dd>
        </dl>
        <dl id="nc_menu_manage" class="menu">
          <dt class="menu_title"> <i class="icon4"></i>商城管理</dt>
          <dd class="item <?php if($_GET['act'] == 'store' && ($_GET['op'] == 'gc_manage' || $_GET['op'] == 'recgc_text_add' || $_GET['op'] == 'gc_manage_menu')){ ?>selected<?php } ?>"> <a href="index.php?act=store&op=gc_manage&wx_id=<?php echo intval($_GET['wx_id']); ?>&reply_type=click_reply">分类管理</a> </dd>
          <dd class="item <?php if($_GET['act'] == 'store' && ($_GET['op'] == 'goods_manage' || $_GET['op'] == 'recgoods_textpic_add')){ ?>selected<?php } ?>"> <a href="index.php?act=store&op=goods_manage&wx_id=<?php echo intval($_GET['wx_id']); ?>&reply_type=click_reply">商品管理</a> </dd>
          <?php if($output['wx_info']['shop_data_type'] == 1){ ?>
          <dd class="item <?php if($_GET['act'] == 'store' && ($_GET['op'] == 'zt_manage' || $_GET['op'] == 'reczt_add')){ ?>selected<?php } ?>"> <a href="index.php?act=store&op=zt_manage&wx_id=<?php echo intval($_GET['wx_id']); ?>&reply_type=click_reply">专题管理</a> </dd>
          <?php } ?>
        </dl>
      </div>
    </div>
    <div class="nc_main">
      <?php require_once($tpl_file);?>
    </div>
  </div>
</div>
<?php require_once template('footer');?>
</body>
</html>