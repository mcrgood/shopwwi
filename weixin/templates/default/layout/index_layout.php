<?php defined('InByShopWWI') or exit('Access Invalid!');?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>ShopWWI微信公众账号管理系统</title>
<meta name="author" content="ShopWWI">
<meta name="copyright" content="ShopWWI Inc. All Rights Reserved">
<link rel="Shortcut Icon" href="https://res.wx.qq.com/mpres/htmledition/images/favicon195d31.ico">
<link href="<?php echo TEMPLATES_URL; ?>/css/base199134.css" type="text/css" rel="stylesheet">
<link href="<?php echo TEMPLATES_URL; ?>/css/lib195d33.css" type="text/css" rel="stylesheet">
<link href="<?php echo TEMPLATES_URL; ?>/css/page_index195d33.css" rel="stylesheet">
<link href="<?php echo TEMPLATES_URL; ?>/css/layout.css" rel="stylesheet">
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.js"></script>
</head>
<body>
<div class="nc_head">
  <div class="inner">
    <div class="logo"> <a title="ShopWWI微信公众账号管理系统" href="<?php echo SHOP_SITE_URL; ?>"></a> </div>
    <div class="user-entry"> <span><a class="nickname" href="#"><?php echo $_SESSION['admin_truename']; ?></a></span> <?php if($_SESSION['admin_is_super'] != 1){ ?><span class="line">|</span> <span><a id="logout" href="index.php?act=admin&op=pwd_edit&admin_id=<?php echo $_SESSION['admin_id']; ?>">修改密码</a> </span><?php } ?> <span class="line">|</span> <span><a id="logout" href="index.php?act=login&op=logout">退出</a> </span> </div>
  </div>
</div>
<div class="nc_layout">
    <div class="nc_warp side_left">
     <div class="nc_side">
      <div class="nc_side_menu">
        <dl id="nc_menu_manage" class="menu no_line">
          <dt class="menu_title"> <i class="icon1"></i>管理</dt>
          <dd class="item  <?php if($_GET['act'] == '' || $_GET['act'] == 'index'){ ?>selected<?php } ?>"> <a href="index.php">公共账号管理</a> </dd>
          <?php if($_SESSION['admin_is_super'] == 1){ ?>
          <dd class="item  <?php if($_GET['act'] == 'admin'){ ?>selected<?php } ?>"> <a href="index.php?act=admin">管理员管理</a></dd>
          <?php } ?>
        </dl>
      </div>
    </div>
    <div class="nc_main">
      <?php require_once($tpl_file);?>
    </div>
  </div>
</div>
<?php require_once template('footer');?>
</body>
</html>

