<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="<?php echo TEMPLATES_URL; ?>/css/css.css" />
	<title>微调研-<?php echo $output['dyinfo']['item_name'];?></title>
    <style>
    .l-wrap-load,.l-wrap-index{background:url(<?php echo TEMPLATES_URL; ?>/images/wdy/bg.jpg) no-repeat 0 0;}
	.l-wrap-qus{background:url(<?php echo TEMPLATES_URL; ?>/images/wdy/qus-tri.png) no-repeat 100% 100%,url(<?php echo TEMPLATES_URL; ?>/images/wdy/qus-bg_hei.jpg) no-repeat 0 0;}
    </style>
    <script type="text/javascript">
    	var $STCONFIG = {
			VERSION 	: '1.0',
			SHAREIT 	: {
				title : '分享标题', 
				con : '分享内容',
				link : document.URL, 
				img  : "图片地址"	
			}
		};
		if(/Android (\d+\.\d+)/.test(navigator.userAgent)){
			var version = parseFloat(RegExp.$1);
			if(version > 2.3){
				var phoneScale = parseInt(window.screen.width) / 640;
				document.write('<meta name="viewport" content="width=640, minimum-scale = '+ phoneScale +', maximum-scale = '+ phoneScale +', target-densitydpi=device-dpi">');
			}else{
				document.write('<meta name="viewport" content="width=640, target-densitydpi=device-dpi">');
			}
		}else{
			document.write('<meta name="viewport" content="width=640, user-scalable=no, target-densitydpi=device-dpi">');
		}
		if(navigator.userAgent.indexOf('MicroMessenger') >= 0){
			document.addEventListener('WeixinJSBridgeReady', function() {
			});
		}
		var TEMPLATESURL = "<?php echo TEMPLATES_URL;?>";
    </script>
</head>
<body>
	<section class="l-wrap l-wrap-load l-load-show J-load">
		<div class="l-load-box">
			<div class="l-load-img"></div>
			<div class="l-load-txt">&nbsp;抽奖载入中...</div>
		</div>
	</section>
	<section class="l-wrap l-wrap-index l-animation J-index">
		<div class="l-index-c">
        	<h1><?php echo $output['lot']['lot_title'];?></h1>
            <p>
            	<?php 
				if($output['lot']['end_time'] <= time()){
					echo $output['lot']['end_title'];
				}else{
					echo $output['lot']['start_title'];
				}				
				?>
            </p>
            <p>
            	<?php 
				if($output['lot']['end_time'] <= time()){
					echo $output['lot']['end_content'];
				}else{
					echo $output['lot']['start_content'];
				}				
				?>
            </p>
            <form method="post">
            	<input type="hidden" name="act" value="wap_research"/>
    			<input type="hidden" name="op" value="getprive"/>
    			<input type="hidden" name="wx_id" value="<?php echo $_GET['wx_id']?>"/>
    			<input type="hidden" name="res_id" value="<?php echo $output['lot']['res_id'];?>"/>
    			<input type="hidden" name="fromuser" value="<?php echo $output['fromuser'];?>"/>
                 <input type="hidden" name="form_submit" value="ok" />
            </form>
        </div>
        <?php if($output['lot']['end_time'] > time()){?>
			<a class="l-btn" id="btn_sub" href="javascript:;" style="font-size:3em; color:#FFF; text-align:center; border:#900 1px solid; background:#f00; border-radius:5px; padding:0.2em; width:50%; margin:0 auto">去抽奖</a>
		<?php }?>		        
	</section>
    <script src="<?php echo TEMPLATES_URL; ?>/js/wdy/sea.js?version=1.0"></script>
	<script src="<?php echo TEMPLATES_URL; ?>/js/wdy/modules/app/main.js?version=1.0"></script>
</body>
</html>