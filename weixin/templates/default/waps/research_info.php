<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="<?php echo TEMPLATES_URL; ?>/css/css.css" />
	<title>微调研-<?php echo $output['dyinfo']['item_name'];?></title>
    <style>
    .l-wrap-load,.l-wrap-index{background:url(<?php echo TEMPLATES_URL; ?>/images/wdy/bg.jpg) no-repeat 0 0;}
	.l-wrap-qus{background:url(<?php echo TEMPLATES_URL; ?>/images/wdy/qus-tri.png) no-repeat 100% 100%,url(<?php echo TEMPLATES_URL; ?>/images/wdy/qus-bg_hei.jpg) no-repeat 0 0;}
    </style>
    <script type="text/javascript">
    	var $STCONFIG = {
			VERSION 	: '1.0',
			SHAREIT 	: {
				title : '分享标题', 
				con : '分享内容',
				link : document.URL, 
				img  : "图片地址"	
			}
		};
		if(/Android (\d+\.\d+)/.test(navigator.userAgent)){
			var version = parseFloat(RegExp.$1);
			if(version > 2.3){
				var phoneScale = parseInt(window.screen.width) / 640;
				document.write('<meta name="viewport" content="width=640, minimum-scale = '+ phoneScale +', maximum-scale = '+ phoneScale +', target-densitydpi=device-dpi">');
			}else{
				document.write('<meta name="viewport" content="width=640, target-densitydpi=device-dpi">');
			}
		}else{
			document.write('<meta name="viewport" content="width=640, user-scalable=no, target-densitydpi=device-dpi">');
		}
		if(navigator.userAgent.indexOf('MicroMessenger') >= 0){
			document.addEventListener('WeixinJSBridgeReady', function() {
			});
		}
		var TEMPLATESURL = "<?php echo TEMPLATES_URL;?>";
    </script>
</head>
<body>
	<section class="l-wrap l-wrap-load l-load-show J-load">
		<div class="l-load-box">
			<div class="l-load-img"></div>
			<div class="l-load-txt">&nbsp;微调研载入中...</div>
		</div>
	</section>
	<section class="l-wrap l-wrap-index l-animation J-index">
		<div class="l-index-c">
        	<h1><?php echo $output['dyinfo']['item_name'];?></h1>
            <p>
            	<?php echo $output['dyinfo']['item_content'];?>
            </p>
        </div>
        <?php if(!empty($output['dycon'])){?>
			<a class="l-btn l-start-btn J-start-btn" href="javascript:;"></a>
        <?php }?>
	</section>
    <form id="resform" method="post" action="?act=wap_research&op=save&wx_id=<?php echo $_GET['wx_id']?>">
    <input type="hidden" name="act" value="wap_research"/>
    <input type="hidden" name="op" value="save"/>
    <input type="hidden" name="wx_id" value="<?php echo $_GET['wx_id']?>"/>
    <input type="hidden" name="res_id" value="<?php echo $output['dyinfo']['item_id'];?>"/>
    <input type="hidden" name="fromuser" value="<?php echo $output['fromuser'];?>"/>
     <input type="hidden" name="form_submit" value="ok" />
    <?php if(!empty($output['dycon'])){?>
    <?php $i=0;
    	foreach($output['dycon'] as $key=>$val){
		$i++;
	?>
    <section class="l-wrap-qus l-wrap-qus<?php echo $i;?> J-qus">
		<div class="l-qus1-pet l-qus-pet"></div>
		<div class="l-qus-box">
			<div class="l-qus<?php echo $i;?>-tit">
            <?php echo $val['qtitle'];?>
            </div>
			<div class="l-qus-ans">
            <?php 
				$anwser = unserialize($val['qanwser']);
				$type = ($val['qtype'])?'checkbox':'radio';
				$na = ($val['qtype'])?'[]':'';
			?>
            
            <?php foreach($anwser as $k=>$v){?>
				<div class="l-qus<?php echo $i?>-ans-<?php echo $k?> fix J-ans-sel" data-sel="<?php echo $k?>">                	
					<span class="l-qus-ans-sel">
                    <input type="<?php echo $type;?>" style="width:1.5em; height:1.5em;" name="ans[<?php echo $val['qid']?>]<?php echo $na;?>" value="<?php echo $k;?>">
                    </span>
					<span class="l-qus-ans-txt"><?php echo $v?></span>
				</div>
             <?php }?>
			</div>
		</div>
		<div class="l-error J-error"></div>
		<div class="<?php echo (count($output['dycon']) == $i)?'l-qus-tj J-tj':'l-qus-next ';?> J-next"></div>
	</section>
	<?php }?>
    <?php }?>
	<section class="J-res l-wrap-res">
		<div class="l-res-logo"></div>
		<div class="l-res-con J-res-con"></div>
		<div class="l-res-btn J-show-score"></div>
	</section>
    </form>
	<script src="<?php echo TEMPLATES_URL; ?>/js/wdy/sea.js?version=1.0"></script>
	<script src="<?php echo TEMPLATES_URL; ?>/js/wdy/modules/app/main.js?version=1.0"></script>
</body>
</html>