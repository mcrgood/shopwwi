<style>
.main_bd{ padding:20px 10px;}
</style>
<div class="main_hd">
  <h2><?php echo '人工客服';?></h2>
</div>
<div class="main_bd">
  <div class="table_msg" style="padding-bottom:25px;">
  	<div style="padding-left:10px; color:#666; font-size:12px; padding-bottom:15px;">
    	微信客服是指微信公众平台自带的多客服系统，只有认证服务号才有此功能。<br>
微信客服启用方法，必须满足两个条件：<br>1、有客服处于登录状态；<br>2、下面的人工客服状态处于打开中。<br>这样系统在回答不上来的时候就会切换到客服模式。在客服模式下系统将不再做任何自动回复。<br>
如需彻底关闭微信客服：必须让客服全部下线，并且在下面关掉客服状态
    </div>
    <form method="post" action="?act=service&wx_id=<?php echo $_GET['wx_id'];?>" name="service_form" id="service_form" style=" margin-left:10px;">
     <input type="hidden" name="form_submit" value="ok" />
     <ul>
        <li class="list_item">
        <label class="label_box"><?php echo '开启人工客服';?></label>
        <div class="label_form"> 
          <span>
          	<input type="radio" name="service_check" value="1" <?php echo ($output['setting']['service_check']==1)?'checked="checked"':''?> />开启 &nbsp;&nbsp;&nbsp;&nbsp;
        	<input type="radio" name="service_check" value="0" <?php echo ($output['setting']['service_check']==0)?'checked="checked"':''?> />关闭
          </span>
        </div>
      </li>     
      <li>
        <div class="btn_bar">
        <input type="submit" class="btn_input" value="<?php echo $lang['nc_save'];?>"></div>
      </li>
    </ul>
    </form>
  </div>
</div>