<div class="main_hd">
  <h2>添加投票</h2>
</div>
<div class="main_bd">
  <form method="post" id="apply_form" action="?act=activity&op=addVote&wx_id=<?php echo intval($_GET['wx_id']);?>">
    <ul>
      <li class="list_item">
        <label class="label_box"><?php echo $lang['nc_weixin_wall_vote_name'];?></label>
        <div class="label_form"> 
          <span>
          	<input type="text" class="label_input" name="vote_name" id="vote_name">
            <label for='vote_name' class='error msg_invalid' style='display:none;'></label>
          </span>     
        </div>
      </li>
      <li class="list_item">
        <label class="label_box"><?php echo $lang['nc_weixin_wall_vote_content'];?></label>
        <div class="label_form">
          <?php showEditor('vote_content','','550px','300px','','true',false);?>
        </div>
      </li>
      <li>
        <div class="btn_bar">
        <input type="submit" class="btn_input" value="<?php echo $lang['nc_save'];?>"></div>
      </li>
    </ul>
  </form>
</div>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.validation.min.js"></script> 
<script type="text/javascript">
$(function(){
	jQuery.validator.addMethod("letters", function(value, element) {
		return this.optional(element) || ($.trim(value.replace(/[^\u0000-\u00ff]/g,"aa")).length==1);
	}, "Letters min please"); 
	
	$("#apply_form").validate({//验证表单
        rules: {
        	vote_name: {
				required:true
            }
        },
        messages:{
        	vote_name:{
        		required:'<?php echo $lang['nc_weixin_wall_vote_name_is_not_null'];?>'
            }
        }
	});
});
</script> 
