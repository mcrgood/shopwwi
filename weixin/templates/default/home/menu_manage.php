<?php defined('InByShopWWI') or exit('Access Invalid!');?>
<div class="main_hd">
  <h2><?php echo L('menu_manage'); ?></h2>
  <p class="extra_info"><a href="index.php?act=menu&op=click_reply_manage&wx_id=<?php echo intval($_GET['wx_id']); ?>"><?php echo L('menu_click_reply_manage'); ?></a></p>
</div>
<form id="add_form" method="post" enctype="multipart/form-data">
<input type="hidden" name="form_submit" value="ok" />
<input type="hidden" name="choose_gc" value="<?php echo $output['choose_gc']; ?>" />
<div class="main_bd">
	<ul>
		<?php for ($num=1;$num<=3;$num++){ ?>
		<li class="list_item">
	        <label class="label_box"><?php echo L('menu_di'); ?><?php echo $num==1?L('menu_one'):($num==2?L('menu_two'):L('menu_three'));?><?php echo L('menu_button').L('nc_colon'); ?></label>
	        <div class="label_form"> 
	          <?php echo L('menu_title').L('nc_colon'); ?><input type="text" name="main_menu_title[]" class="label_input" value="<?php echo $output['menu_list'][$num-1]['name']; ?>"/><br><br>
	          <?php echo L('menu_type').L('nc_colon'); ?><select name="main_menu_type[]" nc_data="<?php echo $num; ?>">
	          <option value="" <?php if($output['menu_list'][$num-1]['type'] == ''){ ?>selected<?php } ?>><?php echo L('menu_include_child_menu'); ?></option>
	          <option value="click" <?php if($output['menu_list'][$num-1]['type'] == 'click'){ ?>selected<?php } ?>><?php echo L('menu_click_event'); ?></option>
	          <option value="view" <?php if($output['menu_list'][$num-1]['type'] == 'view'){ ?>selected<?php } ?>><?php echo L('menu_website_browse'); ?></option>
	          <?php if ($output['choose_gc'] != "") { ?>
	          <option value="menu_recgc" >推荐分类菜单</option>
	          <?php } ?>
	          <option value="home_page" >店铺主页</option>
	          </select><br><br>
	          <span nc_data="<?php echo $num; ?>" class="main_menu_url">URL：<input type="text" name="main_menu_url[]" class="label_input" value="<?php echo $output['menu_list'][$num-1]['url']; ?>"/></span>
	          <span nc_data="<?php echo $num; ?>" class="main_menu_click_reply"><?php echo L('menu_click_reply').L('nc_colon'); ?><select name="main_menu_click_reply[]">
	          <?php if(!empty($output['cr_list'])){ ?>
	          <?php foreach ($output['cr_list'] as $k=>$v){ ?>
	          <option value="<?php echo $v['cr_code']; ?>" <?php if($output['menu_list'][$num-1]['key'] == $v['cr_code']){ ?>selected<?php } ?>><?php echo $v['cr_title']; ?></option>
	          <?php }} ?>
		          </select></span>
		        </div>
	     </li>
	     <li class="list_item" nc_data="<?php echo $num; ?>">
	        <label class="label_box"><?php echo L('menu_child_menu').L('nc_colon'); ?></label>
	        <div class="label_form"> 
	        <?php for ($i=1;$i<=5;$i++){ ?>
	          No.<?php echo $i; ?> <?php echo L('menu_title').L('nc_colon'); ?><input type="text" name="child_menu_title[]" class="label_input" value="<?php echo $output['menu_list'][$num-1]['sub_button'][$i-1]['name']; ?>"/><br><br>
	          No.<?php echo $i; ?> <?php echo L('menu_type').L('nc_colon'); ?><select name="child_menu_type[]" nc_data="<?php echo $num; ?>-<?php echo $i; ?>">
	          <option value="click" <?php if($output['menu_list'][$num-1]['sub_button'][$i-1]['type'] == 'click'){ ?>selected<?php } ?>><?php echo L('menu_click_event'); ?></option>
	          <option value="view" <?php if($output['menu_list'][$num-1]['sub_button'][$i-1]['type'] == 'view'){ ?>selected<?php } ?>><?php echo L('menu_website_browse'); ?></option>
	          </select>
	          <span nc_data="<?php echo $num; ?>-<?php echo $i; ?>" class="child_menu_url">URL：<input type="text" name="child_menu_url[]" class="label_input" value="<?php echo $output['menu_list'][$num-1]['sub_button'][$i-1]['url']; ?>"/></span>
	          <span nc_data="<?php echo $num; ?>-<?php echo $i; ?>" class="child_menu_click_reply"><?php echo L('menu_click_reply').L('nc_colon'); ?><select name="child_menu_click_reply[]">
	          <?php if(!empty($output['cr_list'])){ ?>
	          <?php foreach ($output['cr_list'] as $k=>$v){ ?>
	          <option value="<?php echo $v['cr_code']; ?>" <?php if($output['menu_list'][$num-1]['sub_button'][$i-1]['key'] == $v['cr_code']){ ?>selected<?php } ?>><?php echo $v['cr_title']; ?></option>
	          <?php }} ?>
		          </select></span><br><br><br>
		      <?php } ?>
		        </div>
	     </li>
	     <?php } ?>
	     <li>
	        <div class="btn_bar">
	        <input type="submit" class="btn_input" value="<?php echo L('menu_publish'); ?>">&nbsp;&nbsp;&nbsp;&nbsp;<input type="button" class="btn_input" id="del_menu" value="<?php echo L('nc_delete'); ?>"></div>
	     </li>
     </ul>
</div>
</form>
<script type="text/javascript">
$(function(){
	//初始化
	<?php if(!empty($output['menu_list'])){ ?>
	<?php for ($num=1;$num<=3;$num++){ ?>
	<?php if($output['menu_list'][$num-1]['type'] == ''){ ?>
	$('.main_menu_url[nc_data=<?php echo $num; ?>]').hide();
	$('.main_menu_click_reply[nc_data=<?php echo $num; ?>]').hide();
	$('li[nc_data=<?php echo $num; ?>]').show();
	<?php } ?>
	<?php if($output['menu_list'][$num-1]['type'] == 'click'){ ?>
	$('.main_menu_url[nc_data=<?php echo $num; ?>]').hide();
	$('.main_menu_click_reply[nc_data=<?php echo $num; ?>]').show();
	$('li[nc_data=<?php echo $num; ?>]').hide();
	<?php } ?>
	<?php if($output['menu_list'][$num-1]['type'] == 'view'){ ?>
	$('.main_menu_url[nc_data=<?php echo $num; ?>]').show();
	$('.main_menu_click_reply[nc_data=<?php echo $num; ?>]').hide();
	$('li[nc_data=<?php echo $num; ?>]').hide();
	<?php } ?>
	<?php for ($i=1;$i<=5;$i++){ ?>
	<?php if($output['menu_list'][$num-1]['sub_button'][$i-1]['type'] == 'click' || $output['menu_list'][$num-1]['sub_button'][$i-1]['type'] == ''){ ?>
	$('.child_menu_url[nc_data=<?php echo $num.'-'.$i; ?>]').hide();
	$('.child_menu_click_reply[nc_data=<?php echo $num.'-'.$i; ?>]').show();
	<?php } ?>
	<?php if($output['menu_list'][$num-1]['sub_button'][$i-1]['type'] == 'view'){ ?>
	$('.child_menu_url[nc_data=<?php echo $num.'-'.$i; ?>]').show();
	$('.child_menu_click_reply[nc_data=<?php echo $num.'-'.$i; ?>]').hide();
	<?php } ?>
	<?php } ?>
	<?php } ?>
	<?php }else{ ?>
	$('.main_menu_url').hide();
	$('.main_menu_click_reply').hide();
	$('.child_menu_url').hide();
	<?php } ?>
	//主菜单控制
	$('select[name="main_menu_type[]"]').change(function(){
		var nc_data = $(this).attr("nc_data");
		if($(this).val() == ''){
			$('.main_menu_url[nc_data='+nc_data+']').hide();
			$('.main_menu_click_reply[nc_data='+nc_data+']').hide();
			$('li[nc_data='+nc_data+']').show();
		}
		if($(this).val() == 'click'){
			$('.main_menu_url[nc_data='+nc_data+']').hide();
			$('.main_menu_click_reply[nc_data='+nc_data+']').show();
			$('li[nc_data='+nc_data+']').hide();
		}
		if($(this).val() == 'view'){
			$('.main_menu_url[nc_data='+nc_data+']').show();
			$('.main_menu_click_reply[nc_data='+nc_data+']').hide();
			$('li[nc_data='+nc_data+']').hide();
		}
	});
	//子菜单控制
	$('select[name="child_menu_type[]"]').change(function(){
		var nc_data = $(this).attr("nc_data");
		if($(this).val() == 'click'){
			$('.child_menu_url[nc_data='+nc_data+']').hide();
			$('.child_menu_click_reply[nc_data='+nc_data+']').show();
		}
		if($(this).val() == 'view'){
			$('.child_menu_url[nc_data='+nc_data+']').show();
			$('.child_menu_click_reply[nc_data='+nc_data+']').hide();
		}
	});
	//删除自定义菜单
	$('#del_menu').click(function(){
		$.getJSON('index.php?act=menu&op=menu_del&wx_id=<?php echo intval($_GET['wx_id']); ?>', function(result){
	        if(result.done){
		        alert('<?php echo L('menu_del_succ'); ?>');
	            window.location.href = 'index.php?act=menu&op=menu_manage&wx_id=<?php echo intval($_GET['wx_id']); ?>';
	        }else{
	            alert('<?php echo L('menu_del_failed'); ?>('+result.msg+')');
	        }
	    });
	});
})
</script>