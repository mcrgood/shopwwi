<style>
.main_bd{ padding:20px 10px;}
.titles{ padding-bottom:10px; color:#888;}
.titles ul{ list-style:none; border-bottom:1px #CCC solid; }
.titles ul li { list-style:none; display:inline-block; margin-bottom:-1px; line-height:25px; margin-left:5px;}
.titles a{ color:#888; text-decoration:none; display:block;  padding:5px 10px; line-height:25px; background:#EEE; border:1px #EEE solid;  border-bottom:1px #CCC solid; }
.titles ul li a.curr{ color:#000; background:#CCC;}
</style>
<div class="main_hd">
  <h2><?php echo '群发消息';?></h2>
</div>
<div class="main_bd">
  <div class="titles">
  	<ul>
    	<li><a href="?act=group_message&op=index&wx_id=<?php echo intval($_GET['wx_id']);?>" class="curr">群发消息<!--（通过群发接口）--></a></li>
        <!--<li><a href="?act=group_message&op=service&wx_id=<?php echo intval($_GET['wx_id']);?>">群发消息（通过客服接口）</a></li>-->
        <li><a href="?act=group_message&op=list&wx_id=<?php echo intval($_GET['wx_id']);?>">发送记录</a></li>
        <li><a href="?act=group_message&op=help&wx_id=<?php echo intval($_GET['wx_id']);?>">帮助说明</a></li>
    </ul>
  </div>
  
  <form method="post" id="apply_form" action="?act=group_message&op=index&wx_id=<?php echo intval($_GET['wx_id']);?>">
  	<input type="hidden" name="form_submit" value="ok" />
    <ul>
      <li class="list_item">
        <label class="label_box"><font style="color:red">*&nbsp;</font><?php echo '选择分组';?></label>
        <div class="label_form"> 
          <span>
          	<select name="group" id="group">
            <?php foreach($output['group_list'] as $key=>$val){?>
            	<option value="<?php echo $val['id'];?>"><?php echo $val['name'];?></option>
            <?php }?>
            </select>
            <br />
            <span style="color:#666; font-size:12px">请选择要接收信息的组</span>
            <br />
            <label for='group' class='error msg_invalid' style='display:none;'></label>
          </span>     
        </div>
      </li>
      <li class="list_item">
        <label class="label_box"><font style="color:red">*&nbsp;</font><?php echo '素材';?></label>
        <div class="label_form"> 
          <span>
          	<select name="resource" id="resource">
            <option value="0">-请选择-</option>
            <?php foreach($output['resource'] as $key=>$val){?>
            	<option value="<?php echo $val['reply_id'];?>"><?php echo $val['reply_title'];?></option>
            <?php }?>
            </select>
            <br />
            <span style="color:#666; font-size:12px">请选择要发送的消息</span>
            <br />
            <label for='resource' class='error msg_invalid' style='display:none;'></label>
          </span>     
        </div>
      </li>     
      <li>
        <div class="btn_bar">
        <input type="submit" class="btn_input" value="<?php echo '发送';?>"></div>
      </li>
    </ul>
  </form>
</div>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.validation.min.js"></script> 
<script type="text/javascript">
$(function(){	
	$("#apply_form").validate({//验证表单
        rules: {
        	resource: {
				required:true,
				min:1
            }
        },
        messages:{
        	resource:{
        		required:'<?php echo '请选择素材';?>',
				min:'<? echo '请选择素材';?>'
            }
        }
	});
});

</script> 
