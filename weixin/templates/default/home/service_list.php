<style>
.main_bd{ padding:20px 10px;}
.titles{ padding-bottom:10px; color:#888;}
.titles ul{ list-style:none; border-bottom:1px #CCC solid; }
.titles ul li { list-style:none; display:inline-block; margin-bottom:-1px; line-height:25px; margin-left:5px;}
.titles a{ color:#888; text-decoration:none; display:block;  padding:5px 10px; line-height:25px; background:#EEE; border:1px #EEE solid;  border-bottom:1px #CCC solid; }
.titles ul li a.curr{ color:#000; background:#CCC;}
</style>
<div class="main_hd">
  <h2><?php echo '群发消息';?></h2>
</div>
<div class="main_bd">
  <div class="titles">
  	<ul>
    	<li><a href="?act=group_message&op=index&wx_id=<?php echo intval($_GET['wx_id']);?>">群发消息<!--（通过群发接口）--></a></li>
        <!--<li><a href="?act=group_message&op=service&wx_id=<?php echo intval($_GET['wx_id']);?>">群发消息（通过客服接口）</a></li>-->
        <li><a href="?act=group_message&op=list&wx_id=<?php echo intval($_GET['wx_id']);?>" class="curr">发送记录</a></li>
        <li><a href="?act=group_message&op=help&wx_id=<?php echo intval($_GET['wx_id']);?>">帮助说明</a></li>
    </ul>
  </div>
  <div class="table_msg">
    <table cellspacing="0" cellpadding="0">
      <thead class="nc-thead">
        <tr>
          <th class="table_cell time asc"><?php echo '标题';?></th>
          <th class="table_cell goods"><?php echo '类型';?></th>
          <th class="table_cell goods"><?php echo '成功数量';?></th>
          <th class="table_cell goods"><?php echo '发送时间';?></th>
          <th class="table_cell"><?php echo $lang['nc_handle'];?></th>
        </tr>
      </thead>
      <tbody class="nc-tbody">
        <?php if(!empty($output['list'])){?>
        <?php foreach($output['list'] as $key=>$val){?>
        <tr>
          <td><?php if(mb_strlen($val['msg_title'],'utf-8')>=12){ echo mb_substr($val['msg_title'],0,12,'utf-8').'...';}else{ echo $val['msg_title'];}?></td>
          <td><?php echo $val['type'];?></td>
          <td><?php echo $val['succ_count'];?></td>
          <td><?php echo date('Y-m-d H:i:s',$val['send_time']);?></td>
          <td class="last">
          <a href="javascript:void(0);" onClick="deleteitem(<?php echo $val['msg_id']?>)"><?php echo '删除';?></a>
          </td>
        </tr>
        <?php }?>
        <?php }else{?>
        <tr>
          <td colspan="20" valign="middle" ><div class="norecord"><span><?php echo $lang['nc_record'];?></span></div></td>
        </tr>
        <?php }?>
      </tbody>
    </table>
  </div>
  <div class="pagination"> <?php echo $output['page'];?> </div>
</div>
<script type="text/javascript">
	function deleteitem(activity_id){
		if(confirm('<?php echo '确认要删除吗？'?>')){
			location.href = "?act=group_message&op=del&wx_id=<?php echo intval($_GET['wx_id']);?>&msg_id="+activity_id;
		}
	}	
</script> 
