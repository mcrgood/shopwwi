<?php defined('InByShopWWI') or exit('Access Invalid!');?>

<div class="main_hd">
  <h2><?php echo L('fans_fans_manage'); ?></h2>
  <p class="extra_info"><a href="index.php?act=fans&op=group_list&wx_id=<?php echo intval($_GET['wx_id']); ?>"><?php echo L('fans_group_manage'); ?></a></p>
</div>
<div class="main_bd">
  <div class="zt"> <span><?php echo L('fans_nickname').L('nc_colon'); ?></span>
    <input name="s_nickname" class="label_input_zt" type="text" value="<?php echo trim($_GET['s_nickname']); ?>"/>
    <input type="button" class="search-button" id="search" />
    <input type="button" class="update-button" id="fans_update" /><span class="zt—f"><?php echo L('fans_now_updateing_pls_wait'); ?></span>
  </div>
  <div class="table_msg">
    <table cellspacing="0" cellpadding="0">
      <thead class="nc-thead">
        <tr>
          <th class="table_cell" style="width:50px;"><?php echo '头像'; ?></th>
          <th class="table_cell" style="width:23%"><?php echo L('fans_nickname'); ?></th>
          <th class="table_cell" style="width:40px;"><?php echo L('fans_sex'); ?></th>
          <th class="table_cell"><?php echo L('fans_area'); ?></th>
          <th class="table_cell" style="width:20%"><?php echo L('fans_sub_time'); ?></th>
          <th class="table_cell" style="width:19%"><?php echo L('nc_handle'); ?></th>
        </tr>
      </thead>
      <tbody class="nc-tbody">
        <?php if(!empty($output['fans_list'])){?>
        <?php foreach($output['fans_list'] as $key=>$val){?>
        <tr>
          <td><img src="<?php echo $val['fans_headimgurl']; ?>" style="max-width:40px; margin-top:5px"  /></td>
          <td><?php echo $val['fans_nickname']; ?></td>
          <td><?php echo $val['fans_sex']==1?L('fans_male'):($val['fans_sex']==2?L('fans_female'):'未知'); ?></td>
          <td><?php echo $val['fans_country'].' '.$val['fans_province'].' '.$val['fans_city']; ?></td>
          <td><?php echo date('Y-m-d H:i:s',$val['fans_subtime']); ?></td>
          <td class="last"><a href="index.php?act=fans&op=send_msg&wx_id=<?php echo intval($_GET['wx_id']); ?>&fans_id=<?php echo $val['fans_id']; ?>"><?php echo L('fans_send_msg'); ?></a> <span>|</span> <a href="index.php?act=fans&op=group_move&wx_id=<?php echo intval($_GET['wx_id']); ?>&fans_id=<?php echo $val['fans_id']; ?>"><?php echo L('fans_change_group'); ?></a></td>
        </tr>
        <?php }?>
        <?php }else { ?>
        <tr>
          <td colspan="15" style="text-align:center"><?php echo L('fans_no_record'); ?></td>
        </tr>
        <?php } ?>
      </tbody>
    </table>
  </div>
  <div class="pagination"> <?php echo $output['show_page'];?> </div>
</div>
<script type="text/javascript">
$(function(){
	$('.zt—f').hide();
	$('#fans_update').click(function(){
		$('.zt—f').show();
		$.getJSON('index.php?act=fans&op=fans_update&wx_id=<?php echo intval($_GET['wx_id']); ?>', function(result){
	        if(result.done){
	            window.location.href = 'index.php?act=fans&op=fans_list&wx_id=<?php echo intval($_GET['wx_id']); ?>';
	        }else{
	        	$('.zt—f').hide();
	            alert('<?php echo L('fans_update_fans_list_failed'); ?>('+result.msg+')');
	        }
	    });
	});
	$('#search').click(function(){
		var s_nickname = $('input[type="text"][name="s_nickname"]').val();
		window.location.href = 'index.php?act=fans&op=fans_list&wx_id=<?php echo intval($_GET['wx_id']); ?>&s_nickname='+s_nickname;
	});
})
</script>