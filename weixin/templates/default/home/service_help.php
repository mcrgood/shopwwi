<style>
.main_bd{ padding:20px 10px;}
.titles{ padding-bottom:10px; color:#888;}
.titles ul{ list-style:none; border-bottom:1px #CCC solid; }
.titles ul li { list-style:none; display:inline-block; margin-bottom:-1px; line-height:25px; margin-left:5px;}
.titles a{ color:#888; text-decoration:none; display:block;  padding:5px 10px; line-height:25px; background:#EEE; border:1px #EEE solid;  border-bottom:1px #CCC solid; }
.titles ul li a.curr{ color:#000; background:#CCC;}
</style>
<div class="main_hd">
  <h2><?php echo '群发消息';?></h2>
</div>
<div class="main_bd">
  <div class="titles">
  	<ul>
    	<li><a href="?act=group_message&op=index&wx_id=<?php echo intval($_GET['wx_id']);?>">群发消息<!--（通过群发接口）--></a></li>
        <!--<li><a href="?act=group_message&op=service&wx_id=<?php echo intval($_GET['wx_id']);?>">群发消息（通过客服接口）</a></li>-->
        <li><a href="?act=group_message&op=list&wx_id=<?php echo intval($_GET['wx_id']);?>">发送记录</a></li>
        <li><a href="?act=group_message&op=help&wx_id=<?php echo intval($_GET['wx_id']);?>" class="curr">帮助说明</a></li>
    </ul>
  </div>
  <div class="table_msg">
    <table cellspacing="0" cellpadding="0">
      <thead class="nc-thead">
        <tr>
          <th class="table_cell time asc">&nbsp;</th>
          <th class="table_cell goods">发送消息（通过群发接口）</th>
          <th class="table_cell goods">发送消息（通过客服接口）</th>
        </tr>
      </thead>
      <tbody class="nc-tbody">
        <tr>
          <td>什么号能使用</td>
          <td>交过300块钱认证费的高级服务号</td>
          <td>交过300块钱认证费的高级服务号</td>
        </tr>
        <tr>
          <td>类型限制</td>
          <td>仅能发送图文消息和多图文消息</td>
          <td>文本消息、音频、视频、图片和图文消息</td>
        </tr>
        <tr>
          <td>条数限制</td>
          <td>每月四条</td>
          <td>无条数限制</td>
        </tr>
        <tr>
          <td>发送范围</td>
          <td>所有粉丝</td>
          <td>只有48小时内给公众号发送过信息的粉丝才能接收到信息</td>
        </tr>
        <tr>
          <td>发送速度</td>
          <td>快，一次性执行</td>
          <td>需要针对一个个粉丝发送，比较慢</td>
        </tr>
      </tbody>
    </table>
  </div>
</div>
