<?php defined('InByShopWWI') or exit('Access Invalid!');?>
<div class="main_hd">
  <h2><?php echo L('wx_account_list'); ?></h2>
  <?php if($_SESSION['admin_is_super'] == 1){ ?>
  <p class="extra_info"><a href="index.php?act=index&op=wx_add"><?php echo L('nc_add'); ?></a></p>
  <?php } ?>
</div>
<div class="main_bd">
<div class="table_msg">
  <table cellspacing="0" cellpadding="0">
    <thead class="nc-thead">
      <tr>
        <th class="table_cell"><?php echo L('wx_account_name'); ?></th>
        <th class="table_cell"><?php echo L('wx_account'); ?></th>
        <th class="table_cell" style="width:25%"><?php echo L('nc_handle'); ?></th>
      </tr>
    </thead>
    <tbody class="nc-tbody">
		<?php if(!empty($output['wx_list'])){?>
		<?php foreach($output['wx_list'] as $key=>$val){?>
		<tr>
		  <td><?php echo $val['wx_name']; ?><?php if($val['shop_data_type'] == 1){ ?><font style="color:red">（平台）</font><?php } ?></td>
		  <td><?php echo $val['wx_account']; ?></td>
		  <td class="last"><a href="index.php?act=dashboard&wx_id=<?php echo $val['wx_id']?>"><?php echo L('wx_function_manage'); ?></a>
		  <?php if($_SESSION['admin_is_super'] == 1){ ?>
          <span>|<span>
          <a href="index.php?act=index&op=wx_edit&wx_id=<?php echo $val['wx_id']?>"><?php echo $lang['nc_edit'];?></a>
          <span>|<span>
          <a href="javascript:if(confirm('<?php echo L('wx_account_del_confirm'); ?>'))window.location.href='index.php?act=index&op=wx_del&wx_id=<?php echo $val['wx_id'];?>';"><?php echo $lang['nc_delete']?></a>
          <?php } ?>
          </td>
		</tr>
		<?php }?>
		<?php }else { ?>
        <tr>
          <td colspan="15" style="text-align:center"><?php echo L('wx_no_record'); ?></td>
        </tr>
        <?php } ?>
    </tbody>  
  </table>
</div>
</div>