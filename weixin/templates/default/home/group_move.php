<?php defined('InByShopWWI') or exit('Access Invalid!');?>
<div class="main_hd">
  <h2><?php echo L('fans_group_edit'); ?></h2>
  <p class="extra_info"><a href="index.php?act=fans&op=fans_list&wx_id=<?php echo intval($_GET['wx_id']); ?>"><?php echo L('fans_fans_manage'); ?></a></p>
</div>
<div class="main_bd">
    <ul>
    	<li class="list_item">
	        <label class="label_box"><?php echo L('fans_nickname').L('nc_colon'); ?></label>
	        <div class="label_form"> 
	          <span>
	          	<?php echo $output['fans_info']['fans_nickname']; ?>
	          </span>     
	        </div>
         </li>
         <li class="list_item">
	        <label class="label_box"><?php echo L('fans_sex').L('nc_colon'); ?></label>
	        <div class="label_form"> 
	          <span>
	          	<?php echo $output['fans_info']['fans_sex']==1?L('fans_male'):L('fans_female'); ?>
	          </span>     
	        </div>
         </li>
         <li class="list_item">
	        <label class="label_box"><?php echo L('fans_area').L('nc_colon'); ?></label>
	        <div class="label_form"> 
	          <span>
	          	<?php echo $output['fans_info']['fans_country'].' '.$output['fans_info']['fans_province'].' '.$output['fans_info']['fans_city']; ?>
	          </span>     
	        </div>
         </li>
         <li class="list_item">
	        <label class="label_box"><?php echo L('fans_sub_time').L('nc_colon'); ?></label>
	        <div class="label_form"> 
	          <span>
	          	<?php echo date('Y-m-d H:i:s',$output['fans_info']['fans_subtime']); ?>
	          </span>     
	        </div>
         </li>
         <li class="list_item">
	        <label class="label_box"><?php echo L('fans_group_setting').L('nc_colon'); ?></label>
	        <div class="label_form"> 
	          <span>
	          	<select id="choose_group">
	          	<option value="-1"><?php echo L('fans_pls_choose_group_-'); ?></option>
	          	<?php if(!empty($output['group_list'])){ ?>
	          	<?php foreach ($output['group_list'] as $k=>$v){ ?>
	          	<option value="<?php echo $v['id']; ?>"><?php echo $v['name']; ?></option>
	          	<?php }} ?>
	          	</select>
	          </span>     
	        </div>
         </li>
         <li>
	        <div class="btn_bar">
	        <input type="submit" class="btn_input" value="<?php echo $lang['nc_save'];?>"></div>
	     </li>
    </ul>
</div>
<script type="text/javascript">
$(function(){
	$('.btn_input').click(function(){
		$.getJSON('index.php?act=fans&op=group_move&wx_id=<?php echo intval($_GET['wx_id']); ?>',{
			'form_submit':'ok',
			'choose_group':$('#choose_group').val(),
			'openid':'<?php echo $output['fans_info']['fans_openid']; ?>'
		},function(result){
        if(result.done){
            window.location.href = 'index.php?act=fans&op=fans_list&wx_id=<?php echo intval($_GET['wx_id']); ?>';
        }else{
            alert('<?php echo L('fans_change_fans_group_failed'); ?>('+result.msg+')');
        }
    	});
	});
})
</script>