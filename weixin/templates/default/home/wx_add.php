<?php defined('InByShopWWI') or exit('Access Invalid!');?>
<div class="main_hd">
  <h2><?php echo L('wx_account_add'); ?></h2>
  <p class="extra_info"><a href="index.php"><?php echo L('wx_account_list'); ?></a></p>
</div>
<form id="add_form" method="post">
<input type="hidden" name="form_submit" value="ok" />
<div class="main_bd">
	<ul>
		<li class="list_item">
	        <label class="label_box"><font style="color:red">*</font><?php echo L('wx_name').L('nc_colon'); ?></label>
	        <div class="label_form"> 
	          <span>
	          	<input type="text" name="wx_name" value="" class="label_input">
	          	<label for='wx_name' class='error msg_invalid' style='display:none;'></label>
	          </span>     
	        </div>
         </li>
         <li class="list_item">
	        <label class="label_box"><font style="color:red">*</font><?php echo L('wx_ori_id').L('nc_colon'); ?></label>
	        <div class="label_form"> 
	          <span>
	          	<input type="text" name="wx_ori_id" value="" class="label_input">
	          	<label for='wx_ori_id' class='error msg_invalid' style='display:none;'></label>
	          </span>     
	        </div>
         </li>
         <li class="list_item">
	        <label class="label_box"><font style="color:red">*</font><?php echo L('wx_account').L('nc_colon'); ?></label>
	        <div class="label_form"> 
	          <span>
	          	<input type="text" name="wx_account" value="" class="label_input">
	          	<label for='wx_account' class='error msg_invalid' style='display:none;'></label>
	          </span>     
	        </div>
         </li>
         <li class="list_item">
	        <label class="label_box"><?php echo L('wx_login_email').L('nc_colon'); ?></label>
	        <div class="label_form"> 
	          <span>
	          	<input type="text" name="wx_email" value="" class="label_input">
	          </span>     
	        </div>
         </li>
         <li class="list_item">
	        <label class="label_box"><?php echo L('wx_province').L('nc_colon'); ?></label>
	        <div class="label_form"> 
	          <span>
	          	<input type="text" name="wx_province" value="" class="label_input">
	          </span>     
	        </div>
         </li>
         <li class="list_item">
	        <label class="label_box"><?php echo L('wx_city').L('nc_colon'); ?></label>
	        <div class="label_form"> 
	          <span>
	          	<input type="text" name="wx_city" value="" class="label_input">
	          </span>     
	        </div>
         </li>
         <li class="list_item">
	        <label class="label_box"><font style="color:red">*</font>APPID：</label>
	        <div class="label_form"> 
	          <span>
	          	<input type="text" name="wx_appid" value="" class="label_input">
	          	<label for='wx_appid' class='error msg_invalid' style='display:none;'></label>
	          </span>     
	        </div>
         </li>
         <li class="list_item">
	        <label class="label_box"><font style="color:red">*</font>APPSECRET：</label>
	        <div class="label_form"> 
	          <span>
	          	<input type="text" name="wx_appsecret" value="" class="label_input">
	          	<label for='wx_appsecret' class='error msg_invalid' style='display:none;'></label>
	          </span>     
	        </div>
         </li>
         <li class="list_item">
	        <label class="label_box"><font style="color:red"></font>数据类型：</label>
	        <div class="label_form"> 
	          <span>
	          	<select name="shop_data_type" >
	          		<option value="1" >平台自营</option>
	          		<option value="2" >平台商家</option>
	          	</select>
	          	<label for='shop_data_type' class='error msg_invalid' style='display:none;'></label>
	          </span>     
	        </div>
         </li>
         <li class="list_item" id="show_store_id">
	        <label class="label_box"><font style="color:red">*</font>店铺ID：</label>
	        <div class="label_form"> 
	          <span>
	          	<input type="text" name="store_id" value="" class="label_input" style="width:80px" >
	          	<label for='store_id' class='error msg_invalid' style='display:none;'></label>
	          </span>     
	        </div>
         </li>
         <li>
	        <div class="btn_bar">
	        <input type="submit" class="btn_input" value="<?php echo $lang['nc_save'];?>"></div>
	     </li>
	</ul>
</div>
</form>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.validation.min.js"></script>
<script type="text/javascript">
$(function(){
	$('#show_store_id').hide();
	$('select[name="shop_data_type"]').change(function(){
		var type = $(this).val();
		if(type == "1"){
			$('#show_store_id').hide();
		} else {
			$('#show_store_id').show();
		}
	});
	$("#add_form").validate({
        rules: {
        	wx_name: {
				required:true
            },
            wx_ori_id:{
            	required:true
            },
            wx_account:{
            	required:true
            },
            wx_appid:{
            	required:true
            },
            wx_appsecret:{
            	required:true
            }
        },
        messages:{
        	wx_name:{
        		required:'<?php echo L('wx_account_name_must_write'); ?>'
            },
            wx_ori_id:{
            	required:'<?php echo L('wx_ori_id_must_write'); ?>'
            },
            wx_account:{
            	required:'<?php echo L('wx_account_must_write'); ?>'
            },
            wx_appid:{
            	required:'<?php echo L('wx_appid_must_write'); ?>'
            },
            wx_appsecret:{
            	required:'<?php echo L('wx_appsecret_must_write'); ?>'
            }
        }
	});
})
</script>