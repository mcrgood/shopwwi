<div class="main_hd">
  <h2><?php echo $lang['nc_basic_article'];?></h2>
</div>
<div class="main_bd">
  <form method="post" id="apply_form" action="?act=basic&op=editBasic&wx_id=<?php echo intval($_GET['wx_id']);?>">
    <ul>
      <li class="list_item">
        <label class="label_box"><?php echo $lang['nc_basic_title'];?>:</label>
        <div class="label_form"> 
          <span>
			<?php echo $output['basic']['title'];?>
          </span>     
        </div>
      </li>
      
      <li class="list_item">
        <label class="label_box"><?php echo $lang['nc_basic_content'];?>:</label>
        <div class="label_form">
          <?php showEditor('content',$output['basic']['content'],'550px','300px','','true',false);?>
        </div>
      </li>
      
      <li>
        <div class="btn_bar">
        	<input type="hidden" name="basic_id" value="<?php echo $output['basic']['basic_id']?>">
        	<input type="submit" class="btn_input" value="<?php echo $lang['nc_save'];?>">
        </div>
      </li>
    </ul>
  </form>
</div>