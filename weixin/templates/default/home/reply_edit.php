<?php defined('InByShopWWI') or exit('Access Invalid!');?>

<div class="main_hd">
  <h2><?php echo L('ar_reply_edit'); ?></h2>
  <p class="extra_info"><a href="index.php?act=autoreply&op=reply_manage&wx_id=<?php echo intval($_GET['wx_id']); ?>"><?php echo L('ar_reply_manage'); ?></a></p>
</div>
<form id="add_form" method="post" enctype="multipart/form-data">
  <input type="hidden" name="form_submit" value="ok" />
  <input type="hidden" name="reply_id" value="<?php echo $output['reply_info']['reply_id']; ?>" />
  <div class="main_bd">
    <ul>
      <li class="list_item">
        <label class="label_box"><font style="color:red">*</font><?php echo L('ar_subject').L('nc_colon'); ?></label>
        <div class="label_form"> <span>
          <input type="text" name="reply_title" value="<?php echo $output['reply_info']['reply_title']; ?>" class="label_input">
          <label for='reply_title' class='error msg_invalid' style='display:none;'></label>
          </span> </div>
      </li>
      <li class="list_item">
        <label class="label_box">&nbsp;&nbsp;<?php echo L('ar_intro').L('nc_colon'); ?></label>
        <div class="label_form">
          <textarea name="reply_note" style="width:350px;height:100px"><?php echo $output['reply_info']['reply_note']; ?></textarea>
        </div>
      </li>
      <li class="list_item">
        <label class="label_box"><font style="color:red">*</font><?php echo L('ar_type').L('nc_colon'); ?></label>
        <div class="label_form"> <span>
          <select name="reply_type">
            <option value="1" <?php if($output['reply_info']['reply_type'] == 1){ ?>selected<?php } ?>><?php echo L('ar_pure_text'); ?></option>
            <option value="2" <?php if($output['reply_info']['reply_type'] == 2){ ?>selected<?php } ?>><?php echo L('ar_image_text'); ?></option>
            <option value="3" <?php if($output['reply_info']['reply_type'] == 3){ ?>selected<?php } ?>>语音</option>
          </select>
          <a href="javascript:;" id="add_group"><?php echo L('ar_add_group'); ?></a>
          <label for='reply_type' class='error msg_invalid' style='display:none;'></label>
          </span> </div>
      </li>
      <li class="list_item text_reply">
        <label class="label_box">&nbsp;&nbsp;<?php echo L('ar_response').L('nc_colon'); ?></label>
        <div class="label_form">
          <textarea name="reply_content" style="width:350px;height:180px"><?php if($output['reply_info']['reply_type'] == 1){ echo $output['reply_info']['reply_content']; } ?>
</textarea>
        </div>
      </li>
      <div id="voice_container">
      <?php if($output['reply_info']['reply_type'] == 3){ $voice_content = unserialize($output['reply_info']['reply_content']); } ?>
         <li class="list_item">
         	<label class="label_box"><font style="color:red"></font>语音标题：</label>
	        <div class="label_form"> 
	          <span>
	          	<input type="text" name="voice_title" value="<?php echo $voice_content['title']; ?>" class="label_input">
	          </span>     
	        </div>
         </li>
         <li class="list_item">
         	<label class="label_box"><font style="color:red"></font>语音描述：</label>
	        <div class="label_form"> 
	          <textarea name="voice_desc" style="width:350px;height:180px" placeholder="不要超过30个字" ><?php echo $voice_content['desc']; ?></textarea>
	        </div>
         </li>
         <li class="list_item">
         	<label class="label_box"><font style="color:red"></font>普通音质：</label>
         	<input type="hidden" name="voice_type" value="url"/>
	        <div class="label_form"> 
	          <span>
	          	<input type="text" name="voice_url" value="<?php echo $voice_content['url']; ?>" class="label_input" placeholder="普通音质声音文件链接" style='width: 430px'>
	          	<input type="file" name="voice_upload_file" />
	          	<select name="choose_voice">
	          		<option value="0">请选择语音文件</option>
	          		<?php if (!empty($output['voice_list'])) { ?>
	          		<?php foreach ($output['voice_list'] as $val) { ?>
	          		<option value="<?php echo $val['media_url']; ?>"><?php echo $val['media_name']; ?></option>
	          		<?php }} ?>
	          	</select>
	          	&nbsp;&nbsp;<a id="voice_upload" style="cursor: pointer;text-decoration:none">上传</a>&nbsp;&nbsp;<a id="voice_choose" style="cursor: pointer;text-decoration:none">选择</a>&nbsp;&nbsp;<a id="voice_url" style="cursor: pointer;text-decoration:none">链接</a>
	          </span>     
	        </div>
         </li>
         <li class="list_item">
         	<label class="label_box"><font style="color:red"></font>HQ音质：</label>
         	<input type="hidden" name="hq_voice_type" value="url"/>
	        <div class="label_form"> 
	          <span>
	          	<input type="text" name="hq_voice_url" value="<?php echo $voice_content['hqurl']; ?>" class="label_input" placeholder="高品质声音文件链接，WIFI下优先使用（如没有则与上面保持一致）" style='width: 430px'>
	          	<input type="file" name="hq_voice_upload_file" />
	          	<select name="choose_hq_voice">
	          		<option value="0">请选择语音文件</option>
	          		<?php if (!empty($output['voice_list'])) { ?>
	          		<?php foreach ($output['voice_list'] as $val) { ?>
	          		<option value="<?php echo $val['media_url']; ?>"><?php echo $val['media_name']; ?></option>
	          		<?php }} ?>
	          	</select>
	          	&nbsp;&nbsp;<a id="hq_voice_upload" style="cursor: pointer;text-decoration:none">上传</a>&nbsp;&nbsp;<a id="hq_voice_choose" style="cursor: pointer;text-decoration:none">选择</a>&nbsp;&nbsp;<a id="hq_voice_url" style="cursor: pointer;text-decoration:none">链接</a>
	          </span>     
	        </div>
         </li>
         </div>
      <div id="textimg_container">
        <?php if($output['reply_info']['reply_type'] == 2){ $textimg_list = unserialize($output['reply_info']['reply_content']); ?>
        <?php foreach ($textimg_list as $k=>$v){ ?>
        <li class="list_item textimg_reply">
          <label class="label_box"><?php echo L('ar_di'); ?><?php echo $k+1; ?><?php echo L('ar_zu').L('nc_colon'); ?></label>
          <div class="label_form"> <?php echo L('ar_title').L('nc_colon'); ?>
            <input type="text" name="textimg_reply_title[]" class="label_input" value="<?php echo $v['title']; ?>"/>
            <br>
            <br>
            <?php echo L('ar_desc').L('nc_colon'); ?>
            <textarea name="textimg_reply_desc[]" style="width:322px;height:100px"><?php echo $v['desc']; ?></textarea>
            <br>
            <br>
            <?php echo L('ar_image').L('nc_colon'); ?>
            <input type="file" name="textimg_reply_file[]"/>
            <?php if($v['picurl'] != ''){ ?>
            <img src="<?php echo $v['picurl']; ?>" border="0" width="100" height="55"/>
            <?php } ?>
            <input type="hidden" name="ori_reply_pic[]" value="<?php echo $v['picurl']; ?>" />
            <br>
            <br>
            <?php echo L('ar_url').L('nc_colon'); ?>
            <input type="text" name="textimg_reply_url[]" class="label_input" value="<?php echo $v['url']; ?>"/>
          </div>
        </li>
        <?php } ?>
        <?php }else{ ?>
        <li class="list_item textimg_reply">
          <label class="label_box"><?php echo L('ar_di'); ?>1<?php echo L('ar_zu').L('nc_colon'); ?></label>
          <div class="label_form"> <?php echo L('ar_title').L('nc_colon'); ?>
            <input type="text" name="textimg_reply_title[]" class="label_input"/>
            <br>
            <br>
            <?php echo L('ar_desc').L('nc_colon'); ?>
            <textarea name="textimg_reply_desc[]" style="width:322px;height:100px"></textarea>
            <br>
            <br>
            <?php echo L('ar_image').L('nc_colon'); ?>
            <input type="file" name="textimg_reply_file[]"/>
            <br>
            <br>
            <?php echo L('ar_url').L('nc_colon'); ?>
            <input type="text" name="textimg_reply_url[]" class="label_input"/>
          </div>
        </li>
        <?php } ?>
      </div>
  
    </ul>    <div class="btn_bar">
        <input type="submit" class="btn_input" value="<?php echo $lang['nc_save'];?>">
      </div>
  </div>
</form>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.validation.min.js"></script> 
<script type="text/javascript">
$(function(){
	//纯文字
	<?php if($output['reply_info']['reply_type'] == 1){ ?>
	var num = 2;
	$('#textimg_container').hide();
	$('#add_group').hide();
	$('#voice_container').hide();
	<?php }elseif($output['reply_info']['reply_type'] == 3){ ?>
	var num = 2;
	$('.text_reply').hide();
	$('#textimg_container').hide();
	$('#add_group').hide();
	<?php }else{ ?>
	var num = <?php echo count($textimg_list)+1; ?>;
	$('.text_reply').hide();
	$('#voice_container').hide();
	<?php } ?>
	$('select[name="reply_type"]').change(function(){
		if($(this).val() == 1){
			$('.text_reply').show();
			$('#textimg_container').hide();
			$('#add_group').hide();
			$('#voice_container').hide();
		}
		if($(this).val() == 2){
			$('.text_reply').hide();
			$('#textimg_container').show();
			$('#add_group').show();
			$('#voice_container').hide();
		}
		if($(this).val() == 3){
			$('.text_reply').hide();
			$('#textimg_container').hide();
			$('#add_group').hide();
			$('#voice_container').show();
		}
	});
	$('#add_group').click(function(){
		if(num <= 10){
			$('#textimg_container').append('<li class="list_item textimg_reply"><label class="label_box"><?php echo L('ar_di'); ?>'+num+'<?php echo L('ar_zu').L('nc_colon'); ?></label><div class="label_form"><?php echo L('ar_title').L('nc_colon'); ?><input type="text" name="textimg_reply_title[]" class="label_input"/><br><br><?php echo L('ar_desc').L('nc_colon'); ?><textarea name="textimg_reply_desc[]" style="width:322px;height:100px"></textarea><br><br><?php echo L('ar_image').L('nc_colon'); ?><input type="file" name="textimg_reply_file[]"/><br><br><?php echo L('ar_url').L('nc_colon'); ?><input type="text" name="textimg_reply_url[]" class="label_input"/></div></li>');
			num++;
		}
	});
	$("#add_form").validate({
        rules: {
        	reply_title: {
				required:true
            },
            reply_type:{
            	required:true
            }
        },
        messages:{
        	reply_title:{
        		required:'<?php echo L('ar_subject_must_write'); ?>'
            },
            reply_type:{
            	required:'<?php echo L('ar_type_must_choose'); ?>'
            }
        }
	});
	//语音相关
	$('input[name="voice_upload_file"]').hide();
	$('select[name="choose_voice"]').hide();
	$('input[name="hq_voice_upload_file"]').hide();
	$('select[name="choose_hq_voice"]').hide();
	$('#voice_upload').click(function(){
		$('input[name="voice_url"]').hide();
		$('input[name="voice_upload_file"]').show();
		$('select[name="choose_voice"]').hide();
		$('input[name="voice_type"]').val("upload");
	});
	$('#voice_choose').click(function(){
		$('input[name="voice_url"]').hide();
		$('input[name="voice_upload_file"]').hide();
		$('select[name="choose_voice"]').show();
		$('input[name="voice_type"]').val("choose");
	});
	$('#voice_url').click(function(){
		$('input[name="voice_url"]').show();
		$('input[name="voice_upload_file"]').hide();
		$('select[name="choose_voice"]').hide();
		$('input[name="voice_type"]').val("url");
	});
	$('#hq_voice_upload').click(function(){
		$('input[name="hq_voice_url"]').hide();
		$('input[name="hq_voice_upload_file"]').show();
		$('select[name="choose_hq_voice"]').hide();
		$('input[name="hq_voice_type"]').val("upload");
	});
	$('#hq_voice_choose').click(function(){
		$('input[name="hq_voice_url"]').hide();
		$('input[name="hq_voice_upload_file"]').hide();
		$('select[name="choose_hq_voice"]').show();
		$('input[name="hq_voice_type"]').val("choose");
	});
	$('#hq_voice_url').click(function(){
		$('input[name="hq_voice_url"]').show();
		$('input[name="hq_voice_upload_file"]').hide();
		$('select[name="choose_hq_voice"]').hide();
		$('input[name="hq_voice_type"]').val("url");
	});
})
</script>