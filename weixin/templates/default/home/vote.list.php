<div class="main_hd">
  <h2><?php echo $lang['nc_weixin_wall_vote'];?></h2>
  <p class="extra_info"><a href="?act=activity&op=addVote&wx_id=<?php echo intval($_GET['wx_id']);?>"><?php echo $lang['nc_add']?></a> </p>
</div>
<div class="main_bd">
  <div class="table_msg">
    <table cellspacing="0" cellpadding="0">
      <thead class="nc-thead">
        <tr>
          <th class="table_cell time asc"><?php echo $lang['nc_weixin_wall_vote_name'];?></th>
          <th class="table_cell desc"><?php echo $lang['nc_handle'];?></th>
        </tr>
      </thead>
      <tbody class="nc-tbody">
        <?php if(!empty($output['vote'])){?>
        <?php foreach($output['vote'] as $key=>$val){?>
        <tr>
          <td><?php if(mb_strlen($val['vote_name'],'utf-8')>=20){ echo mb_substr($val['vote_name'],0,20,'utf-8').'...';}else{ echo $val['vote_name']; }?></td>
          <td class="last"><a href="?act=wall&op=vote&vote_id=<?php echo $val['vote_id'];?>" target="_blank"><?php echo $lang['nc_weixin_wall_vote_show'];?></a> <span>|</span> <a href="?act=activity&op=editVote&wx_id=<?php echo intval($_GET['wx_id']);?>&vote_id=<?php echo $val['vote_id']?>"><?php echo $lang['nc_edit'];?></a> <span>|</span> <a href="javascript:void(0);" onclick="javascript:deleteitem(<?php echo $val['vote_id'];?>);"><?php echo $lang['nc_delete']?></a> <span>|</span> <a href="?act=activity&op=voteItem&wx_id=<?php echo intval($_GET['wx_id']);?>&vote_id=<?php echo $val['vote_id'];?>"><?php echo $lang['nc_weixin_wall_vote_item_view_add'];?></a></td>
        </tr>
        <?php }?>
        <?php }else{?>
        <tr>
          <td colspan="20" valign="middle" ><div class="norecord"><span><?php echo $lang['nc_record'];?></span></div></td>
        </tr>
        <?php }?>
      </tbody>
    </table>
  </div>
  <div class="pagination"> <?php echo $output['show_page'];?></div>
</div>
<script type="text/javascript">
	function deleteitem(vote_id){
		if(confirm('<?php echo $lang['nc_weixin_confirm_delete_item'];?>')){
			location.href = "?act=activity&op=deleteVote&wx_id=<?php echo intval($_GET['wx_id']);?>&vote_id="+vote_id;
		}
	}
</script>