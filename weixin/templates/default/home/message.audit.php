<div class="main_hd">
  <h2>审核消息</h2>
</div>
<div class="main_bd">
  <form method="post" id="apply_form" action="?act=activity&op=messageAudit&wx_id=<?php echo intval($_GET['wx_id']);?>">
    <ul>
      <li class="list_item">
        <label class="label_box"><?php echo $lang['nc_weixin_wall_message_nick_name'];?>：</label>
        <div class="label_form"> 
          <span><?php echo $output['message']['user_name'];?></span>     
        </div>
      </li>

	  <li class="list_item">
        <label class="label_box"><?php echo $lang['nc_weixin_wall_message_message'];?>：</label>
        <div class="label_form"> 
          <span><?php echo $output['message']['message_content'];?></span>     
        </div>
      </li>

	  <li class="list_item">
        <label class="label_box"><?php echo $lang['nc_weixin_wall_message_state'];?>：</label>
        <div class="label_form"> 
          <span>
			<?php if($output['message']['state'] == 1){	//1.待审核?>
			<?php echo $lang['nc_weixin_wall_message_wait_audit'];?>
			<?php }elseif($output['message']['state'] == 2){	//2.审核通过?>
			<?php echo $lang['nc_weixin_wall_message_yes_audit'];?>
			<?php }else{	//3.审核不通过?>
			<?php echo $lang['nc_weixin_wall_message_no_audit'];?>
			<?php }?>		
		  </span>     
        </div>
      </li>

	  <li class="list_item">
        <label class="label_box"><?php echo $lang['nc_weixin_wall_message_time'];?>：</label>
        <div class="label_form"> 
          <span><?php echo date("Y-m-d H:i",$output['message']['message_time']);?></span>     
        </div>
      </li>

	  <li class="list_item">
        <label class="label_box"><?php echo $lang['nc_weixin_wall_message_audit'];?>：</label>
        <div class="label_form"> 
          <span>
			<input type="radio" name="state" value="2" <?php if(($output['message']['state'] == 1) || ($output['message']['state'] == 2)){ echo 'checked';}?>>&nbsp;<?php echo $lang['nc_weixin_wall_message_yes_audit'];?>&nbsp;&nbsp;
			<input type="radio" name="state" value="3" <?php if($output['message']['state'] == 3){ echo 'checked';}?>>&nbsp;<?php echo $lang['nc_weixin_wall_message_no_audit'];?>
		  </span>     
        </div>
      </li>
	  
      <li>
        <div class="btn_bar">
			<input type="hidden" name="activity_id" value="<?php echo $output['message']['activity_id'];?>">
	  		<input type="hidden" name="message_id" value="<?php echo $output['message']['message_id'];?>">
	  		<input type="submit" class="btn_input" value="<?php echo $lang['nc_save'];?>">
	  	</div>
      </li>
    </ul>
  </form>
</div>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.validation.min.js"></script> 
<script type="text/javascript">
$(function(){
	$("#J_template").toggle(function(){
		$("#C_template").show();
	},function(){
		$("#C_template").hide();
	});

	$(".u_template li").click(function(){
		var data_id = $(this).attr('data-id');
		var nc_html = $(this).html();

		$("#C_template").hide();
		$(".jsBtLabel").html(nc_html);
		$("input[name=activity_template]").val(data_id);
	});
	
	$("#apply_form").validate({
        rules: {
        	activity_name: {
				required:true
            },
            activity_template:{
            	required:true,
            	number:true
            }
        },
        messages:{
        	activity_name:{
        		required:'<?php echo $lang['nc_weixin_wall_activity_name_is_not_null'];?>'
            },
            activity_template:{
            	required:'<?php echo $lang['nc_weixin_wall_activity_template_is_not_null'];?>',
            	number:'<?php echo $lang['nc_weixin_wall_activity_template_format_is_wrong'];?>'
            }
        }
	});
});
</script> 
