<div class="main_hd">
  <h2>商城动态</h2>
</div>
<div class="main_bd">
  <form method="post" id="apply_form" action="?act=article&op=editArticle&wx_id=<?php echo intval($_GET['wx_id']);?>">
    <ul>
      <li class="list_item">
        <label class="label_box"><font style="color:red">*&nbsp;</font>文章标题:</label>
        <div class="label_form"> 
          <span>
          	<input type="text" class="label_input" name="article_title" id="article_title" value="<?php echo $output['article']['article_title'];?>">
            <label for='article_title' class='error msg_invalid' style='display:none;'></label>
          </span>     
        </div>
      </li>
      
      <li class="list_item">
        <label class="label_box">文章内容:</label>
        <div class="label_form">
          <?php showEditor('article_content',$output['article']['article_content'],'550px','300px','','true',false);?>
        </div>
      </li>
      <li>
        <div class="btn_bar">
        	<input type="hidden" name="article_id" value="<?php echo $output['article']['article_id']?>">
        	<input type="submit" class="btn_input" value="<?php echo $lang['nc_save'];?>">
        </div>
      </li>
    </ul>
  </form>
</div>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.validation.min.js"></script> 
<script type="text/javascript">
$(function(){	
	$("#apply_form").validate({//验证表单
        rules: {
        	article_title:{
				required:true
            }
        },
        messages:{
        	article_title:{
        		required:'文章标题不能为空'
            }
        }
	});
});
</script> 