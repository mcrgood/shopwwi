<style>
.list_item,.list_item2{ padding:8px 4px;}
.list_item3{ padding:5px;}
#dy_question .anwser dd,#dy_question .anwser dt{ padding-left:40px;}
</style>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/date/WdatePicker.js"></script>
<div class="main_hd">
  <h2><?php echo '渠道管理';?></h2>
  <p class="extra_info"> <a href="?act=recongition&op=index&wx_id=<?php echo intval($_GET['wx_id']);?>"><?php echo '返回列表';?></a> </p>
</div>
<div class="main_bd">
  <form method="post" id="apply_form" action="?act=recongition&op=add&wx_id=<?php echo intval($_GET['wx_id']);?>">
  	<input type="hidden" name="form_submit" value="ok" />
    <ul>
      <li class="list_item">
        <label class="label_box"><font style="color:red">*&nbsp;</font><?php echo '渠道名称';?></label>
        <div class="label_form"> 
          <span>
          	<input type="text" class="label_input" name="chn_name" id="chn_name">
            <br />
            <span style="color:#666; font-size:12px">请认真填写生成后不可以编辑！只能删除重建</span>
            <br />
            <label for='chn_name' class='error msg_invalid' style='display:none;'></label>
          </span>     
        </div>
      </li>
      <li class="list_item">
        <label class="label_box"><?php echo '关键词';?></label>
        <div class="label_form"> 
          <span>
          	<input type="text" class="label_input" name="chn_key" id="chn_key">
            <br />
            <span style="color:#666; font-size:12px">非必填项，建议留空（留空保留原关注时回复的内容!）</span>
          </span>     
        </div>
      </li>     
      <li>
        <div class="btn_bar">
        <input type="submit" class="btn_input" value="<?php echo $lang['nc_save'];?>"></div>
      </li>
    </ul>
  </form>
</div>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.validation.min.js"></script> 
<script type="text/javascript">
$(function(){	
	$("#apply_form").validate({//验证表单
        rules: {
        	chn_name: {
				required:true
            }
        },
        messages:{
        	chn_name:{
        		required:'<?php echo '渠道名称不能为空';?>'
            }
        }
	});
});

</script> 
