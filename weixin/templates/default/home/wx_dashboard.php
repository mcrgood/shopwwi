<?php defined('InByShopWWI') or exit('Access Invalid!');?>

<div class="nc_area">
  <div class="nc_area_box">
    <div class="nc_tap wt">
      <ul class="nc_inner">
        <li class="nc_area_item nc_activity">
          <a href="#">
           <span class="nc_area_tap">
              <i class="nc_area_icon"></i>
              <em class="number"><?php echo $output['activity_count'];?></em>
              <strong class="title">活动数</strong>
            </span>
          </a>
         </li>
         
            <li class="nc_area_item nc_vote">
          <a href="#">
           <span class="nc_area_tap  no_extra">
              <i class="nc_area_icon"></i>
              <em class="number"><?php echo $output['vote_count'];?></em>
              <strong class="title">投票活动数</strong>
            </span>
          </a>
         </li>

        
          <li class="nc_area_item nc_lottery">
        <a href="#">
          <span class="nc_area_tap">
          <i class="nc_area_icon"></i>
          <em class="number"><?php echo $output['lottery_count'];?></em>
          <strong class="title">抽奖数</strong>
          </span>
          </a>
        </li>
        
        
        
        <li class="nc_area_item nc_fans">
        <a href="#">
          <span class="nc_area_tap no_extra">
          <i class="nc_area_icon"></i>
          <em class="number"><?php echo $output['fans_count'];?></em>
          <strong class="title">粉丝数</strong>
          </span>
          </a>
        </li>
        
      </ul>
    </div>
        
  </div>
</div>
