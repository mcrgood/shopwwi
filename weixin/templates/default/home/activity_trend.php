<style>
.main_bd{ padding:20px 10px;}
.titles{ padding-bottom:10px; color:#888;}
.titles ul{ list-style:none; border-bottom:1px #CCC solid; }
.titles ul li { list-style:none; display:inline-block; margin-bottom:-1px; line-height:25px; margin-left:5px;}
.titles a{ color:#888; text-decoration:none; display:block;  padding:5px 10px; line-height:25px; background:#EEE; border:1px #EEE solid;  border-bottom:1px #CCC solid; }
.titles ul li a.curr{ color:#000; background:#CCC;}
</style>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fushionCharts/JSClass/FusionCharts.js"></script>
<div class="main_hd">
  <h2><?php echo '粉丝行为分析';?></h2>
</div>
<div class="main_bd">
  <div class="titles">
  	<ul>
    	<li><a href="?act=fans_activity&op=index&wx_id=<?php echo intval($_GET['wx_id']);?>">7日行为统计分析</a></li>
        <li><a class="curr" href="?act=fans_activity&op=trend&wx_id=<?php echo intval($_GET['wx_id']);?>">趋势对比</a></li>
    </ul>
  </div>
  <div>
  <div id="chartdiv1" align="center"></div>
  </div>
  <div class="table_msg">
    <table cellspacing="0" cellpadding="0">
      <thead class="nc-thead">
        <tr>
          <th class="table_cell time asc"><?php echo '名称';?></th>
          <th class="table_cell goods"><?php echo '上一周期';?></th>
          <th class="table_cell goods"><?php echo '本周';?></th>
          <th class="table_cell goods"><?php echo '趋势';?></th>
        </tr>
      </thead>
      <tbody class="nc-tbody">
      <?php 
	  	$str = '<categories>';
	  	$prev = '<dataset seriesName="上一周期" color="C8A1D1" plotBorderColor="C8A1D1">';
		$next = '<dataset seriesName="本周期" color="B1D1DC" plotBorderColor="B1D1DC">';
		$end = '</dataset>';
	  ?>
        <?php if(!empty($output['list'])){?>
        <?php foreach($output['list'] as $key=>$val){
			$str .= '<category label="'.$val['act_content'].'"/>';
			$prev .= '<set value="'.$val['num2'].'"/>';
			$next .= '<set value="'.$val['num'].'"/>';
			?>
        <tr>
          <td><?php echo $val['act_content'];?></td>
          <td><?php echo $val['num2'];?></td>
          <td><?php echo $val['num'];?></td>
          <td><?php if($val['num']>$val['num2']){?><font style="color:#F00">↑</font><?php }else{?><font style="color:#0C0">↓</font><?php }?></td>
        </tr>
        <?php }?>
        
        <?php }else{?>
        <tr>
          <td colspan="20" valign="middle" ><div class="norecord"><span><?php echo $lang['nc_record'];?></span></div></td>
        </tr>
        <?php }?>
        <?php 
		$str .='</categories>';
		?>
      </tbody>
    </table>
  </div>
  <div class="pagination"> <?php echo $output['page'];?> </div>
</div>
<script type="text/javascript">
	var chart = new FusionCharts("<?php echo RESOURCE_SITE_URL;?>/js/fushionCharts/Charts/MSArea.swf", "ChartId", "650", "500", "0", "1");
	chart.setDataXML('<chart bgColor="ffffff" outCnvBaseFontColor="666666" caption="7天趋势分析图 xAxisName="模块" yAxisName="数量" showNames="1" showValues="0" plotFillAlpha="50" numVDivLines="10" showAlternateVGridColor="1" bgAlpha="0" showBorder="0"  AlternateVGridColor="e1f5ff" divLineColor="e1f5ff" vdivLineColor="e1f5ff" baseFontColor="666666" baseFontSize="12" borderThickness="0" canvasBorderThickness="0" showPlotBorder="0" plotBorderThickness="0" canvasBorderColor="eeeeee"><?php echo $str.$prev.$end.$next.$end;?></chart>');
	chart.render("chartdiv1");
</script>
<script>
function chkInfo(con){}
</script>
