<?php defined('InByShopWWI') or exit('Access Invalid!');?>
<div class="main_hd">
  <h2><?php echo L('wx_account_edit'); ?></h2>
  <p class="extra_info"><a href="index.php"><?php echo L('wx_account_list'); ?></a></p>
</div>
<form id="add_form" method="post">
<input type="hidden" name="form_submit" value="ok" />
<div class="main_bd">
	<ul>
		<li class="list_item">
	        <label class="label_box"><font style="color:red">*</font><?php echo L('wx_name').L('nc_colon'); ?></label>
	        <div class="label_form"> 
	          <span>
	          	<input type="text" name="wx_name" value="<?php echo $output['wx_info']['wx_name']; ?>" class="label_input">
	          	<label for='wx_name' class='error msg_invalid' style='display:none;'></label>
	          </span>     
	        </div>
         </li>
         <li class="list_item">
	        <label class="label_box"><font style="color:red">*</font><?php echo L('wx_ori_id').L('nc_colon'); ?></label>
	        <div class="label_form"> 
	          <span>
	          	<input type="text" name="wx_ori_id" value="<?php echo $output['wx_info']['wx_ori_id']; ?>" class="label_input">
	          	<label for='wx_ori_id' class='error msg_invalid' style='display:none;'></label>
	          </span>     
	        </div>
         </li>
         <li class="list_item">
	        <label class="label_box"><font style="color:red">*</font><?php echo L('wx_account').L('nc_colon'); ?></label>
	        <div class="label_form"> 
	          <span>
	          	<input type="text" name="wx_account" value="<?php echo $output['wx_info']['wx_account']; ?>" class="label_input">
	          	<label for='wx_account' class='error msg_invalid' style='display:none;'></label>
	          </span>     
	        </div>
         </li>
         <li class="list_item">
	        <label class="label_box"><?php echo L('wx_login_email').L('nc_colon'); ?></label>
	        <div class="label_form"> 
	          <span>
	          	<input type="text" name="wx_email" value="<?php echo $output['wx_info']['wx_email']; ?>" class="label_input">
	          </span>     
	        </div>
         </li>
         <li class="list_item">
	        <label class="label_box"><?php echo L('wx_province').L('nc_colon'); ?></label>
	        <div class="label_form"> 
	          <span>
	          	<input type="text" name="wx_province" value="<?php echo $output['wx_info']['wx_province']; ?>" class="label_input">
	          </span>     
	        </div>
         </li>
         <li class="list_item">
	        <label class="label_box"><?php echo L('wx_city').L('nc_colon'); ?></label>
	        <div class="label_form"> 
	          <span>
	          	<input type="text" name="wx_city" value="<?php echo $output['wx_info']['wx_city']; ?>" class="label_input">
	          </span>     
	        </div>
         </li>
         <li class="list_item">
	        <label class="label_box"><font style="color:red"></font>TOKEN：</label>
	        <div class="label_form"> 
	          <span>
	          	<font style="color:#ccc"><?php echo $output['wx_info']['wx_token']; ?></font>
	          	<span style="margin-left: 10px">重新生成？<input type="radio" name="remake_token" value="1" id="remake_token_1" />&nbsp;<label for="remake_token_1" style="cursor:pointer">是</label>&nbsp;&nbsp;<input type="radio" name="remake_token" value="0" checked id="remake_token_0" />&nbsp;<label for="remake_token_0" style="cursor:pointer">否</label></span>
	          </span>     
	        </div>
         </li>
         <li class="list_item">
	        <label class="label_box"><font style="color:red">*</font>APPID：</label>
	        <div class="label_form"> 
	          <span>
	          	<input type="text" name="wx_appid" value="<?php echo $output['wx_info']['wx_appid']; ?>" class="label_input">
	          	<label for='wx_appid' class='error msg_invalid' style='display:none;'></label>
	          </span>     
	        </div>
         </li>
         <li class="list_item">
	        <label class="label_box"><font style="color:red">*</font>APPSECRET：</label>
	        <div class="label_form"> 
	          <span>
	          	<input type="text" name="wx_appsecret" value="<?php echo $output['wx_info']['wx_appsecret']; ?>" class="label_input">
	          	<label for='wx_appsecret' class='error msg_invalid' style='display:none;'></label>
	          </span>     
	        </div>
         </li>
         <li class="list_item">
	        <label class="label_box"><font style="color:red"></font>数据类型：</label>
	        <div class="label_form"> 
	          <span>
	          	<select name="shop_data_type" >
	          		<option value="1" <?php if($output['wx_info']['shop_data_type'] == 1){ ?>selected<?php } ?> >平台自营</option>
	          		<option value="2" <?php if($output['wx_info']['shop_data_type'] == 2){ ?>selected<?php } ?> >平台商家</option>
	          	</select>
	          	<label for='shop_data_type' class='error msg_invalid' style='display:none;'></label>
	          </span>     
	        </div>
         </li>
         <li class="list_item" id="show_store_id">
	        <label class="label_box"><font style="color:red">*</font>店铺ID：</label>
	        <div class="label_form"> 
	          <span>
	          	<input type="text" name="store_id" value="<?php echo $output['wx_info']['store_id']; ?>" class="label_input" style="width:80px" >
	          	<label for='store_id' class='error msg_invalid' style='display:none;'></label>
	          </span>     
	        </div>
         </li>
         <li>
	        <div class="btn_bar">
	        <input type="submit" class="btn_input" value="<?php echo $lang['nc_save'];?>"></div>
	     </li>
	</ul>
</div>
</form>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.validation.min.js"></script>
<script type="text/javascript">
$(function(){
	<?php if($output['wx_info']['shop_data_type'] == 1){ ?>
	$('#show_store_id').hide();
	<?php } ?>
	$('select[name="shop_data_type"]').change(function(){
		var type = $(this).val();
		if(type == "1"){
			$('#show_store_id').hide();
		} else {
			$('#show_store_id').show();
		}
	});
	$("#add_form").validate({
        rules: {
        	wx_name: {
				required:true
            },
            wx_ori_id:{
            	required:true
            },
            wx_account:{
            	required:true
            },
            wx_appid:{
            	required:true
            },
            wx_appsecret:{
            	required:true
            }
        },
        messages:{
        	wx_name:{
        		required:'<?php echo L('wx_account_name_must_write'); ?>'
            },
            wx_ori_id:{
            	required:'<?php echo L('wx_ori_id_must_write'); ?>'
            },
            wx_account:{
            	required:'<?php echo L('wx_account_must_write'); ?>'
            },
            wx_appid:{
            	required:'<?php echo L('wx_appid_must_write'); ?>'
            },
            wx_appsecret:{
            	required:'<?php echo L('wx_appsecret_must_write'); ?>'
            }
        }
	});
})
</script>