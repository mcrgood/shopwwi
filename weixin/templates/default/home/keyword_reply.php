<?php defined('InByShopWWI') or exit('Access Invalid!');?>

<div class="main_hd">
  <h2><?php echo L('ar_kw_autoreply'); ?></h2>
  <p class="extra_info"><a href="index.php?act=autoreply&op=keyword_reply_add&wx_id=<?php echo intval($_GET['wx_id']); ?>"><?php echo L('nc_add'); ?></a></p>
</div>
<div class="main_bd">
  <div class="zt"> <span><?php echo L('ar_subject').L('nc_colon'); ?></span>
    <input name="s_title" type="text" class="label_input_zt" value="<?php echo trim($_GET['s_title']); ?>"/>
    <input type="button" class="search-button" id="search" />
  </div>
  <div class="table_msg">
    <table cellspacing="0" cellpadding="0">
      <thead class="nc-thead">
        <tr>
          <th class="table_cell" style="width:30%"><?php echo L('ar_rule_name'); ?></th>
          <th class="table_cell" style="width:10%"><?php echo L('nc_status'); ?></th>
          <th class="table_cell" style="width:25%"><?php echo L('ar_keyword'); ?></th>
          <th class="table_cell" style="width:15%"><?php echo L('ar_addtime'); ?></th>
          <th class="table_cell" style="width:15%"><?php echo L('nc_handle'); ?></th>
        </tr>
      </thead>
      <tbody class="nc-tbody">
        <?php if(!empty($output['kr_list'])){?>
        <?php foreach($output['kr_list'] as $key=>$val){?>
        <tr>
          <td><?php echo $val['kr_title']; ?></td>
          <td><?php echo $val['kr_use']==1?L('nc_open'):L('nc_not_open'); ?></td>
          <td><?php echo ltrim(trim($val['kr_keyword'],','),','); ?></td>
          <td><?php echo date('Y-m-d H:i:s',$val['kr_addtime']); ?></td>
          <td class="last"><a href="index.php?act=autoreply&op=keyword_reply_edit&kr_id=<?php echo $val['kr_id']; ?>&wx_id=<?php echo intval($_GET['wx_id']); ?>"><?php echo L('nc_edit'); ?></a> <span>|</span> <a href="javascript:if(confirm('<?php echo L('ar_del_kwautoreply_confirm'); ?>'))window.location.href='index.php?act=autoreply&op=keyword_reply_del&kr_id=<?php echo $val['kr_id']; ?>&wx_id=<?php echo intval($_GET['wx_id']); ?>';"><?php echo L('nc_delete'); ?></a></td>
        </tr>
        <?php }?>
        <?php }else { ?>
        <tr>
          <td colspan="15" style="text-align:center"><?php echo L('ar_no_record'); ?></td>
        </tr>
        <?php } ?>
      </tbody>
    </table>
  </div>
  <div class="pagination"> <?php echo $output['show_page'];?> </div>
</div>
<script type="text/javascript">
$(function(){
	$('#search').click(function(){
		var s_title = $('input[type="text"][name="s_title"]').val();
		window.location.href = 'index.php?act=autoreply&op=keyword_reply&wx_id=<?php echo intval($_GET['wx_id']); ?>&s_title='+s_title;
	});
})
</script>