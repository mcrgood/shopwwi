<?php defined('InByShopWWI') or exit('Access Invalid!');?>
<div class="main_hd">
  <h2><?php echo L('msg_manage'); ?></h2>
</div>
<div class="main_bd">
<div class="zt">
<span><?php echo L('msg_nickname').L('nc_colon'); ?></span><input class="label_input_zt" style="width:120px;" name="s_nickname" type="text" value="<?php echo trim($_GET['s_nickname']); ?>"/>
<span><?php echo L('msg_content').L('nc_colon'); ?></span><input  class="label_input_zt" style="width:120px;" name="s_content" type="text" value="<?php echo trim($_GET['s_content']); ?>"/>
<span style="margin-right:15px;">
<input name="s_important" id="s_important" value="1" type="checkbox" <?php if(intval($_GET['s_important']) == 1){ ?>checked<?php } ?>/>
<label for="s_important" style="cursor:pointer"><?php echo L('msg_important_msg'); ?></label></span>
<input type="button" class="search-button" id="search" />
</div>
<div class="table_msg">
  <table cellspacing="0" cellpadding="0"> 
    <thead class="nc-thead">
      <tr>
        <th class="table_cell" style="width:15%"><?php echo L('msg_nickname'); ?></th>
        <th class="table_cell" style="width:30%"><?php echo L('msg_content'); ?></th>
        <th class="table_cell" style="width:15%"><?php echo L('msg_send_time'); ?></th>
        <th class="table_cell" style="width:10%"><?php echo L('msg_important'); ?></th>
        <th class="table_cell" style="width:18%"><?php echo L('nc_handle'); ?></th>
      </tr>
    </thead>
    <tbody class="nc-tbody">
		<?php if(!empty($output['msg_list'])){?>
		<?php foreach($output['msg_list'] as $key=>$val){?>
		<tr>
		  <td><?php echo $val['user_name']; ?></td>
		  <td><?php echo $val['message_content']; ?></td>
		  <td><?php echo date('Y-m-d H:i:s',$val['message_time']); ?></td>
		  <td><?php echo $val['is_important']==1?L('nc_yes'):L('nc_no'); ?></td>
		  <td class="last"><a href="index.php?act=msg&op=send_msg&wx_id=<?php echo intval($_GET['wx_id']); ?>&fans_openid=<?php echo $val['user_openid']; ?>"><?php echo L('msg_send_msg'); ?></a>
          <span>|</span>
          <a href="index.php?act=msg&op=set_important&message_id=<?php echo intval($val['message_id']); ?>&important_status=<?php echo $val['is_important']; ?>&wx_id=<?php echo intval($_GET['wx_id']); ?>"><?php if($val['is_important']==1){ echo L('msg_cancel_important'); }else{ echo L('msg_set_important'); } ?></a></td>
		</tr>
		<?php }?>
		<?php }else { ?>
        <tr>
          <td colspan="15" style="text-align:center"><?php echo L('msg_no_record'); ?></td>
        </tr>
        <?php } ?>
    </tbody>  
  </table>
 
</div> <div class="pagination"> <?php echo $output['show_page'];?> </div>
</div>
<script type="text/javascript">
$(function(){
	$('#search').click(function(){
		var s_nickname = $('input[type="text"][name="s_nickname"]').val();
		var s_content = $('input[type="text"][name="s_content"]').val();
		var s_important = $('input[type="checkbox"][name="s_important"]:checked').val();
		var url = 'index.php?act=msg&op=msg_manage&wx_id=<?php echo intval($_GET['wx_id']); ?>&s_content='+s_content+'&s_nickname='+s_nickname;
		if(s_important == 1){
			url += '&s_important=1';
		}
		window.location.href = url;
	});
})
</script>