<style>
.list_item,.list_item2{ padding:8px 4px;}
.list_item3{ padding:5px;}
#dy_question .anwser dd,#dy_question .anwser dt{ padding-left:40px;}
</style>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/date/WdatePicker.js"></script>
<div class="main_hd">
  <h2><?php echo '微调研活动';?></h2>
  <p class="extra_info"> <a href="?act=research&op=list&wx_id=<?php echo intval($_GET['wx_id']);?>"><?php echo '返回列表';?></a> </p>
</div>
<div class="main_bd">
  <form method="post" enctype="multipart/form-data" id="apply_form" action="?act=research&op=add_research&wx_id=<?php echo intval($_GET['wx_id']);?>">
  	<input type="hidden" name="form_submit" value="ok" />
    <ul>
      <li class="list_item">
        <label class="label_box"><font style="color:red">*&nbsp;</font><?php echo '调研标题';?></label>
        <div class="label_form"> 
          <span>
          	<input type="text" class="label_input" name="act_name" id="act_name">
            <label for='act_name' class='error msg_invalid' style='display:none;'></label>
          </span>     
        </div>
      </li>
      <li class="list_item">
        <label class="label_box"><font style="color:red">*&nbsp;</font><?php echo '关键词';?></label>
        <div class="label_form"> 
          <span>
          	<input type="text" class="label_input" name="act_keywords" id="act_keywords">
            <label for='act_keywords' class='error msg_invalid' style='display:none;'></label>
          </span>     
        </div>
      </li>
      <li class="list_item">
        <label class="label_box"><font style="color:red">*&nbsp;</font><?php echo '宣传图片';?></label>
        <div class="label_form"> 
          <span>
          	<input type="file" name="act_image" id="act_image">
            <label for='act_image' class='error msg_invalid' style='display:none;'></label>
          </span>     
        </div>
      </li>
      <li class="list_item">
        <label class="label_box"><font style="color:red">*&nbsp;</font><?php echo '开始时间';?></label>
        <div class="label_form"> 
          <span>
          	<input type="text" readonly="readonly" class="label_input" name="start_time" id="start_time" onClick="WdatePicker({dateFmt:'yyyy-MM-dd  HH:mm:ss'})">
            <label for='start_time' class='error msg_invalid' style='display:none;'></label>
          </span>     
        </div>
      </li>
      <li class="list_item">
        <label class="label_box"><font style="color:red">*&nbsp;</font><?php echo '结束时间';?></label>
        <div class="label_form"> 
          <span>
          	<input type="text" readonly="readonly" class="label_input" name="end_time" id="end_time" onClick="WdatePicker({dateFmt:'yyyy-MM-dd  HH:mm:ss'})">
            <label for='end_time' class='error msg_invalid' style='display:none;'></label>
          </span>     
        </div>
      </li>
      <li class="list_item">
        <label class="label_box"><font style="color:red">*&nbsp;</font><?php echo '调研说明';?></label>
        <div class="label_form"> 
          <?php showEditor('act_content','','550px','250px','','true',false);?>     
        </div>
      </li>
      <li class="list_item2">
      	<ul id="dy_question">
        	
        </ul>
        <div><a href="javascript:void(0);" id="addq">添加问题</a></div>
      </li>      
      <li>
        <div class="btn_bar">
        <input type="submit" class="btn_input" value="<?php echo $lang['nc_save'];?>"></div>
      </li>
    </ul>
  </form>
</div>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.validation.min.js"></script> 
<script type="text/javascript">
var i = 0;
var flag = 0;
var a = new Array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z");
$(function(){	
	dateformat('start_time', 0);
	dateformat('end_time', 7);
	addquestion();
	$('#addq').click(function(){
		if(flag < 25){
			addquestion();
		}else{
			alert('最多可以添加25题');
		}
	});
	$("#apply_form").validate({//验证表单
        rules: {
        	act_name: {
				required:true
            },
			act_keywords: {
				required:true
            },
			act_image:{
				required:true
			},
			start_time: {
				required:true
            },
			end_time: {
				required:true
            },
			act_content: {
				required:true
            }
        },
        messages:{
        	act_name:{
        		required:'<?php echo '调研标题不能为空';?>'
            },
			act_keywords: {
				required:'<?php echo '调研关键词不能为空';?>'
            },
			act_image:{
				required:'<?php echo '调研宣传图片不能为空';?>'
			},
			start_time: {
				required:'<?php echo '调研开始时间不能为空';?>'
            },
			end_time: {
				required:'<?php echo '调研结束时间不能为空';?>'
            },
			act_content: {
				required:'<?php echo '调研说明不能为空';?>'
            }
        }
	});
});
//添加问题
function addquestion(){
	i += 1;
	flag = $('#dy_question').find('li').size()+1;
	var dmax = 4;
	var html = '<li class="list_item2" list_id="'+i+'">'
				+'<div class="question"><label>问题'+flag+'：</label><input type="text" placeholder="请输入问题" value="" class="label_input" name="dywt['+i+'][question]" id="dywt_'+i+'" />'
				+ '&nbsp;&nbsp;&nbsp;&nbsp;<input name="dywt['+i+'][chkm]" type="checkbox" value="1" />&nbsp;可以多选 &nbsp; &nbsp; <a href="javascript:void(0)" onClick="del('+i+')">删除</a></div>'
				+ '<div class="anwser"><dd><input type="hidden" name="anws_'+i+'" value="'+dmax+'" />';
	for(var kk=0; kk < dmax; kk++){
		html += '<dl class="list_item3" anws_id="'+i+'_'+kk+'"><span>'+a[kk]+'&nbsp;选项：</span><input type="text" class="label_input" name="dywt['+i+'][anwser]['+kk+']" id="dywt_'+i+'_'+kk+'" value="" /> &nbsp;&nbsp;<a href="javascript:void(0);" onClick="delitem('+i+','+kk+')">删除</a></dl>';
	}
	html += '</dd><dt><a href="javascript:void(0);" onClick="adda('+i+')">添加选项</a><dt></div></li>';
	$('#dy_question').append(html);
				
}
//添加选项
function adda(m){
	var mm = $('input[name=anws_'+m+']').val();
	var mn = $('#dy_question li[list_id='+m+']').find('dl').size();
	var html = '<dl class="list_item3" anws_id="'+m+'_'+mm+'"><span>'+a[mn]+'&nbsp;选项：</span><input type="text" class="label_input" name="dywt['+m+'][anwser]['+mm+']" id="dywt_'+m+'_'+mm+'" value="" /> &nbsp;&nbsp;<a href="javascript:void(0);" onClick="delitem('+m+','+mm+')">删除</a></dl>';
	$('#dy_question li[list_id='+m+'] dd').append(html);
	$('input[name=anws_'+m+']').val(parseInt(mm)+1);
}
//删除选项
function delitem(n,m){
	if($('#dy_question li[list_id='+n+']').find('dl').size()>2){
		$('#dy_question li[list_id='+n+']').find('dl[anws_id='+n+'_'+m+']').remove();
		var k = 0;
		$('#dy_question li[list_id='+n+'] dl').each(function(){
			$(this).find('span').html(a[k]+'&nbsp;选项：');
			k++;
		});
	}else{
		alert('至少保留两个选项');
	}
}
//删除问题
function del(n){
	if($('#dy_question').find('li').size()>1){
		$('#dy_question').find('li[list_id='+n+']').remove();
		var k = 0;
		$('#dy_question li').each(function(){
			k++;
			$(this).find('label').html('问题'+k+'：');
		});
	}else{
		alert('至少保留一个问题');
	}
}
//时间格式化
function dateformat(id, day){
 var date = new Date();
 date.setDate(date.getDate()+day);
 var deadline = date.getFullYear()+'-'+(date.getMonth()+1)+'-'+date.getDate()+' '+checkTime(date.getHours())+':'+checkTime(date.getMinutes())+':'+checkTime(date.getSeconds());
 $("#" + id).val(deadline);
}
function checkTime(i){
	if (i<10){
		i="0" + i;
	}
  return i;
}
</script> 
