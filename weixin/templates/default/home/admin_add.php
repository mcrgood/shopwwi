<?php defined('InByShopWWI') or exit('Access Invalid!');?>
<div class="main_hd">
  <h2><?php echo L('wx_admin_add_admin'); ?></h2>
  <p class="extra_info"><a href="index.php?act=admin"><?php echo L('wx_admin_admin_account_list'); ?></a></p>
</div>
<form id="add_form" method="post">
<input type="hidden" name="form_submit" value="ok" />
<div class="main_bd">
	<ul>
		<li class="list_item">
	        <label class="label_box"><font style="color:red">*</font><?php echo L('wx_admin_account').L('nc_colon'); ?></label>
	        <div class="label_form"> 
	          <span>
	          	<input type="text" name="admin_name" value="" class="label_input">
	          	<label for='admin_name' class='error msg_invalid' style='display:none;'></label>
	          </span>     
	        </div>
         </li>
         <li class="list_item">
	        <label class="label_box"><font style="color:red">*</font><?php echo L('wx_admin_password').L('nc_colon'); ?></label>
	        <div class="label_form"> 
	          <span>
	          	<input type="password" name="admin_password" value="" class="label_input">
	          	<label for='admin_password' class='error msg_invalid' style='display:none;'></label>
	          </span>     
	        </div>
         </li>
         <li class="list_item">
	        <label class="label_box"><font style="color:red">*</font><?php echo L('wx_admin_truename').L('nc_colon'); ?></label>
	        <div class="label_form"> 
	          <span>
	          	<input type="text" name="admin_truename" value="" class="label_input">
	          	<label for='admin_truename' class='error msg_invalid' style='display:none;'></label>
	          </span>     
	        </div>
         </li>
         <li class="list_item">
	        <label class="label_box"><?php echo L('wx_admin_manage').L('nc_colon'); ?></label>
	        <div class="label_form"> 
	          <span>
	          	<?php if(!empty($output['wx_list'])){ ?>
				<?php foreach($output['wx_list'] as $key=>$val){?>
				<input type="checkbox" name="choose_wx[]" value="<?php echo $val['wx_id']; ?>" <?php if($val['admin_id'] > 0){ ?>disabled<?php } ?>>&nbsp;<font <?php if($val['admin_id'] > 0){ ?>style="color:#ccc"<?php } ?>><?php echo $val['wx_name']; ?></font><?php if($val['shop_data_type'] == 1){ ?><font style="color:red">（平台）</font><?php } ?><br>
				<?php }?>
				<?php } ?>
	          </span>     
	        </div>
         </li>
         <li>
	        <div class="btn_bar">
	        <input type="submit" class="btn_input" value="<?php echo $lang['nc_save'];?>"></div>
	     </li>
     </ul>
</div>
</form>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.validation.min.js"></script>
<script type="text/javascript">
$(function(){
	$("#add_form").validate({
        rules: {
        	admin_name: {
				required:true
            },
            admin_password:{
            	required:true
            },
            admin_truename:{
            	required:true
            }
        },
        messages:{
        	admin_name:{
        		required:'<?php echo L('wx_admin_admin_account_must_write'); ?>'
            },
            admin_password:{
            	required:'<?php echo L('wx_admin_admin_password_must_write'); ?>'
            },
            admin_truename:{
            	required:'<?php echo L('wx_admin_admin_truename_must_write'); ?>'
            }
        }
	});
})
</script>