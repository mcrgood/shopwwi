<?php defined('InByShopWWI') or exit('Access Invalid!');?>
<div class="main_hd">
  <h2>修改密码</h2>
  <p class="extra_info"><a href="index.php?act=admin">管理员列表</a></p>
</div>
<form id="add_form" method="post">
<input type="hidden" name="form_submit" value="ok" />
<div class="main_bd">
	<ul>
		<li class="list_item">
	        <label class="label_box"><font style="color:red">*</font>原密码：</label>
	        <div class="label_form"> 
	          <span>
	          	<input type="password" name="ori_password" value="" class="label_input">
	          	<label for='ori_password' class='error msg_invalid' style='display:none;'></label>
	          </span>     
	        </div>
         </li>
         <li class="list_item">
	        <label class="label_box"><font style="color:red">*</font>新密码：</label>
	        <div class="label_form"> 
	          <span>
	          	<input type="password" name="new_password" value="" class="label_input">
	          	<label for='new_password' class='error msg_invalid' style='display:none;'></label>
	          </span>     
	        </div>
         </li>
         <li class="list_item">
	        <label class="label_box"><font style="color:red">*</font>重复：</label>
	        <div class="label_form"> 
	          <span>
	          	<input type="password" name="re_new_password" value="" class="label_input">
	          	<label for='re_new_password' class='error msg_invalid' style='display:none;'></label>
	          </span>     
	        </div>
         </li>
         <li>
	        <div class="btn_bar">
	        <input type="submit" class="btn_input" value="<?php echo $lang['nc_save'];?>"></div>
	     </li>
     </ul>
</div>
</form>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.validation.min.js"></script>
<script type="text/javascript">
$(function(){
	$("#add_form").validate({
        rules: {
        	ori_password: {
				required:true
            },
            new_password:{
            	required:true
            },
            re_new_password:{
            	required:true
            }
        },
        messages:{
        	ori_password:{
        		required:'原密码必须填写'
            },
            new_password:{
            	required:'新密码必须填写'
            },
            re_new_password:{
            	required:'重复新密码必须填写'
            }
        }
	});
})
</script>