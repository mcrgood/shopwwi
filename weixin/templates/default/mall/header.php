<?php if($_GET['op'] == 'index'){?>
<header class="clearfix home top_page">
	<div class="header_wp">
  <div class="center_box">
    <h1 class="top_title"><?php echo $output['site']['site_name'];?></h1>
  </div>
  <div class="right_box"> <span class="quick-nav c-icon"></span> </div>
  </div>
</header>
<?php }else{?>
<header class="clearfix top_page">
  <div class="header_wp">
      <div class="left_box ml10">
        <a href="javascript:history.go('-1');" class="btn_back c-icon mt10">&nbsp;</a>
      </div>
      <div class="center_box">
        <h1 class="top_title mt10"><?php echo $output['site']['site_name'];?></h1>
      </div>
      <div class="right_box">
        <span class="quick-nav c-icon mt10"></span>
      </div>
  </div>
</header>
<?php }?>
<div class="quick_nav_pannel">
    <div class="qkswipe-wrap">
      <div class="qkswipe-item">
        <a href="javascript:;" class="navlink" link="index.php?act=mall&wx_id=<?php echo intval($_GET['wx_id']);?>">商城首页</a>
        <a href="javascript:;" class="navlink" link="index.php?act=mall&op=list&wx_id=<?php echo intval($_GET['wx_id']);?>"><?php echo $lang['nc_mall_nav_goodslist'];?></a>
        <a href="javascript:;" class="navlink" link="index.php?act=mall&op=articlelist&wx_id=<?php echo intval($_GET['wx_id']);?>"><?php echo $lang['nc_mall_nav_mallnews'];?></a>
      </div>
      <div class="qkswipe-item">
        <a href="javascript:;" class="navlink" link="index.php?act=mall&op=articledetail&wx_id=<?php echo intval($_GET['wx_id']);?>&type=3"><?php echo $lang['nc_mall_nav_mallintro'];?></a>
        <a href="javascript:;" class="navlink" link="index.php?act=mall&op=articledetail&wx_id=<?php echo intval($_GET['wx_id']);?>&type=1"><?php echo $lang['nc_mall_nav_contactus'];?></a>
        <a href="javascript:;" class="navlink" link="index.php?act=mall&op=articledetail&wx_id=<?php echo intval($_GET['wx_id']);?>&type=2"><?php echo $lang['nc_mall_nav_aboutus'];?></a>
      </div>
    </div>
    <div class="qkwipe-sign"></div>
</div> 

<script type="text/javascript">
	$(function(){
		$('.navlink').tap(function(){
			location.href = $(this).attr('link');
		});
	});
</script>