<?php include("header.php");?>
<div id="mySwipe" class="swiper-container mt2" >
  <div class="swipe-wrap">
	<?php if(!empty($output['adv'])){?>  
	<?php foreach($output['adv'] as $adv){?>
    <div class="swipe-item"><img src="<?php echo UPLOAD_SITE_URL;?>/adv/<?php echo $adv['adv_pic'];?>"></div>
    <?php }?>
	<?php }?>
  </div>
  <div class="swipe-paginat"></div>
</div>

<div class="content-wrap">
	<div class="griditem">
	<?php for($i=0;$i<2;$i++){?>
		<dl class="list-item" product_id="<?php echo $output['product'][$i]['goods_id'];?>">
				<dt class="pic"> <img src="<?php echo $output['product'][$i]['goods_image'];?>"> </dt>
				<dd class="goods_name"><?php echo $output['product'][$i]['goods_name'];?></dd>
				<dd class="pricebox clearfix"><span class="grallyprice fl">¥<?php echo $output['product'][$i]['goods_store_price'];?></span></dd>		
		</dl>
	<?php }?>
	</div>		

	<div class="griditem">
	<?php for($i=2;$i<4;$i++){?>
		<dl class="list-item" product_id="<?php echo $output['product'][$i]['goods_id'];?>">
				<dt class="pic"> <img src="<?php echo $output['product'][$i]['goods_image'];?>"> </dt>
				<dd class="goods_name"><?php echo $output['product'][$i]['goods_name'];?></dd>
				<dd class="pricebox clearfix"><span class="grallyprice fl">¥<?php echo $output['product'][$i]['goods_store_price'];?></span></dd>			
		</dl>
	<?php }?>
	</div>		

	<div class="griditem">
	<?php for($i=4;$i<6;$i++){?>
		<dl class="list-item" product_id="<?php echo $output['product'][$i]['goods_id'];?>">	
				<dt class="pic"> <img src="<?php echo $output['product'][$i]['goods_image'];?>"> </dt>
				<dd class="goods_name"><?php echo $output['product'][$i]['goods_name'];?></dd>
				<dd class="pricebox clearfix"><span class="grallyprice fl">¥<?php echo $output['product'][$i]['goods_store_price'];?></span></dd>			
		</dl>
	<?php }?>
	</div>		
	
	<div class="nav-list">
		<a class="w1" href="javascript:;"><span><?php echo $lang['nc_mall_nav_goodslist'];?></span></a>
		<a class="w2" href="javascript:;"><span><?php echo $lang['nc_mall_nav_mallnews'];?></span></a>
		<a class="w3" href="javascript:;"><span><?php echo $lang['nc_mall_nav_mallintro'];?></span></a>
	</div>
	
	<div class="nav-list">
		<a class="w4" href="javascript:;"><span><?php echo $lang['nc_mall_nav_contactus'];?></span></a>
		<a class="w5" href="javascript:;"><span><?php echo $lang['nc_mall_nav_aboutus'];?></span></a>
		<a class="w6" href="javascript:;"><span><?php echo $lang['nc_mall_nav_more'];?></span></a>
	</div>
	</div>
	<script type="text/javascript">
		$(function (){
			$("#search_button").tap(function(){
				var keyword = encodeURIComponent($('#search_keyword').val());
				location.href = "index.php?act=mall&op=list&wx_id="+'<?php echo intval($_GET['wx_id'])?>'+'&keyword='+keyword;
			});

			$(".list-item").tap(function(){
				location.href = '<?php echo $output['site']['mall_url'];?>/index.php?act=goods&goods_id='+$(this).attr('product_id');
			});

			$('.w1').tap(function(){
				location.href = '?act=mall&op=list&wx_id='+'<?php echo intval($_GET['wx_id']);?>';
			});
			
			$('.w2').tap(function(){
				location.href = '?act=mall&op=articlelist&wx_id=<?php echo intval($_GET['wx_id']);?>';
			});

			$('.w3').tap(function(){
				location.href = '?act=mall&op=articledetail&wx_id=<?php echo intval($_GET['wx_id']);?>&type=3';
			});

			$('.w4').tap(function(){
				location.href = '?act=mall&op=articledetail&wx_id=<?php echo intval($_GET['wx_id']);?>&type=1';
			});

			$('.w5').tap(function(){
				location.href = '?act=mall&op=articledetail&wx_id=<?php echo intval($_GET['wx_id']);?>&type=2';
			});

			$('.w6').tap(function(){
				location.href = '?act=mall&op=articlelist&wx_id=<?php echo intval($_GET['wx_id']);?>';
			});			
			
			addPaginat();
			function addPaginat(){
				var swipeSpan= '<span class="swipe-paginat-switch current"></span>';
				var swipeItem = $("#mySwipe .swipe-item");
				for(var i = 1;i< swipeItem.length;i++){
					swipeSpan += '<span class="swipe-paginat-switch"></span>';
				}
				$(".swipe-paginat").html(swipeSpan);
			}
			// pure JS
			var elem = $("#mySwipe")[0];
			window.mySwipe = Swipe(elem, {
			  auto: 3000,
			  continuous: true,
			  disableScroll: true,
			  stopPropagation: true,
			  callback: function(index, element) {
			  	var paginat = $(".swipe-paginat-switch");
			  	paginat.eq(index).addClass("current").siblings().removeClass("current");
			  }
			});
			//搜索
			$(".quick-s").click(function(){
				$(".s_wrapper").show();
			});
			$(".close").click(function (){
				$(".s_wrapper").hide();
			});
		})
	</script>
