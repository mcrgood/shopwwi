define(function(require, exports, module){
	//引用相关模块
	var $ = require('lib/zepto/zepto'),
		$ = require('lib/zepto/touch'),
		iScroll=require('lib/iscroll/iscroll.js'),
		aImg = require('modules/imgsrc/main'),
		oImages=[],i,n=0,num,len;
	//正确答案
	var aAns=['b','d','d','c','a','a'],
		score=1;
	//对外提供接口
	module.exports = {
		//初始化
		init : function () {
			this.loading();//预加载
			this.introscroll();//介绍页
			this.startDo();//开始答题
			this.switchTab();//切换题目
		},
		loading:function(){
			for (i=0,len=aImg.src.length;i<len;i++){
				(function(i){
					oImages[i]=new Image()
					oImages[i].onload=function(){
						n++;
						num=parseInt((n/len*100).toFixed(2));
						if(n>=len){
							$(".J-load").removeClass("l-load-show");
							setTimeout(function(){$(".J-index").addClass("l-animation");},100)
						}
					};
					oImages[i].src=aImg.src[i];
				})(i);
			}
		},
		startDo:function(){
			$(".J-start-btn").on("tap",function(){
				$(".J-qus").eq(0).addClass("l-ani-up");
			});
		},
		switchTab:function(){
			$("body").on("tap",".J-ans-sel",function(){
				$(".J-error").css("opacity",0);
				if($(this).find('input').attr('type') == 'checkbox'){
					$(this).find('input').attr('checked') ? $(this).find('input').removeAttr('checked') : $(this).find('input').attr('checked','checked');
				}		
				if($(this).find('input').attr('type') == 'radio'){					
					$(this).siblings(".J-ans-sel").find('input').removeAttr('checked');
					$(this).find('input').attr('checked','checked');
				}
			});
			$("body").on("tap",".J-next",function(){
				var oPa=$(this).parent(),
					isSel=false,
					self=$(this);
				oPa.find("input").each(function(){
					$(this).attr('checked') && (isSel=true)
				});
				if(isSel){
					$(this).parent(".J-qus").next(".J-qus").addClass("l-ani-up");
					if($(this).hasClass("J-tj")){
						//计算成绩
						$('form').submit();
					}
				}else{
					$(this).prev(".J-error").animate({
						'opacity':1
					},50,"linear",function(){
						setTimeout(function(){
							self.prev(".J-error").css({'opacity':0});
						},2000);
					});
					return;
				}
			});
			$('body').on("tap","#btn_sub",function(){
				$('form').submit();
			});
		},
		introscroll:function(){
			if ($("#J-intro-scroll").length>0){
				new iScroll('#J-intro-scroll', { mouseWheel: true,click:true});
			}
		}
	}
});