<?php
/**
 * 缓存操作
 *
 * @copyright  Copyright (c) 2007-2013 ShopWWI Inc. (http://www.shopwwi1.com)
 * @license    http://www.shopwwi2.com
 * @link       http://www.shopwwi.com
 * @since      File available since Release v1.1
 */
defined('InByShopWWI') or exit('Access Invalid!');
class cacheModel extends Model {
	
	public function __construct(){
		parent::__construct();
	}

	public function call($method){
		$method = '_'.strtolower($method);
		if (method_exists($this,$method)){
			return $this->$method();
		}else{
			return false;
		}
	}

	/**
	 * 基本设置
	 *
	 * @return array
	 */
	private function _setting(){
		$list =$this->table('setting')->where(true)->select();
		$array = array();
		foreach ((array)$list as $v) {
			$array[$v['name']] = $v['value'];
		}
		unset($list);
		return $array;
	}

	/**
	 * 商品分类（到三级目录）
	 *
	 * @return array
	 */
	private function _goods_class(){
		$fields = 'gc_id,gc_name,gc_parent_id,gc_sort,gc_show,gc_image';
	    $result = $this->table('goods_class')->field($fields)->where('gc_show=1')->order('gc_parent_id asc, gc_sort asc')->limit(10000)->select();
	    if (!is_array($result)) return null;
		$goods_class = array();
		if(!empty($result)){
			foreach ($result as $k=>$v){
				if($v['gc_parent_id'] == 0){
					$goods_class[$v['gc_id']] = $v;
					$goods_class[$v['gc_id']]['child'] = array();
					foreach ($result as $rk=>$rv){
						if($rv['gc_parent_id'] == $v['gc_id']){
							$data_arr = array('gc_id'=>$rv['gc_id'],'gc_name'=>$rv['gc_name'],'child'=>array());
							foreach ($result as $rrk=>$rrv){
								if($rrv['gc_parent_id'] == $rv['gc_id']){
									$data_arr['child'][] = array('gc_id'=>$rrv['gc_id'],'gc_name'=>$rrv['gc_name']);
								}
							}
							$goods_class[$v['gc_id']]['child'][] = $data_arr;
							unset($data_arr);
						}
					}
				}else{
					continue;
				}
			}
		}
		return $goods_class;
	}
	/**
	 * 全部分类
	 * 
	 * @return array
	 */
	private function _goods_class_all(){
		$fields = 'gc_id,gc_name,gc_parent_id,gc_sort,gc_show,gc_image';
	    $result = $this->table('goods_class')->field($fields)->where('gc_show=1')->order('gc_parent_id asc, gc_sort asc')->limit(10000)->select();
	    if (!is_array($result)) return null;
	    //处理数组
	    $goods_class_all = array();
	    foreach ($result as $k=>$v){
	    	$goods_class_all[$v['gc_id']] = $v;
	    }
	    return $goods_class_all;
	}
	/**
	 * 商品分类SEO
	 *
	 * @return array
	 */
	private function _goods_class_seo(){

		$list = $this->table('goods_class')->field('gc_id,gc_title,gc_keywords,gc_description')->where(array('gc_keywords'=>array('neq','')))->limit(2000)->select();
		if (!is_array($list)) return null;
		$array = array();
		foreach ($list as $k=>$v) {
			if ($v['gc_title'] != '' || $v['gc_keywords'] != '' || $v['gc_description'] != ''){
				if ($v['gc_name'] != ''){
					$array[$v['gc_id']]['name'] = $v['gc_name'];
				}
				if ($v['gc_title'] != ''){
					$array[$v['gc_id']]['title'] = $v['gc_title'];
				}
				if ($v['gc_keywords'] != ''){
					$array[$v['gc_id']]['key'] = $v['gc_keywords'];
				}
				if ($v['gc_description'] != ''){
					$array[$v['gc_id']]['desc'] = $v['gc_description'];
				}
			}
		}
		return $array;
	}
	
	/**
	 * 商城主要频道SEO
	 *
	 * @return array
	 */
	private function _seo(){
		$list =$this->table('seo')->where(true)->select();
		if (!is_array($list)) return null;
		$array = array();
		foreach ($list as $key=>$value){
			$array[$value['type']] = $value;
		}
		return $array;
	}

	/**
	 * 自定义导航
	 *
	 * @return array
	 */
	private function _nav(){
		$list = $this->table('navigation')->order('nav_sort')->select();
		if (!is_array($list)) return null;
		return $list;
	}
	
	/**
	 * 商品TAG
	 *
	 * @return array
	 */
	private function _class_tag(){
		$field = 'gc_tag_id,gc_tag_name,gc_tag_value,gc_id,type_id';
		$list = $this->table('goods_class_tag')->field($field)->where(true)->select();
		if (!is_array($list)) return null;
		return $list;
	}

	/**
	 * 递归取某个分类下的所有子类ID组成的字符串,以逗号隔开
	 *
	 * @param string $goodsclass 商品分类
	 * @param int $gc_id	待查找子类的ID
	 * @param string $child 存放被查出来的子类ID
	 */
	private function get_child(&$goodsclass,$gc_id,&$child){
		foreach ($goodsclass as $k=>$v) {
			if ($v['gc_parent_id'] == $gc_id){
				$child[] = $v['gc_id'];
				$this->get_child($goodsclass,$v['gc_id'],$child);
			}
		}
	}
	/**
	 * 文章分类缓存
	 */
	private function _articleclass(){
		$model_class = Model('article_class');
		$tree_arr = $model_class->getArticleClassTree();	
		return $tree_arr;
	}
	/**
	 * 支付方式缓存
	 */
	private function _payment(){
		$payment_list = Model()->table('payment')->select();
		foreach ($payment_list as $k=>$v){
			$config_array = $v['payment_config']?unserialize($v['payment_config']):array();
			$payment_list_new[$v['payment_code']] = array_merge($payment_list[$k],$config_array);
		}
		return $payment_list_new;
	}
	/**
	 * 快递公司缓存
	 */
	private function _express(){
	    $fields = 'id,e_name,e_code,e_letter,e_order,e_url';
		$list = $this->table('express')->field($fields)->order('e_order,e_letter')->where(array('e_state'=>1))->select();
		if (!is_array($list)) return null;
		$array = array();
		foreach ($list as $k=>$v) {
			$array[$v['id']] = $v;
		}
		return $array;
	}
	/**
	 * 预设操作原因
	 */
	private function _reason(){
		$reason_list = $this->table('reason')->order('type asc,sort asc')->select();
		if (empty($reason_list)){ return null; }
		$reason_listnew = array();
		foreach ($reason_list as $k=>$v){
			$reason_listnew[$v['type']][$v['id']] = $v;
		}
		return $reason_listnew;
	}
	/**
	 * 生成首页
	 *
	 */
	private function _indexcache(){
		$path = BASE_CACHE_PATH.'/index/index.html';
		$content = file_get_contents(SHOP_SITE_URL.'/index.php?createcache=1');
		//截取需要换成的内容
		preg_match_all("/<cachetag>(.*)<\/cachetag>/is",$content,$out);
		file_put_contents($path,$out[1][0]);
	}
}