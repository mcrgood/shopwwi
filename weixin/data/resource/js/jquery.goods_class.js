$(document).ready(function(){
	//列表下拉
	$('img[nc_type="flex"]').click(function(){
		var status = $(this).attr('status');
		if(status == 'open'){
			var pr = $(this).parent('td').parent('tr');
			var id = $(this).attr('fieldid');
			var obj = $(this);
			$(this).attr('status','none');
			//ajax
			$.ajax({
				url: 'index.php?act=goods_class&op=goods_class&ajax=1&gc_parent_id='+id,
				dataType: 'json',
				success: function(data){
					var src='';
					for(var i = 0; i < data.length; i++){
						var tmp_vertline = "<img class='preimg' src='"+TEMPLATES_URL+"/images/vertline.gif'/>";
						src += "<tr class='"+pr.attr('class')+" row"+id+"'>";
						src += "<td class='w36'><input type='checkbox' name='check_gc_id[]' value='"+data[i].gc_id+"' class='checkitem'>";
						//图片
						if(data[i].have_child == 1){
							src += " <img fieldid='"+data[i].gc_id+"' status='open' nc_type='flex' src='"+TEMPLATES_URL+"/images/tv-expandable.gif' />";
						}else{
							src += " <img fieldid='"+data[i].gc_id+"' status='none' nc_type='flex' src='"+TEMPLATES_URL+"/images/tv-item.gif' />";
						}
						src += "</td><td class='w48 sort'>";						
						//排序
						src += " <span title='"+sl_lang['editable_sort_at_lower_level']+"' ajax_branch='goods_class_sort' datatype='number' fieldid='"+data[i].gc_id+"' fieldname='gc_sort' nc_type='inline_edit' class='editable tooltip'>"+data[i].gc_sort+"</span></td>";
						//名称
						src += "<td class='w50pre name'>";
						
						
						for(var tmp_i=1; tmp_i < (data[i].deep-1); tmp_i++){
							src += tmp_vertline;
						}
						if(data[i].have_child == 1){
							src += " <img fieldid='"+data[i].gc_id+"' status='open' nc_type='flex' src='"+TEMPLATES_URL+"/images/tv-item1.gif' />";
						}else{
							src += " <img fieldid='"+data[i].gc_id+"' status='none' nc_type='flex' src='"+TEMPLATES_URL+"/images/tv-expandable1.gif' />";
						}
						src += " <span title='"+sl_lang['editable_name_at_lower_level']+"' required='1' fieldid='"+data[i].gc_id+"' ajax_branch='goods_class_name' fieldname='gc_name' nc_type='inline_edit' class='editable tooltip'>"+data[i].gc_name+"</span>";
						//新增下级
						if(data[i].deep < 3){
							src += "<a class='btn-add-nofloat marginleft' href='index.php?act=goods_class&op=goods_class_add&gc_parent_id="+data[i].gc_id+"'><span>"+sl_lang['newadd_lowerlevel']+"</span></a>";
						}
						src += "</td>";
						//类型
						//src += "<td>"+data[i].type_name+"</td>";
						//显示
						src += "<td class='align-center power-onoff'>";
						if(data[i].gc_show == 0){
							src += "<a href='JavaScript:void(0);' class='tooltip disabled' fieldvalue='0' fieldid='"+data[i].gc_id+"' ajax_branch='goods_class_show' fieldname='gc_show' nc_type='inline_edit' title='"+sl_lang['editable_category_displayed']+"'><img src='"+TEMPLATES_URL+"/images/transparent.gif'></a>"
						}else {
							src += "<a href='JavaScript:void(0);' class='tooltip enabled' fieldvalue='1' fieldid='"+data[i].gc_id+"' ajax_branch='goods_class_show' fieldname='gc_show' nc_type='inline_edit' title='"+sl_lang['editable_category_displayed']+"'><img src='"+TEMPLATES_URL+"/images/transparent.gif'></a>"
						}
						src += "</td>";
						//操作
						src += "<td class='w84'>";
						src += "<a href='index.php?act=goods_class&op=goods_class_edit&gc_id="+data[i].gc_id+"'>"+sl_lang['edit']+"</a>";
						src += " | <a href=\"javascript:if(confirm('"+sl_lang['delete_class_and_lowerclass_issure']+"'))window.location = 'index.php?act=goods_class&op=goods_class_del&gc_id="+data[i].gc_id+"';\">"+sl_lang['delete']+"</a>";
						src += "</td>";
						src += "</tr>";
					}
					//插入
					pr.after(src);
					obj.attr('status','close');
					obj.attr('src',obj.attr('src').replace("tv-expandable","tv-collapsable"));
					$('img[nc_type="flex"]').unbind('click');
					$('span[nc_type="inline_edit"]').unbind('click');
					//重现初始化页面
                    $.getScript(RESOURCE_SITE_URL+"/js/jquery.edit.js");
					$.getScript(RESOURCE_SITE_URL+"/js/jquery.goods_class.js");
					$.getScript(RESOURCE_SITE_URL+"/js/jquery.tooltip.js");
					$.getScript(RESOURCE_SITE_URL+"/js/admincp.js");
				},
				error: function(){
					alert(sl_lang['failed_to_get_information']);
				}
			});
		}
		if(status == 'close'){
			$(".row"+$(this).attr('fieldid')).remove();
			$(this).attr('src',$(this).attr('src').replace("tv-collapsable","tv-expandable"));
			$(this).attr('status','open');
		}
	})
});