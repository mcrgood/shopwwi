$(document).ready(function(){
    var url = window.location.search;
    var params  = url.substr(1).split('&');
    var act = '';
    var op  = '';
    for(var j=0; j < params.length; j++)
    {
        var param = params[j];
        var arr   = param.split('=');
        if(arr[0] == 'act')
        {
            act = arr[1];
        }
        if(arr[0] == 'op')
        {
            sort = arr[1];
        }
    }
	//给需要修改的位置添加修改行为
	$('span[nc_type="inline_edit"]').click(function(){
		var s_value  = $(this).text();
		var s_name   = $(this).attr('fieldname');
		var s_id     = $(this).attr('fieldid');
		var req      = $(this).attr('required');
		var type     = $(this).attr('datatype');
		var max      = $(this).attr('maxvalue');
		var url      = $(this).attr('fieldurl');
		
		$('<input type="text">')
                        .attr({value:s_value})
                        .insertAfter($(this))
                        .focus()
                        .select()
                        .keyup(function(event){
                        if(event.keyCode == 13)
                        {
                            if(req)
                            {
                                if(!required($(this).attr('value'),s_value,$(this)))
                                {
                                    return;
                                }
                            }
                            if(type)
                            {
                                if(!check_type(type,$(this).attr('value'),s_value,$(this)))
                                {
                                    return;
                                }
                            }
                            if(max)
                            {
                                if(!check_max($(this).attr('value'),s_value,max,$(this)))
                                {
                                    return;
                                }
                            }
                            $(this).prev('span').show().text($(this).attr("value"));
							//branch ajax 分支
							//id 修改内容索引标识
							//column 修改字段名
							//value 修改内容
                            $.get(url,{value:$(this).attr('value')},function(data){
                                if(data === 'false')
                                {
                                    alert(sl_lang['name_already_exists_please_change']);
                                    $('span[fieldname="'+s_name+'"][fieldid="'+s_id+'"]').text(s_value);
                                    return;
                                }
                            });
                            $(this).remove();
                        }
                    })
					.blur(function(){
					if(req)
					{
						if(!required($(this).attr('value'),s_value,$(this)))
						{
							return;
						}
					}
					if(type)
					{
						if(!check_type(type,$(this).attr('value'),s_value,$(this)))
						{
							return;
						}
					}
					if(max)
					{
						if(!check_max($(this).attr('value'),s_value,max,$(this)))
						{
							return;
						}
					}
					$(this).prev('span').show().text($(this).attr('value'));
					$.get(url,{value:$(this).attr('value')},function(data){
						if(data === 'false')
							{
								alert(sl_lang['name_already_exists_please_change']);
								$('span[fieldname="'+s_name+'"][fieldid="'+s_id+'"]').text(s_value);
								return;
							}
					});
					$(this).remove();
				});
		$(this).hide();
	});
	
	
		$('span[nc_type="inline_edit_textarea"]').click(function(){
		var s_value  = $(this).text();
		var s_name   = $(this).attr('fieldname');
		var s_id     = $(this).attr('fieldid');
		var req      = $(this).attr('required');
		var type     = $(this).attr('datatype');
		var max      = $(this).attr('maxvalue');
		var url      = $(this).attr('fieldurl');
		
		$('<textarea>')
                        .attr({value:s_value})
                        .appendTo($(this).parent())
                        .focus()
                        .select()
                        .keyup(function(event){
                        if(event.keyCode == 13)
                        {
                            if(req)
                            {
                                if(!required($(this).attr('value'),s_value,$(this)))
                                {
                                    return;
                                }
                            }
                            if(type)
                            {
                                if(!check_type(type,$(this).attr('value'),s_value,$(this)))
                                {
                                    return;
                                }
                            }
                            if(max)
                            {
                                if(!check_max($(this).attr('value'),s_value,max,$(this)))
                                {
                                    return;
                                }
                            }
                            $(this).prev('span').show().text($(this).attr("value"));
							//branch ajax 分支
							//id 修改内容索引标识
							//column 修改字段名
							//value 修改内容
                            $.get(url,{value:$(this).attr('value')},function(data){
                                if(data === 'false')
                                {
                                    alert(sl_lang['name_already_exists_please_change']);
                                    $('span[fieldname="'+s_name+'"][fieldid="'+s_id+'"]').text(s_value);
                                    return;
                                }
                                if(data === 'goodsname_overnum')
        						{
        							alert(sl_lang['goodsname_not_more_than_30words']);
        							$('span[fieldname="'+s_name+'"][fieldid="'+s_id+'"]').text(s_value);
        							return;
        						}
                            });
                            $(this).remove();
                        }
                    })
					.blur(function(){
					if(req)
					{
						if(!required($(this).attr('value'),s_value,$(this)))
						{
							return;
						}
					}
					if(type)
					{
						if(!check_type(type,$(this).attr('value'),s_value,$(this)))
						{
							return;
						}
					}
					if(max)
					{
						if(!check_max($(this).attr('value'),s_value,max,$(this)))
						{
							return;
						}
					}
					$(this).prev('span').show().text($(this).attr('value'));
					$.get(url,{value:$(this).attr('value')},function(data){
						if(data === 'false')
							{
								alert(sl_lang['name_already_exists_please_change']);
								$('span[fieldname="'+s_name+'"][fieldid="'+s_id+'"]').text(s_value);
								return;
							}
						if(data === 'goodsname_overnum')
						{
							alert(sl_lang['goodsname_not_more_than_30words']);
							$('span[fieldname="'+s_name+'"][fieldid="'+s_id+'"]').text(s_value);
							return;
						}
					});
					$(this).remove();
				});
		$(this).hide();
	});
	
	//给需要修改的图片添加异步修改行为
	$('img[nc_type="inline_edit"]').click(function(){
		var i_id    = $(this).attr('fieldid');
		var i_name  = $(this).attr('fieldname');
		var i_src   = $(this).attr('src');
		var i_val   = ($(this).attr('fieldvalue'))== 0 ? 1 : 0;
		var url      = $(this).attr('fieldurl');

		$.get(url,{value:i_val},function(data){
		if(data == 'true')
			{
				if(i_val == 0)
				{
					$('img[fieldid="'+i_id+'"][fieldname="'+i_name+'"]').attr({'src':i_src.replace('enabled','disabled'),'fieldvalue':i_val});
				}
				else
				{
					$('img[fieldid="'+i_id+'"][fieldname="'+i_name+'"]').attr({'src':i_src.replace('disabled','enabled'),'fieldvalue':i_val});
				}
			}
		});
	});
	$('a[nc_type="inline_edit"]').click(function(){
		var i_id    = $(this).attr('fieldid');
		var i_name  = $(this).attr('fieldname');
		var i_src   = $(this).attr('src');
		var i_val   = ($(this).attr('fieldvalue'))== 0 ? 1 : 0;
		var url      = $(this).attr('fieldurl');

		$.get(url,{value:i_val},function(data){
		if(data == 'true')
			{
				if(i_val == 0){
					$('a[fieldid="'+i_id+'"][fieldname="'+i_name+'"]').attr({'class':('enabled','disabled'),'title':(sl_lang['open'],sl_lang['close']),'fieldvalue':i_val});
				}else{
					$('a[fieldid="'+i_id+'"][fieldname="'+i_name+'"]').attr({'class':('disabled','enabled'),'title':(sl_lang['close'],sl_lang['open']),'fieldvalue':i_val});
				}
			}else{
				alert(sl_lang['response_to_failure']);
			}
		});
	});
    //给每个可编辑的小图片的父元素添加可编辑标题 $('img[nc_type="inline_edit"]').parent().attr('title','可编辑');
    //给列表有排序行为的列添加鼠标手型效果
    $('span[nc_type="order_by"]').hover(function(){$(this).css({cursor:'pointer'});},function(){});
});
//检查提交内容的必须项
function required(str,s_value,jqobj)
{
	if(str == '')
	{
		jqobj.prev('span').show().text(s_value);
		jqobj.remove();
		alert(sl_lang['this_cannot_be_empty']);
		return 0;
	}
	return 1;
}
//检查提交内容的类型是否合法
function check_type(type, value, s_value, jqobj)
{
	if(type == 'number')
	{
		if(isNaN(value))
		{
		jqobj.prev('span').show().text(s_value);
		jqobj.remove();
		alert(sl_lang['the_numbers_alone_can']);
		return 0;
		}
	}
	if(type == 'int')
	{
		var regu = /^-{0,1}[0-9]{1,}$/;
		if(!regu.test(value))
		{
			jqobj.prev('span').show().text(s_value);
			jqobj.remove();
			alert(sl_lang['this_can_only_integers']);
			return 0;
		}
	}
	if(type == 'pint')
	{
		var regu = /^[0-9]+$/;
		if(!regu.test(value))
		{
			jqobj.prev('span').show().text(s_value);
			jqobj.remove();
			alert(sl_lang['the_only_positive_integer']);
			return 0;
		}
	}
	if(type == 'zint')
	{
		var regu = /^[1-9]\d*$/;
		if(!regu.test(value))
		{
			jqobj.prev('span').show().text(s_value);
			jqobj.remove();
			alert(sl_lang['the_only_positive_integer']);
			return 0;
		}
	}
		if(type == 'discount')
	{
		var regu = /[1-9]|0\.[1-9]|[1-9]\.[0-9]/;
		if(!regu.test(value))
		{
			jqobj.prev('span').show().text(s_value);
			jqobj.remove();
			alert(sl_lang['only_is_between_0.1_9.9']);
			return 0;
		}
	}
	return 1;
}
//检查所填项的最大值
function check_max(str,s_value,max,jqobj)
{
	if(parseInt(str) > parseInt(max))
	{
		jqobj.prev('span').show().text(s_value);
		jqobj.remove();
		alert(sl_lang['this_shouldbe_lessthan_or_equalto']+max);
		return 0;
	}
	return 1;
}


//新的inline_edit调用方法
//javacript
//$('span[nc_type="class_sort"]').inline_edit({act: 'microshop',wwi: 'update_class_sort'});
//html
//<span nc_type="class_sort" column_id="<?php echo $val['class_id'];?>" title="<?php echo $lang['nc_editable'];?>" class="editable tooltip"><?php echo $val['class_sort'];?></span>
//php 
//$result = array();
//$result['result'] = FALSE;/TURE
//$result['message'] = '错误';
//echo json_encode($result);
 
(function($) {
 $.fn.inline_edit= function(options) {
     var settings = $.extend({}, {open: false}, options);
     return this.each(function() {
         $(this).click(onClick);
     });

     function onClick() {
         var span = $(this);
         var old_value = $(this).html();
         var column_id = $(this).attr("column_id");
         $('<input type="text">')
         .insertAfter($(this))
         .focus()
         .select()
         .val(old_value)
         .blur(function(){
             var new_value = $(this).attr("value");
             if(new_value != '') {
                 $.get('index.php?act='+settings.act+'&op='+settings.op+'&branch=ajax',{id:column_id,value:new_value},function(data){
                     data = $.parseJSON(data);
                     if(data.result) {
                         span.show().text(new_value);
                     } else {
                         span.show().text(old_value);
                         alert(data.message);
                     }
                 });
             } else {
                 span.show().text(old_value);
             }
             $(this).remove();
         })
         $(this).hide();
     }
}
})(jQuery);

(function($) {
 $.fn.inline_edit_confirm = function(options) {
     var settings = $.extend({}, {open: false}, options);
     return this.each(function() {
         $(this).click(onClick);
     });

     function onClick() {
         var $span = $(this);
         var old_value = $(this).text();
         var column_id = $(this).attr("column_id");
         var $input = $('<input type="text">');
         var $btn_submit = $('<a class="inline-edit-submit" href="JavaScript:;">'+sl_lang['confirm']+'</a>');
         var $btn_cancel = $('<a class="inline-edit-cancel" href="JavaScript:;">'+sl_lang['cancel']+'</a>');

         $input.insertAfter($span).focus().select().val(old_value);
         $btn_submit.insertAfter($input);
         $btn_cancel.insertAfter($btn_submit);
         $span.hide();

         $btn_submit.click(function(){
             var new_value = $input.attr("value");
             if(new_value !== '' && new_value !== old_value) {
                 $.post('index.php?act=' + settings.act + '&op=' + settings.op, {id:column_id, value:new_value}, function(data) {
                     data = $.parseJSON(data);
                     if(data.result) {
                         $span.text(new_value);
                     } else {
                         alert(data.message);
                     }
                 });
             }
             show();
         });

         $btn_cancel.click(function() {
             show();
         });

         function show() {
             $span.show();
             $input.remove();
             $btn_submit.remove();
             $btn_cancel.remove();
         }
     }
};
})(jQuery);