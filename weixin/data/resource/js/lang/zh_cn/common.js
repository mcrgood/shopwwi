/**
 * js公用语言包
 */
sl_lang = new Array();
sl_lang['parameter_error'] 			= "参数错误";
sl_lang['please_select_operation_records'] = '请先选择操作记录';
sl_lang['login']					= '登录';
sl_lang['collection_success']		= '收藏成功';
sl_lang['please_choose'] 			= "---请选择---";
sl_lang['please_select_a_region'] 	= "请选择所在地区";
sl_lang['ie6version_prompt'] 		= '提示：您正在使用IE6内核浏览器，为了您更好的使用网站功能，建议您升级IE浏览器或改用其他内核浏览器。';
sl_lang['failed_to_get_information']= "获取信息失败";
sl_lang['name_already_exists_please_change']	= "名称已经存在，请您换一个";
sl_lang['goodsname_not_more_than_30words']		= "商品名称请不要超过30个字";
sl_lang['open'] 					= '开启';
sl_lang['close'] 					= '关闭';
sl_lang['response_to_failure'] 		= '响应失败';
sl_lang['this_cannot_be_empty'] 	= '此项不能为空';
sl_lang['the_numbers_alone_can'] 	= '此项仅能为数字';
sl_lang['this_can_only_integers'] 	= '此项仅能为整数';
sl_lang['the_only_positive_integer']= '此项仅能为正整数';
sl_lang['only_is_between_0.1_9.9']	= '只能是0.1-9.9之间的数字';
sl_lang['this_shouldbe_lessthan_or_equalto'] = '此项应小于等于';
sl_lang['confirm'] 					= '确定';
sl_lang['cancel'] 					= "取消";
sl_lang['editable_sort_at_lower_level']	= "可编辑下级分类排序";
sl_lang['editable_name_at_lower_level']	= "可编辑下级分类名称";
sl_lang['editable_category_displayed']	= "可编辑该分类是否显示";
sl_lang['newadd_lowerlevel']			= "新增下级";
sl_lang['edit']							= "编辑";
sl_lang['delete']						= "删除";
sl_lang['delete_class_and_lowerclass_issure']	= "删除该分类将会同时删除该分类的所有下级分类，您确定要删除吗";
sl_lang['must_be_a_number']				= "必须是数字";
sl_lang['canot_be_empty']				= "不能为空";
sl_lang['must_be_an_integer']			= "必须是整数";
sl_lang['maxrange_0_to']				= "范围是0~";