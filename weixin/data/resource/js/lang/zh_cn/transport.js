/**
 * js公用语言包
 */
sl_lang['the_default_shipping'] 				=	"默认运费：";
sl_lang['the_default_quantity_of_freight'] 		=	"默认运费件数";
sl_lang['inside_piece'] 						=	"件内，";
sl_lang['the_default_shipping_prices'] 			=	"默认运费价格";
sl_lang['yuan_each_additional'] 				=	"元， 每增加";
sl_lang['each_additional_piece'] 				=	"每加件";
sl_lang['piece_increase_the_freight'] 			=	"件， 增加运费";
sl_lang['add_piece_the_freight'] 				=	"加件运费";
sl_lang['currency_zh'] 							=	"元";
sl_lang['check_all'] 							=	"全选";
sl_lang['cancel_batch'] 						=	"取消批量";
sl_lang['batch_set'] 							=	"批量设置";
sl_lang['batch_delete']							=	"批量删除";
sl_lang['setup_freight_for_given_area']			=	"为指定地区城市设置运费";
sl_lang['batch_operation']						=	"批量操作";
sl_lang['area_is_empty_or_specified_error']		=	"指定地区城市为空或指定错误";
sl_lang['first_should_enter_a_number_1_to_9999']=	"首件应输入1至9999的数字";
sl_lang['firstfee_should_enter_a_number_0.00_to_999.99']=	"首费应输入0.00至999.99的数字";
sl_lang['renewal_should_enter_1_to_9999_numbers']		=	"续件应输入1至9999的数字";
sl_lang['renewalfee_should_enter_0.00_to_999.99']		=	"续费应输入0.00至999.99的数字";
sl_lang['delivery_to_the']						=	"运送到";
sl_lang['sl_piece']								=	"件";
sl_lang['first_thing_piece']					=	"首件";
sl_lang['first_charge_currency_text']			=	"首费";
sl_lang['renewal_piece']						=	"续件";
sl_lang['renewal_fee']							=	"续费";
sl_lang['operation']							=	"操作";
sl_lang['edit_shipping_area']					=	"编辑运送区域";
sl_lang['sl_edit']								=	"编辑";
sl_lang['not_add_region']						=	"未添加地区";
sl_lang['sl_delete']							=	"删除";
sl_lang['confirm_to_delete']					=	"确认删除吗?";
sl_lang['please_choose_to_batch_setting_area']	=	"请选择要批量设置的地区";
sl_lang['please_choose_to_batch_processing_areas']=	"请选择要批量处理的地区";
sl_lang['confirm_batch_delete']					=	"确认批量删除吗？";
