<?php
/**
 * 默认展示页面
 * @copyright  Copyright (c) 2007-2013 ShopWWI Inc. (http://www.shopwwi1.com)
 * @license    http://www.shopwwi2.com
 * @link       http://www.shopwwi.com
 * @since      File available since Release v1.1
 */
defined('InByShopWWI') or exit('Access Invalid!');
define('MYSQL_RESULT_TYPE',1);
class mallControl extends mallBaseControl{
	
	public function __construct(){
		parent::__construct();
	}
	
	public function indexOp(){
		$this->mallOp();
	}
	
	
	/*
	 * 商城首页
	 */
	public function mallOp(){
		$model = Model();
		$adv = $model->table('adv')->where(array('wx_id'=>intval($_GET['wx_id']),'adv_state'=>'0'))->order("adv_sort desc")->select();		
		Tpl::output('adv',$adv);//广告位
		
		$article = $model->table('article')->field("article_id,article_title")->where(array('wx_id'=>intval($_GET['wx_id'])))->order("article_id desc")->limit("0,5")->select();
		Tpl::output('article',$article);//消息模块
		
		$site = $model->table('site')->where(array('wx_id'=>intval($_GET['wx_id'])))->find();
		$product = file_get_contents($site['mall_url'].'/api/weixin/index.php');
		$product = json_decode($product,true);
		Tpl::output('product',$product);//商品列表
		
		Tpl::showpage('index');
	}
	
	
	/*
	 * 商城列表
	 */
	public function listOp(){
		
		$model = Model();
		$site = $model->table('site')->where(array('wx_id'=>intval($_GET['wx_id'])))->find();
		
		$offset = isset($_GET['offset'])?intval($_GET['offset']):0;
		$rows	= isset($_GET['rows'])?intval($_GET['rows']):10;
		
		$list = @file_get_contents($site['mall_url'].'/api/weixin/index.php?offset='.$offset.'&rows='.$rows);
		$list = json_decode($list,true);
		
		Tpl::output('list',$list);//产品列表
		
		Tpl::showpage('product.list');
	}
	
	
	/*
	 *	商品列表 
	 */	
	public function productajaxOp(){
		
		$offset = isset($_GET['offset'])?intval($_GET['offset']):0;
		$rows	= isset($_GET['rows'])?intval($_GET['rows']):10;
		$limit  = $offset*$rows;
		
		$model = Model();
		$site  = $model->table('site')->where(array('wx_id'=>intval($_GET['wx_id'])))->find();
		
		$list = @file_get_contents($site['mall_url'].'/api/weixin/index.php?offset='.$limit.'&rows='.$rows);
		$arr  = @json_decode($list,true);
		
		echo json_encode(array('product'=>$arr,'offset'=>($offset+1)));
		exit;
	}

	/*
	 * 商品详情
	 */
	public function detailOp(){	
		$model = Model();
		$product = $model->table("product")->where(array('product_id'=>intval($_GET['product_id']),'wx_id'=>intval($_GET['wx_id'])))->find();
		
		if(empty($product)){
			showMessage('该商品不存在','?act=index','error');
		}
		
		Tpl::output('product',$product);//产品列表	
		
		$comment = $model->table('comment')->where(array('wx_id'=>intval($_GET['wx_id']),'item_id'=>intval($_GET['product_id'])))->select();
		Tpl::output('comment',$comment);//评论列表
		
		Tpl::showpage('product.detail');
	}

	/*
	 * 商品评论
	 */
	public function addCommentOp(){	
		if(isset($_POST) && !empty($_POST)){//评论商品
			$model = Model();
			$product = $model->table('product')->where(array('product_id'=>intval($_POST['product_id']),'wx_id'=>intval($_GET['wx_id'])))->find();
				
			if(empty($product)){
				showMessage('该商品不存在','?act=index','error');
			}
			
			$fans = $model->table('fans')->where(array('fans_id'=>intval($_POST['fans_id']),'wx_id'=>intval($_GET['wx_id'])))->find();
			if(empty($fans)){
				showMessage('该粉丝不存在','?act=index','error');
			}
			
			$params 	= array();
			$params['comment_content'] = $_POST['comment_content'];
			$params['comment_time']	   = time();
			$params['fans_id']		   = intval($_POST['fans_id']);
			$params['fans_nickname']   = $fans['fans_nickname'];
			$params['item_id']		   = trim($_POST['product_id']);
			$params['wx_id']		   = intval($_GET['wx_id']);	
			
			$res = $model->table('comment')->insert($params);
			if($res){
				showMessage('评论成功','?act=index','succ');
			}else{
				showMessage('评论失败','?act=index','error');
			}
		}
		
		Tpl::showpage('comment.add');
	}
	
	/*
	 * 商城动态
	 */
	public function articlelistOp(){
		$model = Model();
		$article = $model->table('article')->where(array('wx_id'=>intval($_GET['wx_id'])))->select();
		Tpl::output('article',$article);//文章详情
		Tpl::showpage('article.list');
	}
	
	/*
	 * 文章信息
	 */
	public function articleOp(){		
		$model = Model();
		$article = $model->table('article')->where(array('article_id'=>intval($_GET['article_id']),'wx_id'=>intval($_GET['wx_id'])))->find();
		
		if(empty($article)){
			showMessage('该文章不存在','?act=index','error');
		}
		
		Tpl::output('article',$article);//文章详情 	
		Tpl::showpage('article');
	}
	
	
	/*
	 * 系统文章
	 */
	public function articledetailOp(){
		$type = intval($_GET['type']); //1.联系我们 2.关于我们 3.商城介绍
		
		$model = Model();
		$basic = $model->table('basic')->where(array('wx_id'=>intval($_GET['wx_id']),'type'=>$type))->find();
		
		if(empty($basic)){
			showMessage('该文章不存在','?act=index','error');
		}
		Tpl::output('basic',$basic);//文章详情
		Tpl::showpage('basic');
		
	}
}