<?php
/**
 * 粉丝管理
 * @copyright  Copyright (c) 2007-2013 ShopWWI Inc. (http://www.shopwwi1.com)
 * @license    http://www.shopwwi2.com
 * @link       http://www.shopwwi.com
 * @since      File available since Release v1.1
 */
defined('InByShopWWI') or exit('Access Invalid!');
class fansControl extends BaseHomeControl{
	public function __construct(){
		parent::__construct();
		Language::read('fans');
	}
	/**
	 * 粉丝列表
	 */
	public function fans_listOp(){
		$model = Model();
		$wx_id = intval($_GET['wx_id']);
		$where_condition = array('wx_id'=>$wx_id);
		if(trim($_GET['s_nickname']) != ''){
			$where_condition['fans_nickname'] = array('like','%'.trim($_GET['s_nickname']).'%');
		}
		$fans_list = $model->table('fans')->where($where_condition)->page(15)->order('fans_subtime desc')->select();
		Tpl::output('show_page',$model->showpage());
		Tpl::output('fans_list',$fans_list);
		Tpl::showpage('fans_list');
	}
	/**
	 * 更新粉丝列表
	 */
	public function fans_updateOp(){
		$model = Model();
		$wx_id = intval($_GET['wx_id']);
		//调取公众账号的接口信息
		$wx_info = $model->field('wx_appid,wx_appsecret,wx_accesstoken,wx_nextopenid')->table('wxaccount')->where(array('wx_id'=>$wx_id))->find();
		$access_token = $wx_info['wx_accesstoken'];
		if($access_token == ''){
			$access_token = $this->get_accesstoken($wx_info['wx_appid'], $wx_info['wx_appsecret'], $wx_id);
		}
		$url = 'https://api.weixin.qq.com/cgi-bin/user/get?access_token='.$access_token;
//		if($wx_info['wx_nextopenid'] != ''){
//			$url .= '&next_openid='.$wx_info['wx_nextopenid'];
//		}
		$res = @file_get_contents($url);
		$res = json_decode($res,true);
		if($res['errcode'] != ''){
			if($res['errcode'] == '40001' || $res['errcode'] == '42001' || $res['errcode'] == '42002' || $res['errcode'] == '42003'){
				$access_token = $this->get_accesstoken($wx_info['wx_appid'], $wx_info['wx_appsecret'], $wx_id);
				$res = @file_get_contents('https://api.weixin.qq.com/cgi-bin/user/get?access_token='.$access_token);
				$res = json_decode($res,true);
				if($res['errcode'] != ''){
					echo json_encode(array('done'=>false,'msg'=>'data_fetch_failed:'.$res['errcode']));die;//抓取失败
				}
			}else{
				echo json_encode(array('done'=>false,'msg'=>'data_fetch_failed_2:'.$res['errcode']));die;//抓取失败
			}
		}
		//删除原来的所有粉丝
		$model->table('fans')->where(array('wx_id'=>$wx_id))->delete();
		if($res['count'] > 0){
			$openid_list = $res['data']['openid'];
			$pagenum = ceil($res['total']/$res['count']);
			$next_openid = $res['next_openid'];
			if($pagenum > 1){
				for ($i=2;$i<=$pagenum;$i++){
					$rs = @file_get_contents('https://api.weixin.qq.com/cgi-bin/user/get?access_token='.$access_token.'&next_openid='.$next_openid);
					if($res['errcode'] != ''){
						if($res['errcode'] == '40001' || $res['errcode'] == '42001' || $res['errcode'] == '42002' || $res['errcode'] == '42003'){
							$access_token = $this->get_accesstoken($wx_info['wx_appid'], $wx_info['wx_appsecret'], $wx_id);
							$res = @file_get_contents('https://api.weixin.qq.com/cgi-bin/user/get?access_token='.$access_token.'&next_openid='.$next_openid);
							$res = json_decode($res,true);
							if($res['errcode'] != ''){
								echo json_encode(array('done'=>false,'msg'=>'data_fetch_failed_3:'.$res['errcode']));die;
							}
						}else{
							echo json_encode(array('done'=>false,'msg'=>'data_fetch_failed_3:'.$res['errcode']));die;
						}
					}
					$openid_list = array_merge($openid_list,$rs['data']['openid']);
					$next_openid = $rs['next_openid'];
				}
			}
			//抓取所有粉丝信息
			$fans_insert_array = array();
			if(!empty($openid_list)){
				foreach ($openid_list as $val){
					$fans_info = @file_get_contents('https://api.weixin.qq.com/cgi-bin/user/info?access_token='.$access_token.'&openid='.$val);
					$fans_info = json_decode($fans_info,true);
					if($fans_info['errcode'] != ''){
						if($fans_info['errcode'] == '40001' || $fans_info['errcode'] == '42001' || $fans_info['errcode'] == '42002' || $fans_info['errcode'] == '42003'){
							$access_token = $this->get_accesstoken($wx_info['wx_appid'], $wx_info['wx_appsecret'], $wx_id);
							$fans_info = @file_get_contents('https://api.weixin.qq.com/cgi-bin/user/info?access_token='.$access_token.'&openid='.$val);
							$res = json_decode($res,true);
							if($res['errcode'] != ''){
								echo json_encode(array('done'=>false,'msg'=>'data_fetch_failed_4:'.$fans_info['errcode']));die;
							}
						}else{
							echo json_encode(array('done'=>false,'msg'=>'data_fetch_failed_4:'.$fans_info['errcode']));die;
						}
					}
					$fans_insert_array[] = array(
						'fans_openid'=>$val,
						'wx_id'=>$wx_id,
						'fans_nickname'=>$fans_info['nickname'],
						'fans_sex'=>$fans_info['sex'],
						'fans_language'=>$fans_info['language'],
						'fans_city'=>$fans_info['city'],
						'fans_province'=>$fans_info['province'],
						'fans_country'=>$fans_info['country'],
						'fans_headimgurl'=>$fans_info['headimgurl'],
						'fans_pic'=>'',
						'fans_subtime'=>$fans_info['subscribe_time']
					);
				}
				$frs = $model->table('fans')->insertAll($fans_insert_array);
				if($frs){
					//更新next_openid
//					$model->table('wxaccount')->where(array('wx_id'=>$wx_id))->update(array('wx_nextopenid'=>$next_openid));
					echo json_encode(array('done'=>true));die;
				}else{
					echo json_encode(array('done'=>false,'msg'=>'data_save_failed'));die;
				}
			}
		}
		echo json_encode(array('done'=>true));die;
	}
	/**
	 * 分组列表
	 */
	public function group_listOp(){
		$model = Model();
		$wx_id = intval($_GET['wx_id']);
		//调取公众账号的接口信息
		$wx_info = $model->field('wx_appid,wx_appsecret,wx_accesstoken')->table('wxaccount')->where(array('wx_id'=>$wx_id))->find();
		$access_token = $wx_info['wx_accesstoken'];
		if($access_token == ''){
			$access_token = $this->get_accesstoken($wx_info['wx_appid'], $wx_info['wx_appsecret'], $wx_id);
		}
		$url = 'https://api.weixin.qq.com/cgi-bin/groups/get?access_token='.$access_token;
		$res = @file_get_contents($url);
		$res = json_decode($res,true);
		if($res['errcode'] == '40001' || $res['errcode'] == '42001' || $res['errcode'] == '42002' || $res['errcode'] == '42003'){
			$access_token = $this->get_accesstoken($wx_info['wx_appid'], $wx_info['wx_appsecret'], $wx_id);
			$url = 'https://api.weixin.qq.com/cgi-bin/groups/get?access_token='.$access_token;
			$res = @file_get_contents($url);
			$res = json_decode($res,true);
		}
		Tpl::output('group_list',$res['errcode'] != ''?'error':$res['groups']);
		Tpl::showpage('group_list');
	}
	/**
	 * 添加新分组
	 */
	public function group_addOp(){
		$model = Model();
		$wx_id = intval($_GET['wx_id']);
		//调取公众账号的接口信息
		$wx_info = $model->field('wx_appid,wx_appsecret,wx_accesstoken')->table('wxaccount')->where(array('wx_id'=>$wx_id))->find();
		$access_token = $wx_info['wx_accesstoken'];
		if($access_token == ''){
			$access_token = $this->get_accesstoken($wx_info['wx_appid'], $wx_info['wx_appsecret'], $wx_id);
		}
		$data = '{
				    "group": {
				        "name": "'.trim($_GET['new_group']).'"
				    }
				}';
		 $ch = curl_init();  
         curl_setopt($ch, CURLOPT_URL, "https://api.weixin.qq.com/cgi-bin/groups/create?access_token=".$access_token);  
         curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");  
         curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);  
         curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);  
         curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; MSIE 5.01; Windows NT 5.0)');  
         curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);  
         curl_setopt($ch, CURLOPT_AUTOREFERER, 1);  
         curl_setopt($ch, CURLOPT_POSTFIELDS, $data);  
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);  
         $tmpInfo = curl_exec($ch);  
         if (curl_errno($ch)) {  
          echo json_encode(array('done'=>false,'msg'=>L('fans_far_request_failed')));die;
         }
         curl_close($ch);
         $tmpInfo = json_decode($tmpInfo,true);  
		if($tmpInfo['errcode'] == '40001' || $tmpInfo['errcode'] == '42001' || $tmpInfo['errcode'] == '42002' || $tmpInfo['errcode'] == '42003'){
			$access_token = $this->get_accesstoken($wx_info['wx_appid'], $wx_info['wx_appsecret'], $wx_id);
			 $ch = curl_init();  
	         curl_setopt($ch, CURLOPT_URL, "https://api.weixin.qq.com/cgi-bin/groups/create?access_token=".$access_token);  
	         curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");  
	         curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);  
	         curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);  
	         curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; MSIE 5.01; Windows NT 5.0)');  
	         curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);  
	         curl_setopt($ch, CURLOPT_AUTOREFERER, 1);  
	         curl_setopt($ch, CURLOPT_POSTFIELDS, $data);  
	         curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);  
	         $tmpInfo = curl_exec($ch);  
	         if (curl_errno($ch)) {  
	          echo json_encode(array('done'=>false,'msg'=>L('fans_far_request_failed')));die;
	         }
	         curl_close($ch);
	         $tmpInfo = json_decode($tmpInfo,true);
		}
		if($tmpInfo['errcode']!=''){ 
			echo json_encode(array('done'=>false,'msg'=>$tmpInfo['errmsg']));
		}else{
			echo json_encode(array('done'=>true));
		}
	}
	/**
	 * 编辑分组
	 */
	public function group_editOp(){
		$model = Model();
		$wx_id = intval($_GET['wx_id']);
		//调取公众账号的接口信息
		$wx_info = $model->field('wx_appid,wx_appsecret,wx_accesstoken')->table('wxaccount')->where(array('wx_id'=>$wx_id))->find();
		$access_token = $wx_info['wx_accesstoken'];
		if($access_token == ''){
			$access_token = $this->get_accesstoken($wx_info['wx_appid'], $wx_info['wx_appsecret'], $wx_id);
		}
		$data = '{
				    "group": {
				    	"id":'.intval($_GET['group_id']).',
				        "name":"'.trim($_GET['group_name']).'"
				    }
				}';
		 $ch = curl_init();  
         curl_setopt($ch, CURLOPT_URL, "https://api.weixin.qq.com/cgi-bin/groups/update?access_token=".$access_token);  
         curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");  
         curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);  
         curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);  
         curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; MSIE 5.01; Windows NT 5.0)');  
         curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);  
         curl_setopt($ch, CURLOPT_AUTOREFERER, 1);  
         curl_setopt($ch, CURLOPT_POSTFIELDS, $data);  
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);  
         $tmpInfo = curl_exec($ch);  
         if (curl_errno($ch)) {  
          echo json_encode(array('done'=>false,'msg'=>L('fans_far_request_failed')));die;
         }
         curl_close($ch);
         $tmpInfo = json_decode($tmpInfo,true);  
		if($tmpInfo['errcode'] == '40001' || $tmpInfo['errcode'] == '42001' || $tmpInfo['errcode'] == '42002' || $tmpInfo['errcode'] == '42003'){
			$access_token = $this->get_accesstoken($wx_info['wx_appid'], $wx_info['wx_appsecret'], $wx_id);
			 $ch = curl_init();  
	         curl_setopt($ch, CURLOPT_URL, "https://api.weixin.qq.com/cgi-bin/groups/update?access_token=".$access_token);  
	         curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");  
	         curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);  
	         curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);  
	         curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; MSIE 5.01; Windows NT 5.0)');  
	         curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);  
	         curl_setopt($ch, CURLOPT_AUTOREFERER, 1);  
	         curl_setopt($ch, CURLOPT_POSTFIELDS, $data);  
	         curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);  
	         $tmpInfo = curl_exec($ch);  
	         if (curl_errno($ch)) {  
	          echo json_encode(array('done'=>false,'msg'=>L('fans_far_request_failed')));die;
	         }
	         curl_close($ch);
	         $tmpInfo = json_decode($tmpInfo,true);
		}
		if($tmpInfo['errcode']==0){ 
			echo json_encode(array('done'=>true));
		}else{
			echo json_encode(array('done'=>false,'msg'=>$tmpInfo['errmsg']));
		}
	}
	/**
	 * 移动用户分组
	 */
	public function group_moveOp(){
		$model = Model();
		$wx_id = intval($_GET['wx_id']);
		//调取公众账号的接口信息
		$wx_info = $model->field('wx_appid,wx_appsecret,wx_accesstoken')->table('wxaccount')->where(array('wx_id'=>$wx_id))->find();
		$access_token = $wx_info['wx_accesstoken'];
		if($access_token == ''){
			$access_token = $this->get_accesstoken($wx_info['wx_appid'], $wx_info['wx_appsecret'], $wx_id);
		}
		//开始移动粉丝到指定分组
		if(chksubmit()){
			if(intval($_GET['choose_group']) < 0){
				echo json_encode(array('done'=>false,'msg'=>L('fans_pls_choose_group')));die;
			}
			$data = '{
					    "openid": "'.trim($_GET['openid']).'", 
					    "to_groupid": '.intval($_GET['choose_group']).'
					}';
			 $ch = curl_init();  
	         curl_setopt($ch, CURLOPT_URL, "https://api.weixin.qq.com/cgi-bin/groups/members/update?access_token=".$access_token);  
	         curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");  
	         curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);  
	         curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);  
	         curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; MSIE 5.01; Windows NT 5.0)');  
	         curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);  
	         curl_setopt($ch, CURLOPT_AUTOREFERER, 1);  
	         curl_setopt($ch, CURLOPT_POSTFIELDS, $data);  
	         curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);  
	         $tmpInfo = curl_exec($ch);  
	         if (curl_errno($ch)) {  
	          echo json_encode(array('done'=>false,'msg'=>L('fans_far_request_failed')));die;
	         }
	         curl_close($ch);
	         $tmpInfo = json_decode($tmpInfo,true);  
			if($tmpInfo['errcode'] == '40001' || $tmpInfo['errcode'] == '42001' || $tmpInfo['errcode'] == '42002' || $tmpInfo['errcode'] == '42003'){
				$access_token = $this->get_accesstoken($wx_info['wx_appid'], $wx_info['wx_appsecret'], $wx_id);
				 $ch = curl_init();  
		         curl_setopt($ch, CURLOPT_URL, "https://api.weixin.qq.com/cgi-bin/groups/members/update?access_token=".$access_token);  
		         curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");  
		         curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);  
		         curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);  
		         curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; MSIE 5.01; Windows NT 5.0)');  
		         curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);  
		         curl_setopt($ch, CURLOPT_AUTOREFERER, 1);  
		         curl_setopt($ch, CURLOPT_POSTFIELDS, $data);  
		         curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);  
		         $tmpInfo = curl_exec($ch);  
		         if (curl_errno($ch)) {  
		          echo json_encode(array('done'=>false,'msg'=>L('fans_far_request_failed')));die;
		         }
		         curl_close($ch);
		         $tmpInfo = json_decode($tmpInfo,true);
			}
			if($tmpInfo['errcode']==0){ 
				echo json_encode(array('done'=>true));die;
			}else{
				echo json_encode(array('done'=>false,'msg'=>$tmpInfo['errmsg']));die;
			}
		}
		//调取分组信息
		$url = 'https://api.weixin.qq.com/cgi-bin/groups/get?access_token='.$access_token;
		$res = @file_get_contents($url);
		$res = json_decode($res,true);
		if($res['errcode'] == '40001' || $res['errcode'] == '42001' || $res['errcode'] == '42002' || $res['errcode'] == '42003'){
			$access_token = $this->get_accesstoken($wx_info['wx_appid'], $wx_info['wx_appsecret'], $wx_id);
			$url = 'https://api.weixin.qq.com/cgi-bin/groups/get?access_token='.$access_token;
			$res = @file_get_contents($url);
			$res = json_decode($res,true);
		}
		Tpl::output('group_list',$res['errcode'] != ''?'error':$res['groups']);
		//调取粉丝信息
		$fans_id = intval($_GET['fans_id']);
		$fans_info = $model->table('fans')->where(array('fans_id'=>$fans_id))->find();
		Tpl::output('fans_info',$fans_info);
		Tpl::showpage('group_move');
	}
	/**
	 * 发信息
	 */
	public function send_msgOp(){
		$model = Model();
		$wx_id = intval($_GET['wx_id']);
		//开始向粉丝发送信息
		if(chksubmit()){
			//调取公众账号的接口信息
			$wx_info = $model->field('wx_appid,wx_appsecret,wx_accesstoken')->table('wxaccount')->where(array('wx_id'=>$wx_id))->find();
			$access_token = $wx_info['wx_accesstoken'];
			if($access_token == ''){
				$access_token = $this->get_accesstoken($wx_info['wx_appid'], $wx_info['wx_appsecret'], $wx_id);
			}
			$data = '{
					    "touser":"'.trim($_GET['openid']).'",
					    "msgtype":"text",
					    "text":
					    {
					         "content":"'.trim($_GET['msg']).'"
					    }
					}';
			 $ch = curl_init();  
	         curl_setopt($ch, CURLOPT_URL, "https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token=".$access_token);  
	         curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");  
	         curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);  
	         curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);  
	         curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; MSIE 5.01; Windows NT 5.0)');  
	         curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);  
	         curl_setopt($ch, CURLOPT_AUTOREFERER, 1);  
	         curl_setopt($ch, CURLOPT_POSTFIELDS, $data);  
	         curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);  
	         $tmpInfo = curl_exec($ch);  
	         if (curl_errno($ch)) {  
	          echo json_encode(array('done'=>false,'msg'=>L('fans_far_request_failed')));die;
	         }
	         curl_close($ch);
	         $tmpInfo = json_decode($tmpInfo,true);  
			if($tmpInfo['errcode'] == '40001' || $tmpInfo['errcode'] == '42001' || $tmpInfo['errcode'] == '42002' || $tmpInfo['errcode'] == '42003'){
				$access_token = $this->get_accesstoken($wx_info['wx_appid'], $wx_info['wx_appsecret'], $wx_id);
				 $ch = curl_init();  
		         curl_setopt($ch, CURLOPT_URL, "https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token=".$access_token);  
		         curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");  
		         curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);  
		         curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);  
		         curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; MSIE 5.01; Windows NT 5.0)');  
		         curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);  
		         curl_setopt($ch, CURLOPT_AUTOREFERER, 1);  
		         curl_setopt($ch, CURLOPT_POSTFIELDS, $data);  
		         curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);  
		         $tmpInfo = curl_exec($ch);  
		         if (curl_errno($ch)) {  
		          echo json_encode(array('done'=>false,'msg'=>L('fans_far_request_failed')));die;
		         }
		         curl_close($ch);
		         $tmpInfo = json_decode($tmpInfo,true);
			}
			if($tmpInfo['errcode']==0){ 
				echo json_encode(array('done'=>true));die;
			}else{
				echo json_encode(array('done'=>false,'msg'=>$tmpInfo['errmsg']));die;
			}
		}
		//调取粉丝信息
		$fans_id = intval($_GET['fans_id']);
		$fans_info = $model->table('fans')->where(array('fans_id'=>$fans_id))->find();
		Tpl::output('fans_info',$fans_info);
		Tpl::showpage('send_msg');
	}
}