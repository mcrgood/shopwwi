<?php
/**
 * 默认展示页面
 * @copyright  Copyright (c) 2007-2013 ShopWWI Inc. (http://www.shopwwi1.com)
 * @license    http://www.shopwwi2.com
 * @link       http://www.shopwwi.com
 * @since      File available since Release v1.1
 */
defined('InByShopWWI') or exit('Access Invalid!');
define('MYSQL_RESULT_TYPE',1);
class basicControl extends BaseHomeControl{
	
	public function __construct(){
		parent::__construct();
	}

	public function indexOp(){
		$this->listOp();
	}
	
	/*
	 * 文章列表
	 */
	public function listOp(){		
		$model = Model();
		$list = $model->table('basic')->where(array('wx_id'=>intval($_GET['wx_id'])))->select();
		
		Tpl::output('list',$list);
		
		Tpl::output('sign','basic');
		Tpl::showpage('basic.list');
	}

	/*
	 * 编辑文章
	 */	
	public function editBasicOp(){	
		if(isset($_POST) && !empty($_POST)){
			$params = array();
			$params['content'] = $_POST['content'];
				
			$model = Model();
			$condition = array();
			$condition['basic_id'] = intval($_POST['basic_id']);
			$condition['wx_id']	   = intval($_GET['wx_id']);	
						
			$res = $model->table('basic')->where($condition)->update($params);
			if($res){
				showMessage('编辑成功','?act=basic&wx_id='.intval($_GET['wx_id']),'succ');
			}else{
				showMessage('编辑失败','?act=basic&wx_id='.intval($_GET['wx_id']),'error');
			}
		}
		
		$condition = array();//条件
		$condition['wx_id'] = intval($_GET['wx_id']);
		$condition['basic_id'] = intval($_GET['basic_id']);
		
		$model = Model();
		$basic = $model->table('basic')->where($condition)->find();
		Tpl::output('basic',$basic);
		Tpl::output('sign','basic');
		Tpl::showpage('basic.edit');
			
	}	
}