<?php
/**
 * 微调研
 * */

defined('InByShopWWI') or exit('Access Invalid!');
class researchControl extends BaseHomeControl{
	var $wx_id;
	public function __construct(){
		parent::__construct();
		$this->wx_id = intval($_REQUEST['wx_id']);
		if($this->wx_id <= 0){
			showMessage('参数错误','','error');
		}
	}

	/**
	 * 调研活动列表
	 * */
	public function listOp(){
		$model = Model();
		$list = $model->table('research')->page('10')->select();
		Tpl::output('list',$list);
		Tpl::output('page',$model->showpage());
		Tpl::showpage('research_list');
	}

	/**
	 * 添加调研活动
	 * */
	public function add_researchOp(){
	    if(chksubmit()){
	    	$obj_validate = new Validate();
	    	$obj_validate->validateparam = array(
	    			array("input"=>$_POST["act_name"],		"require"=>"true", "message"=>'调研标题不能为空'),
	    			array("input"=>$_POST["act_keywords"],		"require"=>"true", "message"=>'调研关键词不能为空'),
	    			array("input"=>$_POST["start_time"],		"require"=>"true", "message"=>'调研开始时间不能为空'),
	    			array("input"=>$_POST["end_time"],		"require"=>"true", "message"=>'调研结束时间不能为空'),
	    			array("input"=>$_POST["act_content"],		"require"=>"true", "message"=>'调研说明不能为空'),
	    	);
	    	$error = $obj_validate->validate();
	    	if ($error != ''){
	    		showDialog($error);
	    	}else {
	    	    $act = array();
	    	    $dywt = $_POST['dywt'];
	    	    $act['item_name'] = trim($_POST['act_name']);
	    	    $act['keyword'] = trim($_POST['act_keywords']);
	    	    $act['item_content'] = trim($_POST['act_content']);
	    	    $act['start_time'] = strtotime(trim($_POST['start_time']));
	    	    $act['end_time'] = strtotime(trim($_POST['end_time']));
	    	    $act['wx_id'] = $this->wx_id;
	    	    $act['insert_time'] = time();
	    	    $act['item_question'] = serialize($dywt);
	    	    $file_name = '';
	    	    if($_FILES['act_image'] != ''){
	    	    	$name_type=substr($_FILES['act_image']['name'],-4,4);
					$file_name = md5(uniqid(rand(),true)).$name_type;
					move_uploaded_file($_FILES['act_image']['tmp_name'],BASE_UPLOAD_PATH.'/research/'.$file_name);
	    	    }
	    	    $act['item_image'] = $file_name!=''?UPLOAD_SITE_URL.'/research/'.$file_name:'';

	    	    $model = Model();
	    	    $res = $model->table('research')->insert($act);
				if($res){
				    $question = array();
				    foreach($dywt as $key => $val){
				    	if(empty($val))continue;
				        $tmp = array();
				        $tmp['research_id'] = $res;
				        $tmp['qtitle'] = trim($val['question']);
				        $tmp['qtype'] = isset($val['chkm'])?intval($val['chkm']):0;
				        $anwser = array();
				        $pp = array();
				        foreach($val['anwser'] as $k=>$v){
				        	if($v == '')continue;
				            $pp[] = 0;
				            $anwser[] = $v;
				        }
				        $tmp['qanwser'] = serialize($anwser);
				        $tmp['acount'] = serialize($pp);
				        $question[] = $tmp;
				    }
				    $res = $model->table('research_question')->insertAll($question);
				    if($res){
				    	showMessage('活动添加成功','?act=research&op=list&wx_id='.$this->wx_id,'succ');
				    }else{
				    	showMessage('活动添加失败','?act=research&op=list&wx_id='.$this->wx_id,'error');
				    }
				}else{
					showMessage('活动添加失败','?act=research&op=list&wx_id='.$this->wx_id,'error');
				}
	    	}
	    }
		Tpl::showpage('research_add');
	}

	/**
	 * 编辑微调研活动
	 * */
	public function edit_researchOp(){
		$res_id = intval($_REQUEST['res_id']);
		$model = Model();
		$res_list = $model->table('research')->where(array('item_id'=>$res_id))->find();
		$question_list = $model->table('research_question')->where(array('research_id'=>$res_id))->select();
		if($res_id <= 0){
	    	showMessage('参数错误','?act=research&op=list&wx_id='.$this->wx_id,'error');
	    }
		if(chksubmit()){
			$obj_validate = new Validate();
			$obj_validate->validateparam = array(
					array("input"=>$_POST["act_name"],		"require"=>"true", "message"=>'调研标题不能为空'),
					array("input"=>$_POST["act_keywords"],		"require"=>"true", "message"=>'调研关键词不能为空'),
					array("input"=>$_POST["start_time"],		"require"=>"true", "message"=>'调研开始时间不能为空'),
					array("input"=>$_POST["end_time"],		"require"=>"true", "message"=>'调研结束时间不能为空'),
					array("input"=>$_POST["act_content"],		"require"=>"true", "message"=>'调研说明不能为空'),
			);
			$error = $obj_validate->validate();
			if ($error != ''){
				showDialog($error);
			}else {
				$act = array();
				$dywt = $_POST['dywt'];
				$act['item_name'] = trim($_POST['act_name']);
				$act['keyword'] = trim($_POST['act_keywords']);
				$act['item_content'] = trim($_POST['act_content']);
				$act['start_time'] = strtotime(trim($_POST['start_time']));
				$act['end_time'] = strtotime(trim($_POST['end_time']));
				$act['item_question'] = serialize($dywt);
				$file_name = '';
				if($_FILES['act_image'] != ''){
					$name_type=substr($_FILES['act_image']['name'],-4,4);
					$file_name = md5(uniqid(rand(),true)).$name_type;
					move_uploaded_file($_FILES['act_image']['tmp_name'],BASE_UPLOAD_PATH.'/research/'.$file_name);
				}
				$act['item_image'] = $file_name!=''?UPLOAD_SITE_URL.'/research/'.$file_name:$_POST['ori_img'];

				$res = $model->table('research')->where(array('item_id'=>$res_id))->update($act);
				if($res){
					$question = array();
					foreach($dywt as $key => $val){
						$tmp = array();
						if(intval($val['qid'])>0){
							$tmp['qid'] = intval($val['qid']);
						}
						$tmp['research_id'] = $res;
						$tmp['qtitle'] = trim($val['question']);
						$tmp['qtype'] = isset($val['chkm'])?intval($val['chkm']):0;
						$anwser = array();
						$pp = array();
						foreach($val['anwser'] as $k=>$v){
							if($v == '')continue;
							$pp[] = $val['acount'][$k];
							$anwser[] = $v;
						}
						$tmp['qanwser'] = serialize($anwser);
						$tmp['acount'] = serialize($pp);
						$question[] = $tmp;
					}
					$res = $model->table('research_question')->insertAll($question,array(),true);
					if($res){
						if(trim($_POST['del_list']) != ''){
						    $strd = substr(trim($_POST['del_list']),1);
						    $d_arr = explode(',',$strd);
						    $model->table('research_question')->where(array('qid'=>array('in',$d_arr)))->delete();
						}
						showMessage('活动编辑成功','?act=research&op=list&wx_id='.$this->wx_id,'succ');
					}else{
						showMessage('活动编辑失败','?act=research&op=list&wx_id='.$this->wx_id,'error');
					}
				}else{
					showMessage('活动编辑失败','?act=research&op=list&wx_id='.$this->wx_id,'error');
				}
			}
		}
		Tpl::output('info',$res_list);
		Tpl::output('list',$question_list);
		Tpl::showpage('research_edit');
	}

	/**
	 * 删除调研活动
	 * */
	public function delOp(){
	    $res_id = intval($_GET['res_id']);
	    if($res_id <= 0){
	    	showMessage('参数错误','?act=research&op=list&wx_id='.$this->wx_id,'error');
	    }
	    $model = Model();
	    $res = $model->table('research_question')->where(array('research_id'=>$res_id))->delete();
	    if($res){
	        $res = $model->table('research')->where(array('item_id'=>$res_id))->delete();
	        if($res){
	        	showMessage('活动删除成功','?act=research&op=list&wx_id='.$this->wx_id,'succ');
	        }else{
	        	showMessage('活动删除失败','?act=research&op=list&wx_id='.$this->wx_id,'error');
	        }
	    }else{
	    	showMessage('活动删除失败','?act=research&op=list&wx_id='.$this->wx_id,'error');
	    }
	}

	/**
	 * 设置调研活动奖项
	 * */
	public function researchOp(){
		$res_id = intval($_REQUEST['res_id']);
		if($res_id <= 0){
			showMessage('参数错误','?act=research&op=list&wx_id='.$this->wx_id,'error');
		}
		$model = Model();
		$info = $model->table('research_lottery')->where(array('res_id'=>$res_id))->find();
		if(chksubmit()){
			$obj_validate = new Validate();
			$obj_validate->validateparam = array(
					array("input"=>$_POST["start_title"],		"require"=>"true", "message"=>'兑奖信息不能为空'),
					array("input"=>$_POST["start_content"],		"require"=>"true", "message"=>'中奖提示不能为空'),
					array("input"=>$_POST["end_title"],		"require"=>"true", "message"=>'活动结束公告主题不能为空'),
					array("input"=>$_POST["end_content"],		"require"=>"true", "message"=>'活动结束说明不能为空'),
					array("input"=>$_POST["join_count"],		"require"=>"true", "message"=>'预计参与人数不能为空'),
					array("input"=>$_POST["lot_password"],		"require"=>"true", "message"=>'兑奖密码不能为空'),
			);
			$error = $obj_validate->validate();
			if ($error != ''){
				showDialog($error);
			}else {
				$res_arr = $model->table('research')->field('item_name,keyword,start_time,end_time')->where(array('item_id'=>$res_id))->find();
				$lottery = array();
				$lottery['lot_title'] = $res_arr['item_name'];
				$lottery['lot_keyword'] = $res_arr['keyword'];
				$lottery['start_title'] = trim($_POST['start_title']);
				$lottery['start_content'] = trim($_POST['start_content']);
				$lottery['end_title'] = trim($_POST['end_title']);
				$lottery['end_content'] = trim($_POST['end_content']);
				$lottery['lot_password'] = md5($_POST['lot_password']);
				$lottery['join_count'] = intval($_POST['join_count']);
				$lottery['isshow_lotcount'] = intval($_POST['isshow_lotcount']);
				$lottery['end_time'] = $res_arr['end_time'];
				$lottery['start_time'] = $res_arr['start_time'];
				$lottery['wx_id'] = $this->wx_id;
				$lot = array();
				foreach($_POST['dyjx'] as $k=>$v){
				    if(trim($v['jp'])!='' && intval($v['num'])>0){
				        $lot[] = $v;
				    }
				}
				$lottery['lot_info'] = serialize($lot);
				$lottery['res_id'] = $res_id;
				$res =  $model->table('research_lottery')->insert($lottery);
				if($res){
		        	showMessage('奖项设置成功','?act=research&op=list&wx_id='.$this->wx_id,'succ');
		        }else{
		        	showMessage('奖项设置失败','?act=research&op=list&wx_id='.$this->wx_id,'error');
		        }
			}
		}
		$template = 'research_research.list';
		if(empty($info)){
		    $template = 'research_research.add';
		}else{
		    Tpl::output('info',$info);
		}
		Tpl::showpage($template);
	}

	/**
	 * 编辑调研活动奖项
	 * */
	public function res_editOp(){
		$lot_id = intval($_REQUEST['lot_id']);
		if($lot_id <= 0){
			showMessage('参数错误','?act=research&op=list&wx_id='.$this->wx_id,'error');
		}
		$model = Model();
		if(chksubmit()){
			$obj_validate = new Validate();
			$obj_validate->validateparam = array(
					array("input"=>$_POST["start_title"],		"require"=>"true", "message"=>'兑奖信息不能为空'),
					array("input"=>$_POST["start_content"],		"require"=>"true", "message"=>'中奖提示不能为空'),
					array("input"=>$_POST["end_title"],		"require"=>"true", "message"=>'活动结束公告主题不能为空'),
					array("input"=>$_POST["end_content"],		"require"=>"true", "message"=>'活动结束说明不能为空'),
					array("input"=>$_POST["join_count"],		"require"=>"true", "message"=>'预计参与人数不能为空'),
			);
			$error = $obj_validate->validate();
			if ($error != ''){
				showDialog($error);
			}else {
				$lottery = array();
				$lottery['start_title'] = trim($_POST['start_title']);
				$lottery['start_content'] = trim($_POST['start_content']);
				$lottery['end_title'] = trim($_POST['end_title']);
				$lottery['end_content'] = trim($_POST['end_content']);
				if(trim($_POST['lot_password']) != ''){
					$lottery['lot_password'] = md5($_POST['lot_password']);
				}
				$lottery['join_count'] = intval($_POST['join_count']);
				$lottery['isshow_lotcount'] = intval($_POST['isshow_lotcount']);
				$lot = array();
				foreach($_POST['dyjx'] as $k=>$v){
					if(trim($v['jp'])!='' && intval($v['num'])>0){
						$lot[] = $v;
					}
				}
				$lottery['lot_info'] = serialize($lot);
				$res =  $model->table('research_lottery')->where(array('lot_id'=>$lot_id))->update($lottery);
				if($res){
					showMessage('奖项设置成功','?act=research&op=list&wx_id='.$this->wx_id,'succ');
				}else{
					showMessage('奖项设置失败','?act=research&op=list&wx_id='.$this->wx_id,'error');
				}
			}
		}
		$info = $model->table('research_lottery')->where(array('lot_id'=>$lot_id))->find();
		Tpl::output('info',$info);
		Tpl::showpage('research_research.edit');
	}

	/**
	 * 删除调研活动奖项
	 * */
	public function res_delOp(){
		$res_id = intval($_GET['lot_id']);
		if($res_id <= 0){
			showMessage('参数错误','?act=research&op=list&wx_id='.$this->wx_id,'error');
		}
		$model = Model();
		$res = $model->table('research_lottery')->where(array('lot_id'=>$res_id))->delete();
		if($res){
			showMessage('活动奖项删除成功','?act=research&op=list&wx_id='.$this->wx_id,'succ');
		}else{
			showMessage('活动奖项删除失败','?act=research&op=list&wx_id='.$this->wx_id,'error');
		}
	}

	/**
	 * 获奖管理
	 * */
	public function lotteryOp(){
		$res_id = intval($_GET['res_id']);
		if($res_id <= 0){
			showMessage('参数错误','?act=research&op=list&wx_id='.$this->wx_id,'error');
		}
		$model = Model();
		$list = $model->table('research_lottery_record')->where(array('res_id'=>$res_id))->page(10)->order('send_stat asc,is_lot desc,prize asc')->select();
		$page = $model->showpage();
		$islot_count = $model->table('research_lottery_record')->where(array('res_id'=>$res_id,'is_lot'=>1))->count();
		$sent_count = $model->table('research_lottery_record')->where(array('res_id'=>$res_id,'send_stat'=>1))->count();
		$lot = $model->table('research_lottery')->where(array('res_id'=>$res_id))->find();
		$lot_info = unserialize($lot['lot_info']);
		$lot_num = 0;
		foreach((array)$lot_info as $k=>$v){
		    $lot_num += $v['num'];
		}
		Tpl::output('list',$list);
		Tpl::output('islot',$islot_count);
		Tpl::output('send_count',$sent_count);
		Tpl::output('lot_count',$lot_num);
		Tpl::output('lot',$lot);
		Tpl::output('page',$page);
		Tpl::showpage('research_lottery.list');
	}

	/**
	 * 发奖
	 * */
	public function lot_sendOp(){
	    $record_id = intval($_GET['research_id']);
	    $res_id = intval($_GET['res_id']);
	    if($record_id <= 0){
	    	showMessage('参数错误','?act=research&op=lottery&res_id='.$res_id.'&wx_id='.$this->wx_id,'error');
	    }
	    $model = Model();
	    $arr_update = array();
	    $arr_update['send_stat'] = 1;
	    $arr_update['send_time'] = time();
	    $model->table('research_lottery_record')->where(array('record_id'=>$record_id))->update($arr_update);
	    echo '<script>location.href="index.php??act=research&op=lottery&res_id='.$res_id.'&wx_id='.$this->wx_id.'"</script>';
	    exit;
	}

	/**
	 * 取消发奖
	 * */
	public function chanle_lotOp(){
		$record_id = intval($_GET['research_id']);
		$res_id = intval($_GET['res_id']);
		if($record_id <= 0){
			showMessage('参数错误','?act=research&op=lottery&res_id='.$res_id.'&wx_id='.$this->wx_id,'error');
		}
		$model = Model();
		$arr_update = array();
		$arr_update['send_stat'] = 0;
		$arr_update['send_time'] = 0;
		$model->table('research_lottery_record')->where(array('record_id'=>$record_id))->update($arr_update);
		echo '<script>location.href="index.php??act=research&op=lottery&res_id='.$res_id.'&wx_id='.$this->wx_id.'"</script>';
		exit;
	}

	/**
	 * 删除抽奖信息
	 * */
	public function del_lotOp(){
		$record_id = intval($_GET['research_id']);
		$res_id = intval($_GET['res_id']);
		if($record_id <= 0){
			showMessage('参数错误','?act=research&op=lottery&res_id='.$res_id.'&wx_id='.$this->wx_id,'error');
		}
		$model = Model();
		$res = $model->table('research_lottery_record')->where(array('record_id'=>$record_id))->delete();
		if($res){
			showMessage('删除成功','?act=research&op=lottery&res_id='.$res_id.'&wx_id='.$this->wx_id,'succ');
		}else{
			showMessage('删除失败','?act=research&op=lottery&res_id='.$res_id.'&wx_id='.$this->wx_id,'error');
		}
	}

	/**
	 * 活动统计
	 * */
	public function infoOp(){
		$res_id = intval($_GET['res_id']);
		if($res_id <= 0){
			showMessage('参数错误','?act=research&op=list&wx_id='.$this->wx_id,'error');
		}
		$model = Model();
		$list = $model->table('research_question')->where(array('research_id'=>$res_id))->page('10')->select();
		Tpl::output('list',$list);
		Tpl::output('page',$model->showpage());
		Tpl::showpage('research_info');
	}

}