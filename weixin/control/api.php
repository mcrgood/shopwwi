<?php
/**
 * 默认展示页面
 * @copyright  Copyright (c) 2007-2013 ShopWWI Inc. (http://www.shopwwi1.com)
 * @license    http://www.shopwwi2.com
 * @link       http://www.shopwwi.com
 * @since      File available since Release v1.1
 */
defined('InByShopWWI') or exit('Access Invalid!');
define('MYSQL_RESULT_TYPE',1);
class apiControl extends Control{
	/*
	 * 构造方法
	 */
	public function __construct(){
		$wx_token = trim($_GET['token']);
		$model = Model();
		$valid = $model->table('wxaccount')->where(array('wx_token'=>$wx_token,'wx_is_verify'=>1))->find();
		if(empty($valid)){
			$this->valid($wx_token);
		}
		Language::read('weixin_wall');
	}

	/*
	 * 验证接入成功
	 */
	private function valid($wx_token){
		$echoStr = $_GET["echostr"];
		$signature = $_GET["signature"];
        $timestamp = $_GET["timestamp"];
        $nonce = $_GET["nonce"];

		$tmpArr = array($wx_token, $timestamp, $nonce);
		sort($tmpArr);
		$tmpStr = implode( $tmpArr );
		$tmpStr = sha1( $tmpStr );
		if( $tmpStr == $signature ){
			$model = Model();
			$res = $model->table('wxaccount')->where(array('wx_token'=>$wx_token))->update(array('wx_is_verify'=>1));//1.接入成功
			if($res){
				echo $echoStr;
			}
		}
		exit;
	}

	/*
	 * 接受消息
	 */
	public function responseMsgOp(){
		$model = Model();
		$postStr = $GLOBALS["HTTP_RAW_POST_DATA"];
		if(!empty($postStr)){
			$postObj = simplexml_load_string($postStr, 'SimpleXMLElement', LIBXML_NOCDATA);
			$postObj = (array)$postObj;
			$fromUsername = $postObj['FromUserName'];
            $toUsername = $postObj['ToUserName'];
            $createtime = $postObj['CreateTime'];
            $content = trim($postObj['Content']);
            //事件响应
            $msgtype = trim($postObj['MsgType']);

            if($msgtype == 'event'){
            	$event = trim($postObj['Event']);
            	switch ($event){
            	    case 'MASSSENDJOBFINISH':
            	    	$msg_id = trim($postObj['MsgID']);
            	    	$succ_count = intval($postObj['SentCount']);
            	    	$model->table('group_message')->where(array('wx_msg_id'=>$msg_id))->update(array('succ_count'=>$succ_count));
            	    	break;
            		case 'CLICK'://自定义菜单点击
            			$eventkey = trim($postObj['EventKey']);
            			$cr_info = $model->table('click_reply')->where(array('cr_code'=>$eventkey))->find();
            			if(empty($cr_info)){
            				exit;
            			}
            			$fans_one = $model->table('fans')->where(array('fans_openid'=>$fromUsername))->find();
            			if(!empty($fans_one)){
            				$activ_in = array('act_type'=>'CLICK','act_content'=>'自定义菜单'.$cr_info['cr_content'],'act_time'=>time(),'fans_openid'=>$fromUsername);
            				$wxinfo = $model->table('wxaccount')->where(array('wx_ori_id'=>$toUsername))->find();
            				$activ_in['fans_id'] = $fans_one['fans_id'];
            				$activ_in['wx_id'] = $wxinfo['wx_id'];
            				$model->table('fans_activity')->insert($activ_in);
            			}
            			if($cr_info['cr_type'] == 1){//纯文字
            				$reply_data = '<xml>
											<ToUserName><![CDATA['.$fromUsername.']]></ToUserName>
											<FromUserName><![CDATA['.$toUsername.']]></FromUserName>
											<CreateTime>'.time().'</CreateTime>
											<MsgType><![CDATA[text]]></MsgType>
											<Content><![CDATA['.$cr_info['cr_content'].']]></Content>
											</xml>';
            			}
            			if($cr_info['cr_type'] == 2){//图文
            				$cr_content = unserialize($cr_info['cr_content']);
            				$reply_data = '<xml>
											<ToUserName><![CDATA['.$fromUsername.']]></ToUserName>
											<FromUserName><![CDATA['.$toUsername.']]></FromUserName>
											<CreateTime>'.time().'</CreateTime>
											<MsgType><![CDATA[news]]></MsgType>
											<ArticleCount>'.count($cr_content).'</ArticleCount>
											<Articles>';
            				if(!empty($cr_content)){
            					foreach ($cr_content as $k=>$v){
            						$reply_data .= '<item>
													<Title><![CDATA['.$v['title'].']]></Title>
													<Description><![CDATA['.$v['desc'].']]></Description>
													<PicUrl><![CDATA['.$v['picurl'].']]></PicUrl>
													<Url><![CDATA['.$v['url'].']]></Url>
													</item>';
            					}
            				}
            				$reply_data .= '</Articles>
											</xml>';
            			}
            			echo $reply_data;exit;
            			break;
            		case 'subscribe'://关注账号
            			$wx_info = $model->table('wxaccount')->where(array('wx_ori_id'=>$toUsername))->find();
		            	$access_token = $wx_info['wx_accesstoken'];
						if($access_token == ''){
							$access_token = $this->get_accesstoken($wx_info['wx_appid'], $wx_info['wx_appsecret'], $wx_info['wx_id']);
						}
						$data = @file_get_contents("https://api.weixin.qq.com/cgi-bin/user/info?access_token=".$access_token."&openid=".$fromUsername);
						$fans = json_decode($data,true);
						if($fans['errcode'] == '40001' || $fans['errcode'] == '42001' || $fans['errcode'] == '42002' || $fans['errcode'] == '42003'){
							$access_token = $this->get_accesstoken($wx_info['wx_appid'], $wx_info['wx_appsecret'], $wx_info['wx_id']);
							$data = @file_get_contents("https://api.weixin.qq.com/cgi-bin/user/info?access_token=".$access_token."&openid=".$fromUsername);
							$fans = json_decode($data,true);
							if($fans['errcode'] != ''){
								exit;
							}
						}
//						$textTpl = "<xml>
//							<ToUserName><![CDATA[%s]]></ToUserName>
//							<FromUserName><![CDATA[%s]]></FromUserName>
//							<CreateTime>%s</CreateTime>
//							<MsgType><![CDATA[%s]]></MsgType>
//							<Content><![CDATA[%s]]></Content>
//							<FuncFlag>0</FuncFlag>
//						</xml>";
//
//						$resultStr = sprintf($textTpl, $fromUsername, $toUsername, time(), "text", var_export($fans,true));
//						echo $resultStr;die;
            			$params['fans_openid']	= $fans['openid'];
            			$params['wx_id']		= $wx_info['wx_id'];
            			$params['fans_nickname']= $fans['nickname'];
            			$params['fans_sex']		= $fans['sex'];
            			$params['fans_language']= $fans['language'];
            			$params['fans_city']	= $fans['city'];
            			$params['fans_province']= $fans['province'];
            			$params['fans_country']	= $fans['country'];
            			$params['fans_headimgurl'] = $fans['headimgurl'];
            			$params['fans_subtime']	= $fans['subscribe_time'];
            			$fans_id = $model->table('fans')->insert($params);
            			$activ_in = array('act_type'=>'subscribe','act_content'=>'关注','act_time'=>time(),'fans_openid'=>$fans['openid']);
            			$activ_in['fans_id'] = $fans_id;
            			$activ_in['wx_id'] = $wx_info['wx_id'];
            			$model->table('fans_activity')->insert($activ_in);

            			//发送欢迎信息
            			$rs_info = $model->field('reply_setting.*,reply.reply_type,reply.reply_content')->table('reply_setting,reply')->join('left join')->on('reply_setting.reply_id=reply.reply_id')->where(array('reply_setting.wx_id'=>$wx_info['wx_id'],'reply_setting.rs_type'=>1))->find();
            			if(empty($rs_info)){
            				exit;
            			}
            			if($rs_info['rs_use'] == 1){
            				if($rs_info['reply_type'] == 1){//纯文字
            				$reply_data = '<xml>
											<ToUserName><![CDATA['.$fromUsername.']]></ToUserName>
											<FromUserName><![CDATA['.$toUsername.']]></FromUserName>
											<CreateTime>'.time().'</CreateTime>
											<MsgType><![CDATA[text]]></MsgType>
											<Content><![CDATA['.$rs_info['reply_content'].']]></Content>
											</xml>';
	            			}
	            			if($rs_info['reply_type'] == 2){//图文
	            				$reply_content = unserialize($rs_info['reply_content']);
	            				$reply_data = '<xml>
												<ToUserName><![CDATA['.$fromUsername.']]></ToUserName>
												<FromUserName><![CDATA['.$toUsername.']]></FromUserName>
												<CreateTime>'.time().'</CreateTime>
												<MsgType><![CDATA[news]]></MsgType>
												<ArticleCount>'.count($reply_content).'</ArticleCount>
												<Articles>';
	            				if(!empty($reply_content)){
	            					foreach ($reply_content as $k=>$v){
	            						$reply_data .= '<item>
														<Title><![CDATA['.$v['title'].']]></Title>
														<Description><![CDATA['.$v['desc'].']]></Description>
														<PicUrl><![CDATA['.$v['picurl'].']]></PicUrl>
														<Url><![CDATA['.$v['url'].']]></Url>
														</item>';
	            					}
	            				}
	            				$reply_data .= '</Articles>
												</xml>';
	            			}
	            			if($rs_info['reply_type'] == 3){//语音
	            				$reply_content = unserialize($rs_info['reply_content']);
	            				$reply_data = '<xml>
											<ToUserName><![CDATA['.$fromUsername.']]></ToUserName>
											<FromUserName><![CDATA['.$toUsername.']]></FromUserName>
											<CreateTime>'.time().'</CreateTime>
											<MsgType><![CDATA[music]]></MsgType>
											<Music>
											<Title><![CDATA['.$v['title'].']]></Title>
											<Description><![CDATA['.$v['desc'].']]></Description>
											<MusicUrl><![CDATA['.$v['url'].']]></MusicUrl>
											<HQMusicUrl><![CDATA['.$v['hqurl'].']]></HQMusicUrl>
											</Music>
											</xml>';
	            			}
	            			echo $reply_data;exit;
            			}else{
            				exit;
            			}
            			break;
            	case 'unsubscribe':
            		$model->table('fans')->where(array('fans_openid'=>$fromUsername))->delete();
            		break;
            	}
            }

            $wx_info = $model->table('wxaccount')->where(array('wx_ori_id'=>$toUsername))->find();
            //微调研部分
            $wxresearch = $model->table('research')->where(array('keyword'=>$content,'wx_id'=>$wx_info['wx_id'],'start_time'=>array('lt',time()),'end_time'=>array('gt',time())))->find();
            if(!empty($wxresearch)){
                $params = array(
                		'tousername'	=>	$postObj['ToUserName'],
                		'fromusername'	=>	$postObj['FromUserName'],
                		'createtime'	=>	$postObj['CreateTime'],
                		'content'		=>	$content,
                		'keyword'		=>	$keyword,
                		'wx_id'         =>  $wx_info['wx_id'],
                		'item_id'		=> $wxresearch['item_id']
                );
                $res = $this->addResearchItem($params);
                exit;
            }

            //微信墙部分
			$content_pattern = "/\#([^\#|.]+)\#/";
			preg_match_all($content_pattern, $content, $keywordarr);
			$keyword = implode(',',$keywordarr[1]);
			$activity = $model->table('activity')->where(array('activity_name'=>$keyword))->find();
			if(!empty($activity)){
				$params	=	array(
					'tousername'	=>	$postObj['ToUserName'],
					'fromusername'	=>	$postObj['FromUserName'],
					'createtime'	=>	$postObj['CreateTime'],
					'content'		=>	$content,
					'activity_name'	=>	$keyword,
					'wx_id'         =>  $wx_info['wx_id']
				);
				$res = $this->addMessage($params);
				exit;
			}

			$vote = $model->table('vote')->where(array('vote_name'=>$keyword))->find();
			if(!empty($vote)){
				$params	=	array(
						'tousername'	=>	$postObj['ToUserName'],
						'fromusername'	=>	$postObj['FromUserName'],
						'createtime'	=>	$postObj['CreateTime'],
						'content'		=>	str_replace('#'.$keyword.'#','',$content)
				);
				$this->addVoteItem($params);
			}
			//将信息写库
			$fans_info = $model->table('fans')->where(array('fans_openid'=>$fromUsername))->find();
			$insert_array = array();
			$insert_array['user_openid']	 = $fans_info['fans_openid'];
			$insert_array['user_name']		 = $fans_info['fans_nickname'];
			$insert_array['activity_id']	 = 0;
			$insert_array['activity_name']	 = '';
			$insert_array['message_time']	 = intval($createtime);
			$insert_array['message_content'] = $content;
			$insert_array['wx_id']           = $wx_info['wx_id'];
			$insert_array['is_important']    = 0;
			$res = $model->table('message')->insert($insert_array);
			//关键字自动回复
			$reply_info = $model->field('reply.reply_type,reply.reply_content')->table('keyword_reply,reply')->join('left join')->on('keyword_reply.reply_id=reply.reply_id')->where(array('keyword_reply.wx_id'=>$wx_info['wx_id'],'keyword_reply.kr_keyword'=>array('like','%,'.$content.',%'),'keyword_reply.kr_use'=>1))->find();
			if(!empty($reply_info)){
				$acty = array(
						'type' => 'select',
						'content' => '关键词查询',
						'fromusername' => $fromUsername,
						'tousername' => $toUsername
				);
				$this->activityCount($acty);
				if($reply_info['reply_type'] == 1){//纯文字
	            $reply_data = '<xml>
							<ToUserName><![CDATA['.$fromUsername.']]></ToUserName>
							<FromUserName><![CDATA['.$toUsername.']]></FromUserName>
							<CreateTime>'.time().'</CreateTime>
							<MsgType><![CDATA[text]]></MsgType>
							<Content><![CDATA['.$reply_info['reply_content'].']]></Content>
							</xml>';
	            }
	            if($reply_info['reply_type'] == 2){//图文
		            $reply_content = unserialize($reply_info['reply_content']);
		            $reply_data = '<xml>
									<ToUserName><![CDATA['.$fromUsername.']]></ToUserName>
									<FromUserName><![CDATA['.$toUsername.']]></FromUserName>
									<CreateTime>'.time().'</CreateTime>
									<MsgType><![CDATA[news]]></MsgType>
									<ArticleCount>'.count($reply_content).'</ArticleCount>
									<Articles>';
		            if(!empty($reply_content)){
		            	foreach ($reply_content as $k=>$v){
		            		$reply_data .= '<item>
											<Title><![CDATA['.$v['title'].']]></Title>
											<Description><![CDATA['.$v['desc'].']]></Description>
											<PicUrl><![CDATA['.$v['picurl'].']]></PicUrl>
											<Url><![CDATA['.$v['url'].']]></Url>
											</item>';
		            	}
		            }
		            $reply_data .= '</Articles>
									</xml>';
	            }
	            if($rs_info['reply_type'] == 3){//语音
	            	$reply_content = unserialize($rs_info['reply_content']);
	            	$reply_data = '<xml>
											<ToUserName><![CDATA['.$fromUsername.']]></ToUserName>
											<FromUserName><![CDATA['.$toUsername.']]></FromUserName>
											<CreateTime>'.time().'</CreateTime>
											<MsgType><![CDATA[music]]></MsgType>
											<Music>
											<Title><![CDATA['.$v['title'].']]></Title>
											<Description><![CDATA['.$v['desc'].']]></Description>
											<MusicUrl><![CDATA['.$v['url'].']]></MusicUrl>
											<HQMusicUrl><![CDATA['.$v['hqurl'].']]></HQMusicUrl>
											</Music>
											</xml>';
	            }
	            echo $reply_data;exit;
			}
            //消息自动回复
			$rs_info = $model->field('reply_setting.*,reply.reply_type,reply.reply_content')->table('reply_setting,reply')->join('left join')->on('reply_setting.reply_id=reply.reply_id')->where(array('reply_setting.wx_id'=>$wx_info['wx_id'],'reply_setting.rs_type'=>2))->find();
            if(empty($rs_info)){
            	exit;
            }
			if($rs_info['rs_use'] == 1){
				$acty = array(
						'type' => 'select',
						'content' => '消息查询',
						'fromusername' => $fromUsername,
						'tousername' => $toUsername
				);
				$this->activityCount($acty);
            	if($rs_info['reply_type'] == 1){//纯文字
            	$reply_data = '<xml>
								<ToUserName><![CDATA['.$fromUsername.']]></ToUserName>
								<FromUserName><![CDATA['.$toUsername.']]></FromUserName>
								<CreateTime>'.time().'</CreateTime>
								<MsgType><![CDATA[text]]></MsgType>
								<Content><![CDATA['.$rs_info['reply_content'].']]></Content>
								</xml>';
	            }
	            if($cr_info['reply_type'] == 2){//图文
	            $reply_content = unserialize($rs_info['reply_content']);
	            $reply_data = '<xml>
								<ToUserName><![CDATA['.$fromUsername.']]></ToUserName>
								<FromUserName><![CDATA['.$toUsername.']]></FromUserName>
								<CreateTime>'.time().'</CreateTime>
								<MsgType><![CDATA[news]]></MsgType>
								<ArticleCount>'.count($reply_content).'</ArticleCount>
								<Articles>';
	            if(!empty($reply_content)){
	            	foreach ($reply_content as $k=>$v){
	            		$reply_data .= '<item>
										<Title><![CDATA['.$v['title'].']]></Title>
										<Description><![CDATA['.$v['desc'].']]></Description>
										<PicUrl><![CDATA['.$v['picurl'].']]></PicUrl>
										<Url><![CDATA['.$v['url'].']]></Url>
										</item>';
	            	}
	            }
	            $reply_data .= '</Articles>
								</xml>';
	            }
	            if($rs_info['reply_type'] == 3){//语音
	            	$reply_content = unserialize($rs_info['reply_content']);
	            	$reply_data = '<xml>
									<ToUserName><![CDATA['.$fromUsername.']]></ToUserName>
									<FromUserName><![CDATA['.$toUsername.']]></FromUserName>
									<CreateTime>'.time().'</CreateTime>
									<MsgType><![CDATA[music]]></MsgType>
									<Music>
									<Title><![CDATA['.$v['title'].']]></Title>
									<Description><![CDATA['.$v['desc'].']]></Description>
									<MusicUrl><![CDATA['.$v['url'].']]></MusicUrl>
									<HQMusicUrl><![CDATA['.$v['hqurl'].']]></HQMusicUrl>
									</Music>
									</xml>';
	            }
	            echo $reply_data;exit;
            }

            /**
             * 以上自动回复失败转入人工客服
             * */
            $acty = array(
            		'type' => 'select',
            		'content' => '人工客服咨询',
            		'fromusername' => $fromUsername,
            		'tousername' => $toUsername
            );
            $this->activityCount($acty);exit;
		}
		exit;
	}

	/*
	 * 消息写入数据库
	 */
	private function addMessage($params){
		$model = Model();
		$activity = $model->table('activity')->where(array('activity_name'=>$params['activity_name']))->find();
		if(empty($activity)){//活动不存在
			exit;
		}

		$fans = $model->table('fans')->where(array('fans_openid'=>$params['fromusername']))->find();

		if(empty($fans)){//粉丝不存在
			exit;
		}

		$data					= array();
		$data['user_openid']	= $fans['fans_openid'];
		$data['user_name']		= $fans['fans_nickname'];
		$data['activity_id']	= $activity['activity_id'];
		$data['activity_name']	= $activity['activity_name'];
		$data['message_time']	= $params['createtime'];
		$data['message_content']= $params['content'];
		$data['wx_id']          = $params['wx_id'];
		$data['is_important']   = 0;

		$res = $model->table('message')->insert($data);
		if($res){
			$acty = array(
					'type' => 'activity',
					'content' => '推广活动_微信墙',
					'fromusername' => $params['fromusername'],
					'tousername' => $params['tousername']
			);
			$this->activityCount($acty);
			$filename = $this->save_img($fans['fans_headimgurl'],$fans['fans_openid'],$activity['activity_wx_id']);
			if(!empty($filename)){
				$model->table('fans')->where(array('fans_id'=>$fans['fans_id']))->update(array('fans_pic'=>$filename));
			}
			$textTpl = "<xml>
							<ToUserName><![CDATA[%s]]></ToUserName>
							<FromUserName><![CDATA[%s]]></FromUserName>
							<CreateTime>%s</CreateTime>
							<MsgType><![CDATA[%s]]></MsgType>
							<Content><![CDATA[%s]]></Content>
							<FuncFlag>0</FuncFlag>
						</xml>";

			$resultStr = sprintf($textTpl, $params['fromusername'], $params['tousername'], time(), "text", L('nc_weixin_wall_activity_joinin_succ'));
			echo $resultStr;
		}
		exit;
	}


	/*
	 * 投票写入数据库
	 */
	private function addVoteItem($params){
		$model = Model();
		$res = $model->table('vote_item')->where(array('item_name'=>$params['content']))->setInc('item_num',1);

		if($res){
			$acty = array(
					'type' => 'activity',
					'content' => '推广活动_投票',
					'fromusername' => $params['fromusername'],
					'tousername' => $params['tousername']
			);
			$this->activityCount($acty);
			$textTpl = "<xml>
							<ToUserName><![CDATA[%s]]></ToUserName>
							<FromUserName><![CDATA[%s]]></FromUserName>
							<CreateTime>%s</CreateTime>
							<MsgType><![CDATA[%s]]></MsgType>
							<Content><![CDATA[%s]]></Content>
							<FuncFlag>0</FuncFlag>
						</xml>";
			$resultStr = sprintf($textTpl,$params['fromusername'],$params['tousername'],time(),"text",L('nc_weixin_wall_vote_item_joinin_succ'));
			echo $resultStr;
		}
		exit;
	}

	/**
	 * 调研处理
	 * */
	private function addResearchItem($params){
		$model = Model();
		$res = $model->table('research')->where(array('item_id'=>$params['item_id']))->find();
		if($res){
			$acty = array(
					'type' => 'activity',
					'content' => '推广活动_微调研',
					'fromusername' => $params['fromusername'],
					'tousername' => $params['tousername']
			);
			$this->activityCount($acty);
			$url = SHOP_SITE_URL.'/index.php?act=wap_research&wx_id='.$params['wx_id'].'&item_id='.$params['item_id'].'&wxchat_id='.$params['fromusername'];
			$reply_data = '<xml>
								<ToUserName><![CDATA['.$params['fromusername'].']]></ToUserName>
								<FromUserName><![CDATA['.$params['tousername'].']]></FromUserName>
								<CreateTime>'.time().'</CreateTime>
								<MsgType><![CDATA[news]]></MsgType>
								<ArticleCount>1</ArticleCount>
								<Articles>
										<item>
											<Title><![CDATA['.$res['item_name'].']]></Title>
											<Description><![CDATA['.$res['item_content'].']]></Description>
											<PicUrl><![CDATA['.$res['item_image'].']]></PicUrl>
											<Url><![CDATA['.$url.']]></Url>
										</item>
								</Articles>
							</xml>';
			echo $reply_data;exit;
		}
		exit;
	}

	/**
	 * 行为统计
	 * */
	private function activityCount($param){
		$model = Model();
		$fans_one = $model->table('fans')->where(array('fans_openid'=>$param['fromusername']))->find();
		if(!empty($fans_one)){
			$activ_in = array('act_type'=>$param['type'],'act_content'=>$param['content'],'act_time'=>time(),'fans_openid'=>$param['fromusername']);
			$wxinfo = $model->table('wxaccount')->where(array('wx_ori_id'=>$param['tousername']))->find();
			$activ_in['fans_id'] = $fans_one['fans_id'];
			$activ_in['wx_id'] = $wxinfo['wx_id'];
			$model->table('fans_activity')->insert($activ_in);
		}
	}
}