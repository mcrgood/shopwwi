<?php
/**
 * 默认展示页面
 * @copyright  Copyright (c) 2007-2013 ShopWWI Inc. (http://www.shopwwi1.com)
 * @license    http://www.shopwwi2.com
 * @link       http://www.shopwwi.com
 * @since      File available since Release v1.1
 */
defined('InByShopWWI') or exit('Access Invalid!');
define('MYSQL_RESULT_TYPE',1);
class wallControl extends WallBaseControl{
	
	public function __construct(){
		parent::__construct();
	}
	
	/*
	 * 消息墙
	 */
	public function messageOp(){
		$model = Model();
		$activity = $model->table('activity')->where(array('activity_id'=>intval($_GET['activity_id'])))->find();
		
		if(empty($activity)){
			showMessage(L('nc_weixin_wall_activity_is_not_exists'),'','error');
		}
		Tpl::output('activity',$activity);//活动
		
		$account = $model->table('wxaccount')->where(array('wx_id'=>$activity['activity_wx_id']))->find();
		Tpl::output('account',$account);//账号
		
		$message = $model->table('message')->where(array('activity_id'=>intval($_GET['activity_id']),'state'=>2))->order('message_time desc')->limit(0,3)->select();//2.上墙显示
		
		if(!empty($message)){
			foreach($message as $key=>$val){
				$fans = $model->table('fans')->where(array('fans_openid'=>$val['user_openid']))->find();
				$message[$key]['fans_nickname'] = $fans['fans_nickname'];
				$message[$key]['fans_headimgurl'] = $fans['fans_pic'];
			}
		}
		Tpl::output('message',$message);//消息
		Tpl::showpage('message','null_layout');
	}
	
	/*
	 * 投票墙
	 */
	public function voteOp(){
		$model = Model();
		$vote = $model->table('vote')->where(array('vote_id'=>intval($_GET['vote_id'])))->find();
		if(empty($vote)){
			showMessage(L('nc_weixin_wall_vote_activity_is_not_exists'),'','error');
		}
		Tpl::output('vote',$vote);
		
		$account = $model->table('wxaccount')->where(array('wx_id'=>$vote['vote_wx_id']))->find();
		Tpl::output('account',$account);//账号
		
		$vote_item = $model->table('vote_item')->where(array('vote_id'=>intval($_GET['vote_id'])))->select();

		$array	= array();//投票数组
		$count  = 0;//投票总数
		if(!empty($vote_item)){
			foreach($vote_item as $key=>$val){
				$array[$key]['id']	  = 's'.$val['item_id'];
				$array[$key]['title'] = $val['item_name'].'.'.$val['item_content'];
				$array[$key]['num']	  = $val['item_num'];
				$array[$key]['color'] = $val['item_color'];
				$count+=$val['item_num'];
			}
		}else{
			showMessage(L('nc_weixin_wall_activity_vote_item_is_null'),'','error');
		}

		Tpl::output('count',$count);
		Tpl::output('vote_item',json_encode($array));
		Tpl::showpage('vote','null_layout');
	}
	
	/*
	 * 抽奖墙
	 */
	public function lotteryOp(){
		$model = Model();
		$activity = $model->table('activity')->where(array('activity_id'=>intval($_GET['activity_id'])))->find();
		
		if(empty($activity)){//活动不存在
			showMessage(L('nc_weixin_wall_activity_is_not_exists'),'','error');
		}
		Tpl::output('activity',$activity);
		
		$member = $model->table('message')->field('*,count(`user_openid`) as count')->where(array('activity_id'=>intval($_GET['activity_id'])))->group('user_openid')->limit("0,6")->select();
		Tpl::output('member',$member);//抽奖会员
		
		$member_count = $model->table('message')->where(array('activity_id'=>intval($_GET['activity_id'])))->group('user_openid')->select();
		Tpl::output('member_count',count($member_count));//人数
		
		$lottery = $model->table('lottery,fans')->field('lottery.user_name as user_name,fans.fans_pic as fans_pic')->join('left')->on('lottery.user_id=fans.fans_openid')->where(array('lottery.activity_id'=>intval($_GET['activity_id'])))->order("lottery_time desc")->limit("0,6")->select();	
		Tpl::output('lottery',$lottery);//获奖信息
		
		$lottery_count = $model->table('lottery,fans')->join('left')->on('lottery.user_id=fans.fans_openid')->where(array('lottery.activity_id'=>intval($_GET['activity_id'])))->count();	
		Tpl::output('lottery_count',$lottery_count);//中奖人数
			
		$account = $model->table('wxaccount')->where(array('wx_id'=>$activity['activity_wx_id']))->find();
		Tpl::output('account',$account);//账号
		Tpl::showpage('lottery','null_layout');
	}
	
	/*
	 * 抽奖
	 */	
	public function lotteryAddOp(){
		if($_SESSION['is_login'] != 1){
			exit;
		}
		
		if(isset($_POST) && !empty($_POST)){
			$model = Model();
			$activity = $model->table('activity')->where(array('activity_id'=>intval($_POST['activity_id'])))->find();
			
			if(empty($activity)){//活动不存在
				exit;
			}
			Tpl::output('activity',$activity);
			
			$member = $model->table('message')->where(array('activity_id'=>intval($_POST['activity_id'])))->group('user_openid')->select();
			$key = array_rand($member,1);
			$member = $member[$key];

			$params					= array();
			$params['activity_id']	= $activity['activity_id'];
			$params['activity_name']= $activity['activity_name'];
			$params['user_id']		= $member['user_openid'];
			$params['user_name']	= $member['user_name'];
			$params['lottery_time']	= time();
			$params['wx_id']		= $activity['activity_wx_id'];
			
			$res = $model->table('lottery')->insert($params);
			if($res){//公布中奖信息
				Tpl::output('prize',$member);
				$account = $model->table('wxaccount')->where(array('wx_id'=>$vote['vote_wx_id']))->find();
				Tpl::output('account',$account);//账号
				
				Tpl::showpage('lottery.prize','null_layout');
			}			
		}
		exit;
	}
	
	
	/*
	 * 排行榜
	 */
	public function rankOp(){
		$model = Model();
		$activity = $model->table('activity')->where(array('activity_id'=>intval($_GET['activity_id'])))->find();
		
		if(empty($activity)){//活动不存在
			exit;
		}
		Tpl::output('activity',$activity);
		
		$member = $model->table('message')->field('*,count(`user_openid`) as count')->where(array('activity_id'=>intval($_GET['activity_id'])))->group('user_openid')->order('count desc')->limit(0,10)->select();
		if(!empty($member)){
			foreach($member as $key=>$val){
				$fans = $model->table('fans')->where(array('fans_openid'=>$val['user_openid']))->find();
				$member[$key]['fans_headimgurl'] = $fans['fans_pic'];
			}
		}
		Tpl::output('member',$member);
		
		$account = $model->table('wxaccount')->where(array('wx_id'=>$activity['activity_wx_id']))->find();
		Tpl::output('account',$account);//账号
		
		Tpl::showpage('rank','null_layout');
	}
	
}