<?php
/**
 * 默认展示页面
 * @copyright  Copyright (c) 2007-2013 ShopWWI Inc. (http://www.shopwwi1.com)
 * @license    http://www.shopwwi2.com
 * @link       http://www.shopwwi.com
 * @since      File available since Release v1.1
 */
defined('InByShopWWI') or exit('Access Invalid!');
define('MYSQL_RESULT_TYPE',1);
class siteControl extends BaseHomeControl{
	
	public function __construct(){
		parent::__construct();
	}
	
	/*
	 * 编辑站点信息
	 */
	public function indexOp(){
		$this->editSiteOp();
	}
	
	
	/*
	 * 编辑站点信息
	 */
	public function editSiteOp(){
		
		if(isset($_POST) && !empty($_POST)){			
			//表单验证
			$obj_validate = new Validate();
			$obj_validate->validateparam	=	array(
					array("input"=>trim($_POST['site_name']),"require"=>"true","message"=>'站点名称不能为空'),
					array("input"=>trim($_POST['seo_keyword']),"require"=>"true","message"=>'站点关键字不能为空'),
					array("input"=>trim($_POST['seo_keyword']),"require"=>"true","message"=>'商城网址不能为空')
			);
				
			$error = $obj_validate->validate();
			if ($error != ''){
				showMessage(Language::get('error').$error,'','error');
			}
			
			$params = array();
			$params['site_name'] 	= trim($_POST['site_name']);
			$params['seo_keyword']	= trim($_POST['seo_keyword']);
			$params['mall_url']		= trim($_POST['mall_url']);
			$params['seo_content']	= $_POST['seo_content'];
			$params['wx_id']		= intval($_GET['wx_id']);
			
			$condition = array();
			$condition['site_id'] = intval($_POST['site_id']);
			
			$model = Model();
			$res = $model->table('site')->where($condition)->update($params);
			if($res){
				showMessage('编辑站点成功','?act=site&wx_id='.intval($_GET['wx_id']),'succ');
			}else{
				showMessage('编辑站点失败','?act=site&wx_id='.intval($_GET['wx_id']),'error');
			}
		}
		
		$model = Model();
		$site = $model->table('site')->where(array('wx_id'=>intval($_GET['wx_id'])))->find();
		Tpl::output('site',$site);
		Tpl::output('sign','site');
		Tpl::showpage('site');
		
	}
	
	/*
	 * 测试连接
	 */
	public function testOp(){
		$url = trim($_GET['url'],'/').'/api/weixin/index.php?weixin=shopwwi';
		$result = @file_get_contents($url);
		$result = intval($result);
		if($result){
			echo json_encode(array('result'=>$result,'msg'=>'连接成功'));
			exit;		
		}else{
			echo json_encode(array('result'=>$result,'msg'=>'连接失败'));
			exit;
		}
	}
}