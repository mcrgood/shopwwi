<?php
/**
 * 粉丝行为管理
 * */

defined('InByShopWWI') or exit('Access Invalid!');
class fans_activityControl extends BaseHomeControl{
	public function __construct(){
		parent::__construct();
		$this->wx_id = intval($_REQUEST['wx_id']);
		if($this->wx_id <= 0){
			showMessage('参数错误','','error');
		}
	}

	public function indexOp(){
		$model = Model();
		$time = time()-7*3600*24;
		$sql = "select act_content,count(*) as num from ".C('tablepre')."fans_activity where act_time>".$time." group by act_content";
		$activity = $model->query($sql);
		Tpl::output('list',$activity);
	    Tpl::showpage('activity_action');
	}

	/**
	 * 行为趋势对比
	 * */
	public function trendOp(){
		$model = Model();
		$time = time()-7*3600*24;
		$time2 = time()-14*3600*24;
		$sql = "select act_content,count(*) as num from ".C('tablepre')."fans_activity where act_time>".$time." group by act_content";
		$sql2 = "select act_content,count(*) as num from ".C('tablepre')."fans_activity where act_time<".$time." and act_time>".$time2." group by act_content";
		$activity = $model->query($sql);
		$activity2 = $model->query($sql2);
		$return = array();
		foreach((array)$activity as $k=>$v){
		    $return[] = $v;
		    $return[]['num2'] = $activity2[$k]['num'];
		}
		Tpl::output('list',$return);
		Tpl::showpage('activity_trend');
	}


}