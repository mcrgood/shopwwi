<?php
/**
 * 消息管理
 * @copyright  Copyright (c) 2007-2013 ShopWWI Inc. (http://www.shopwwi1.com)
 * @license    http://www.shopwwi2.com
 * @link       http://www.shopwwi.com
 * @since      File available since Release v1.1
 */
defined('InByShopWWI') or exit('Access Invalid!');
class msgControl extends BaseHomeControl{
	public function __construct(){
		parent::__construct();
		Language::read('msg');
	}
	/**
	 * 消息管理
	 */
	public function msg_manageOp(){
		$model = Model();
		$wx_id = intval($_GET['wx_id']);
		$where_condition = array('wx_id'=>$wx_id);
		if(trim($_GET['s_content']) != ''){
			$where_condition['message_content'] = array('like','%'.trim($_GET['s_content']).'%');
		}
		if(trim($_GET['s_nickname']) != ''){
			$where_condition['user_name'] = array('like','%'.trim($_GET['s_nickname']).'%');
		}
		if(intval($_GET['s_important']) == 1){
			$where_condition['is_important'] = 1;
		}
		$msg_list = $model->table('message')->where($where_condition)->page(15)->order('message_time desc')->select();
		tpl::output('msg_list',$msg_list);
		Tpl::output('show_page',$model->showpage());
		Tpl::showpage('msg_manage');
	}
	/**
	 * 发送消息
	 */
	public function send_msgOp(){
		$model = Model();
		$wx_id = intval($_GET['wx_id']);
		//开始向粉丝发送信息
		if(chksubmit()){
			//调取公众账号的接口信息
			$wx_info = $model->field('wx_appid,wx_appsecret,wx_accesstoken')->table('wxaccount')->where(array('wx_id'=>$wx_id))->find();
			$access_token = $wx_info['wx_accesstoken'];
			if($access_token == ''){
				$access_token = $this->get_accesstoken($wx_info['wx_appid'], $wx_info['wx_appsecret'], $wx_id);
			}
			$data = '{
					    "touser":"'.trim($_GET['openid']).'",
					    "msgtype":"text",
					    "text":
					    {
					         "content":"'.trim($_GET['msg']).'"
					    }
					}';
			 $ch = curl_init();  
	         curl_setopt($ch, CURLOPT_URL, "https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token=".$access_token);  
	         curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");  
	         curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);  
	         curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);  
	         curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; MSIE 5.01; Windows NT 5.0)');  
	         curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);  
	         curl_setopt($ch, CURLOPT_AUTOREFERER, 1);  
	         curl_setopt($ch, CURLOPT_POSTFIELDS, $data);  
	         curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);  
	         $tmpInfo = curl_exec($ch);  
	         if (curl_errno($ch)) {  
	          echo json_encode(array('done'=>false,'msg'=>L('msg_request_failed')));die;
	         }
	         curl_close($ch);
	         $tmpInfo = json_decode($tmpInfo,true);  
			if($tmpInfo['errcode'] == '40001' || $tmpInfo['errcode'] == '42001' || $tmpInfo['errcode'] == '42002' || $tmpInfo['errcode'] == '42003'){
				$access_token = $this->get_accesstoken($wx_info['wx_appid'], $wx_info['wx_appsecret'], $wx_id);
				 $ch = curl_init();  
		         curl_setopt($ch, CURLOPT_URL, "https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token=".$access_token);  
		         curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");  
		         curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);  
		         curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);  
		         curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; MSIE 5.01; Windows NT 5.0)');  
		         curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);  
		         curl_setopt($ch, CURLOPT_AUTOREFERER, 1);  
		         curl_setopt($ch, CURLOPT_POSTFIELDS, $data);  
		         curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);  
		         $tmpInfo = curl_exec($ch);  
		         if (curl_errno($ch)) {  
		          echo json_encode(array('done'=>false,'msg'=>L('msg_request_failed')));die;
		         }
		         curl_close($ch);
		         $tmpInfo = json_decode($tmpInfo,true);
			}
			if($tmpInfo['errcode']==0){ 
				echo json_encode(array('done'=>true));die;
			}else{
				echo json_encode(array('done'=>false,'msg'=>$tmpInfo['errmsg']));die;
			}
		}
		//调取粉丝信息
		$fans_openid = trim($_GET['fans_openid']);
		$fans_info = $model->table('fans')->where(array('fans_openid'=>$fans_openid))->find();
		Tpl::output('fans_info',$fans_info);
		Tpl::showpage('msg_send_msg');
	}
	/**
	 * 重要消息设置
	 */
	public function set_importantOp(){
		$model = Model();
		$wx_id = intval($_GET['wx_id']);
		$message_id = intval($_GET['message_id']);
		$important_status = intval($_GET['important_status']);
		if($message_id <= 0 || $important_status < 0){
			showMessage(L('wrong_argument'),'','error');
		}
		$rs = $model->table('message')->where(array('message_id'=>$message_id))->update(array('is_important'=>($important_status==1?0:1)));
		if($rs){
			showMessage(L('nc_common_op_succ'),'index.php?act=msg&op=msg_manage&wx_id='.$wx_id,'succ');
		}else{
			showMessage(L('nc_common_op_fail'),'','error');
		}
	}
}