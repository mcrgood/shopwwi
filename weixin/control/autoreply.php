<?php
/**
 * 自动回复管理
 * @copyright  Copyright (c) 2007-2013 ShopWWI Inc. (http://www.shopwwi1.com)
 * @license    http://www.shopwwi2.com
 * @link       http://www.shopwwi.com
 * @since      File available since Release v1.1
 */
defined('InByShopWWI') or exit('Access Invalid!');
class autoreplyControl extends BaseHomeControl{
	public function __construct(){
		parent::__construct();
		Language::read('autoreply');
	}
	/**
	 * 关注自动回复
	 */
	public function sub_replyOp(){
		$model = Model();
		$wx_id = intval($_GET['wx_id']);
		if(chksubmit()){
			if(intval($_POST['reply_id']) == 0){
				showMessage(L('ar_pls_choose_reply'),'','error');
			}
			if(trim($_POST['exc_type']) == 'add'){
				$insert_array = array();
				$insert_array['wx_id'] = $wx_id;
				$insert_array['reply_id'] = intval($_POST['reply_id']);
				$insert_array['rs_type'] = 1;
				$insert_array['rs_addtime'] = time();
				$insert_array['rs_addstaff'] = $_SESSION['admin_id'];
				$insert_array['rs_use'] = intval($_POST['rs_status']);
				$rs = $model->table('reply_setting')->insert($insert_array);
			}else{
				$update_array = array();
				$update_array['reply_id'] = intval($_POST['reply_id']);
				$update_array['rs_use'] = intval($_POST['rs_status']);
				$rs = $model->table('reply_setting')->where(array('rs_id'=>intval($_POST['rs_id'])))->update($update_array);
			}
			if($rs){
				showMessage(L('ar_subreply_set_succ'),'index.php?act=autoreply&op=sub_reply&wx_id='.$wx_id,'succ');
			}else{
				showMessage(L('ar_subreply_set_failed'),'','error');
			}
		}
		//调取回复素材列表
		$reply_list = $model->field('reply_id,reply_title')->table('reply')->where(array('wx_id'=>$wx_id))->select();
		Tpl::output('reply_list',$reply_list);
		//调取自动回复设置信息
		$rs_info = $model->table('reply_setting')->where(array('wx_id'=>$wx_id,'rs_type'=>1))->find();
		Tpl::output('rs_info',$rs_info);
		Tpl::showpage('sub_reply');
	}
	/**
	 * 发消息自动回复
	 */
	public function msg_replyOp(){
		$model = Model();
		$wx_id = intval($_GET['wx_id']);
		if(chksubmit()){
			if(intval($_POST['reply_id']) == 0){
				showMessage(L('ar_pls_choose_reply'),'','error');
			}
			if(trim($_POST['exc_type']) == 'add'){
				$insert_array = array();
				$insert_array['wx_id'] = $wx_id;
				$insert_array['reply_id'] = intval($_POST['reply_id']);
				$insert_array['rs_type'] = 2;
				$insert_array['rs_addtime'] = time();
				$insert_array['rs_addstaff'] = $_SESSION['admin_id'];
				$insert_array['rs_use'] = intval($_POST['rs_status']);
				$rs = $model->table('reply_setting')->insert($insert_array);
			}else{
				$update_array = array();
				$update_array['reply_id'] = intval($_POST['reply_id']);
				$update_array['rs_use'] = intval($_POST['rs_status']);
				$rs = $model->table('reply_setting')->where(array('rs_id'=>intval($_POST['rs_id'])))->update($update_array);
			}
			if($rs){
				showMessage(L('ar_autoreply_set_succ'),'index.php?act=autoreply&op=msg_reply&wx_id='.$wx_id,'succ');
			}else{
				showMessage(L('ar_autoreply_set_failed'),'','error');
			}
		}
		//调取回复素材列表
		$reply_list = $model->field('reply_id,reply_title')->table('reply')->where(array('wx_id'=>$wx_id))->select();
		Tpl::output('reply_list',$reply_list);
		//调取自动回复设置信息
		$rs_info = $model->table('reply_setting')->where(array('wx_id'=>$wx_id,'rs_type'=>2))->find();
		Tpl::output('rs_info',$rs_info);
		Tpl::showpage('msg_reply');
	}
	/**
	 * 关键词自动回复
	 */
	public function keyword_replyOp(){
		$model = Model();
		$wx_id = intval($_GET['wx_id']);
		//调取关键词回复规则列表
		$where_condition = array('wx_id'=>$wx_id);
		if(trim($_GET['s_title']) != ''){
			$where_condition['kr_title'] = array('like','%'.trim($_GET['s_title']).'%');
		}
		$kr_list = $model->table('keyword_reply')->where($where_condition)->page(15)->order('kr_addtime desc')->select();
		Tpl::output('kr_list',$kr_list);
		Tpl::output('show_page',$model->showpage());
		Tpl::showpage('keyword_reply');
	}
	/**
	 * 添加关键字自动回复
	 */
	public function keyword_reply_addOp(){
		$model = Model();
		$wx_id = intval($_GET['wx_id']);
		if(chksubmit()){
			//表单验证
			$obj_validate = new Validate();
			$obj_validate->validateparam = array(
				array("input"=>trim($_POST['kr_title']),"require"=>"true","message"=>L('ar_subject_must_write')),
				array("input"=>trim($_POST['kr_keyword']),"require"=>"true","message"=>L('ar_keyword_must_write'))
			);
			$error = $obj_validate->validate();
			if ($error != ''){
				showMessage($error,'','error');
			}
			if(intval($_POST['reply_id']) == 0){
				showMessage(L('ar_pls_choose_reply'),'','error');
			}
			$insert_array = array();
			$insert_array['kr_title'] = trim(trim($_POST['kr_title']));
			$insert_array['reply_id'] = intval($_POST['reply_id']);
			$insert_array['wx_id'] = $wx_id;
			$insert_array['kr_keyword'] = ','.trim($_POST['kr_keyword']).',';
			$insert_array['kr_addtime'] = time();
			$insert_array['kr_addstaff'] = $_SESSION['admin_id'];
			$insert_array['kr_use'] = intval($_POST['kr_status']);
			$rs = $model->table('keyword_reply')->insert($insert_array);
			if($rs){
				showMessage(L('ar_add_kwreply_succ'),'index.php?act=autoreply&op=keyword_reply&wx_id='.$wx_id,'succ');
			}else{
				showMessage(L('ar_add_kwreply_failed'),'','error');
			}
		}
		//调取回复素材列表
		$reply_list = $model->field('reply_id,reply_title')->table('reply')->where(array('wx_id'=>$wx_id))->select();
		Tpl::output('reply_list',$reply_list);
		Tpl::showpage('keyword_reply_add');
	}
	/**
	 * 编辑关键字自动回复
	 */
	public function keyword_reply_editOp(){
		$model = Model();
		$wx_id = intval($_GET['wx_id']);
		if(chksubmit()){
			//表单验证
			$obj_validate = new Validate();
			$obj_validate->validateparam = array(
				array("input"=>trim($_POST['kr_title']),"require"=>"true","message"=>L('ar_subject_must_write')),
				array("input"=>trim($_POST['kr_keyword']),"require"=>"true","message"=>L('ar_keyword_must_write'))
			);
			$error = $obj_validate->validate();
			if ($error != ''){
				showMessage($error,'','error');
			}
			if(intval($_POST['reply_id']) == 0){
				showMessage(L('ar_pls_choose_reply'),'','error');
			}
			$update_array = array();
			$update_array['kr_title'] = trim(trim($_POST['kr_title']));
			$update_array['reply_id'] = intval($_POST['reply_id']);
			$update_array['kr_keyword'] = ','.trim($_POST['kr_keyword']).',';
			$update_array['kr_use'] = intval($_POST['kr_status']);
			$rs = $model->table('keyword_reply')->where(array('kr_id'=>intval($_POST['kr_id'])))->update($update_array);
			if($rs){
				showMessage(L('ar_edit_kwreply_succ'),'index.php?act=autoreply&op=keyword_reply&wx_id='.$wx_id,'succ');
			}else{
				showMessage(L('ar_edit_kwreply_failed'),'','error');
			}
		}
		//调取关键字自动回复信息
		$kr_id = intval($_GET['kr_id']);
		$kr_info = $model->table('keyword_reply')->where(array('kr_id'=>$kr_id))->find();
		Tpl::output('kr_info',$kr_info);
		//调取回复素材列表
		$reply_list = $model->field('reply_id,reply_title')->table('reply')->where(array('wx_id'=>$wx_id))->select();
		Tpl::output('reply_list',$reply_list);
		Tpl::showpage('keyword_reply_edit');
	}
	/**
	 * 删除关键字自动回复
	 */
	public function keyword_reply_delOp(){
		$model = Model();
		$wx_id = intval($_GET['wx_id']);
		$kr_id = intval($_GET['kr_id']);
		$rs = $model->table('keyword_reply')->where(array('kr_id'=>$kr_id))->delete();
		if($rs){
			showMessage(L('ar_del_kwreply_succ'),'index.php?act=autoreply&op=keyword_reply&wx_id='.$wx_id,'succ');
		}else{
			showMessage(L('ar_del_kwreply_failed'),'','error');
		}
	}
	/**
	 * 回复素材管理
	 */
	public function reply_manageOp(){
		$model = Model();
		$wx_id = intval($_GET['wx_id']);
		$where_condition = array('wx_id'=>$wx_id);
		if (trim($_GET['s_title']) != '') {
			$where_condition['reply_title'] = array('like','%'.trim($_GET['s_title']).'%');
		}
		if (intval($_GET['s_type']) != 0) {
			$where_condition['reply_type'] = intval($_GET['s_type']);
		}
		$cr_list = $model->table('reply')->where($where_condition)->page(15)->order('reply_addtime desc')->select();
		Tpl::output('reply_list',$cr_list);
		Tpl::output('show_page',$model->showpage());
		Tpl::showpage('reply_manage');
	}
	/**
	 * 添加素材
	 */
	public function reply_addOp(){
		$model = Model();
		$wx_id = intval($_GET['wx_id']);
		if(chksubmit()){
			//表单验证
			$obj_validate = new Validate();
			$obj_validate->validateparam = array(
				array("input"=>trim($_POST['reply_title']),"require"=>"true","message"=>L('ar_subject_must_write')),
				array("input"=>trim($_POST['reply_type']),"require"=>"true","message"=>L('ar_type_must_choose'))
			);
			$error = $obj_validate->validate();
			if ($error != ''){
				showMessage($error,'','error');
			}
			$insert_array = array();
			$insert_array['reply_title'] = trim($_POST['reply_title']);
			$insert_array['reply_note'] = trim($_POST['reply_note']);
			$insert_array['reply_type'] = intval($_POST['reply_type']);
			$insert_array['reply_addtime'] = time();
			$insert_array['reply_addstaff'] = $_SESSION['admin_id'];
			$insert_array['wx_id'] = $wx_id;
			//处理回复内容主体信息
			switch (intval($_POST['reply_type'])){
				case 1://纯文字
					$insert_array['reply_content'] = trim($_POST['reply_content']);
					break;
				case 2://图文
					$textimg_content = array();
					foreach ($_POST['textimg_reply_title'] as $k=>$v){
						if($v != ''){
							$file_name = '';
							if($_FILES['textimg_reply_file']['name'][$k] != ''){
								$name_type=substr($_FILES['textimg_reply_file']['name'][$k],-4,4);
								$file_name = md5(uniqid(rand(),true)).$name_type;
								move_uploaded_file($_FILES['textimg_reply_file']['tmp_name'][$k],BASE_UPLOAD_PATH.'/reply/'.$file_name);
							}
							$textimg_content[] = array(
								'title'=>$_POST['textimg_reply_title'][$k],
								'desc'=>$_POST['textimg_reply_desc'][$k],
								'picurl'=>$file_name!=''?UPLOAD_SITE_URL.'/reply/'.$file_name:'',
								'url'=>$_POST['textimg_reply_url'][$k]
							);
						}
					}
					$insert_array['reply_content'] = serialize($textimg_content);
					break;
				case 3://语音
					$voice_info = array(
						'title'=>trim($_POST['voice_title']),
						'desc'=>trim($_POST['voice_desc'])
					);
					//普通音质
					if (trim($_POST['voice_type']) == "url") {
						$voice_info['url'] = trim($_POST['voice_url']);
					}
					if (trim($_POST['voice_type']) == "upload") {
						$file_name = '';
						if($_FILES['voice_upload_file']['name'] != ''){
							$name_type=substr($_FILES['voice_upload_file']['name'],-4,4);
							$file_name = md5(uniqid(rand(),true)).$name_type;
							move_uploaded_file($_FILES['voice_upload_file']['tmp_name'][$k],BASE_UPLOAD_PATH.'/reply/'.$file_name);
						}
						$voice_info['url'] = $file_name!=''?UPLOAD_SITE_URL.'/reply/'.$file_name:'';
						$media_insert = array(
							'media_name'=>$_FILES['voice_upload_file']['name'],
							'media_url'=>$voice_info['url'],
							'addtime'=>time(),
							'addstaff'=>$_SESSION['admin_id'],
							'wx_id'=>$wx_id,
							'media_type'=>1
						);
						$model->table('media')->insert($media_insert);
					}
					if (trim($_POST['voice_type']) == "choose") {
						$voice_info['url'] = $_POST['choose_voice'];
					}
					//高音质
					if (trim($_POST['hq_voice_type']) == "url") {
						$voice_info['hqurl'] = trim($_POST['hq_voice_url']);
					}
					if (trim($_POST['hq_voice_type']) == "upload") {
						$file_name = '';
						if($_FILES['hq_voice_upload_file']['name'] != ''){
							$name_type=substr($_FILES['hq_voice_upload_file']['name'],-4,4);
							$file_name = md5(uniqid(rand(),true)).$name_type;
							move_uploaded_file($_FILES['hq_voice_upload_file']['tmp_name'][$k],BASE_UPLOAD_PATH.'/reply/'.$file_name);
						}
						$voice_info['hqurl'] = $file_name!=''?UPLOAD_SITE_URL.'/reply/'.$file_name:'';
						$media_insert = array(
								'media_name'=>$_FILES['hq_voice_upload_file']['name'],
								'media_url'=>$voice_info['hqurl'],
								'addtime'=>time(),
								'addstaff'=>$_SESSION['admin_id'],
								'wx_id'=>$wx_id,
								'media_type'=>1
						);
						$model->table('media')->insert($media_insert);
					}
					if (trim($_POST['hq_voice_type']) == "choose") {
						$voice_info['hqurl'] = $_POST['choose_hq_voice'];
					}
					$insert_array['reply_content'] = serialize($voice_info);
					break;
			}
			$rs = $model->table('reply')->insert($insert_array);
			if($rs){
				showMessage(L('ar_add_reply_succ'),'index.php?act=autoreply&op=reply_manage&wx_id='.$wx_id,'succ');
			}else{
				showMessage(L('ar_add_reply_failed'),'','error');
			}
		}
		//调取语音素材
		$voice_list = $model->table('media')->where(array('wx_id'=>$wx_id,'media_type'=>1))->select();
		Tpl::output('voice_list', $voice_list);
		Tpl::showpage('reply_add');
	}
	/**
	 * 编辑素材
	 */
	public function reply_editOp(){
		$model = Model();
		$wx_id = intval($_GET['wx_id']);
		if(chksubmit()){
			//表单验证
			$obj_validate = new Validate();
			$obj_validate->validateparam = array(
				array("input"=>trim($_POST['reply_title']),"require"=>"true","message"=>L('ar_subject_must_write')),
				array("input"=>trim($_POST['reply_type']),"require"=>"true","message"=>L('ar_type_must_choose'))
			);
			$error = $obj_validate->validate();
			if ($error != ''){
				showMessage($error,'','error');
			}
			$update_array = array();
			$update_array['reply_title'] = trim($_POST['reply_title']);
			$update_array['reply_note'] = trim($_POST['reply_note']);
			$update_array['reply_type'] = intval($_POST['reply_type']);
			//处理回复内容主体信息
			switch (intval($_POST['reply_type'])){
				case 1://纯文字
					$update_array['reply_content'] = trim($_POST['reply_content']);
					break;
				case 2://图文
					$textimg_content = array();
					foreach ($_POST['textimg_reply_title'] as $k=>$v){
						if($v != ''){
							$file_name = '';
							if($_FILES['textimg_reply_file']['name'][$k] != ''){
								$name_type=substr($_FILES['textimg_reply_file']['name'][$k],-4,4);
								$file_name = md5(uniqid(rand(),true)).$name_type;
								move_uploaded_file($_FILES['textimg_reply_file']['tmp_name'][$k],BASE_UPLOAD_PATH.'/reply/'.$file_name);
							}
							$textimg_content[] = array(
								'title'=>$_POST['textimg_reply_title'][$k],
								'desc'=>$_POST['textimg_reply_desc'][$k],
								'picurl'=>$file_name!=''?UPLOAD_SITE_URL.'/reply/'.$file_name:trim($_POST['ori_reply_pic'][$k]),
								'url'=>$_POST['textimg_reply_url'][$k]
							);
						}
					}
					$update_array['reply_content'] = serialize($textimg_content);
					break;
				case 3://语音
					$voice_info = array(
					'title'=>trim($_POST['voice_title']),
					'desc'=>trim($_POST['voice_desc'])
					);
					//普通音质
					if (trim($_POST['voice_type']) == "url") {
						$voice_info['url'] = trim($_POST['voice_url']);
					}
					if (trim($_POST['voice_type']) == "upload") {
						$file_name = '';
						if($_FILES['voice_upload_file']['name'] != ''){
							$name_type=substr($_FILES['voice_upload_file']['name'],-4,4);
							$file_name = md5(uniqid(rand(),true)).$name_type;
							move_uploaded_file($_FILES['voice_upload_file']['tmp_name'][$k],BASE_UPLOAD_PATH.'/reply/'.$file_name);
						}
						$voice_info['url'] = $file_name!=''?UPLOAD_SITE_URL.'/reply/'.$file_name:'';
						$media_insert = array(
								'media_name'=>$_FILES['voice_upload_file']['name'],
								'media_url'=>$voice_info['url'],
								'addtime'=>time(),
								'addstaff'=>$_SESSION['admin_id'],
								'wx_id'=>$wx_id,
								'media_type'=>1
						);
						$model->table('media')->insert($media_insert);
					}
					if (trim($_POST['voice_type']) == "choose") {
						$voice_info['url'] = $_POST['choose_voice'];
					}
					//高音质
					if (trim($_POST['hq_voice_type']) == "url") {
						$voice_info['hqurl'] = trim($_POST['hq_voice_url']);
					}
					if (trim($_POST['hq_voice_type']) == "upload") {
						$file_name = '';
						if($_FILES['hq_voice_upload_file']['name'] != ''){
							$name_type=substr($_FILES['hq_voice_upload_file']['name'],-4,4);
							$file_name = md5(uniqid(rand(),true)).$name_type;
							move_uploaded_file($_FILES['hq_voice_upload_file']['tmp_name'][$k],BASE_UPLOAD_PATH.'/reply/'.$file_name);
						}
						$voice_info['hqurl'] = $file_name!=''?UPLOAD_SITE_URL.'/reply/'.$file_name:'';
						$media_insert = array(
								'media_name'=>$_FILES['hq_voice_upload_file']['name'],
								'media_url'=>$voice_info['hqurl'],
								'addtime'=>time(),
								'addstaff'=>$_SESSION['admin_id'],
								'wx_id'=>$wx_id,
								'media_type'=>1
						);
						$model->table('media')->insert($media_insert);
					}
					if (trim($_POST['hq_voice_type']) == "choose") {
						$voice_info['hqurl'] = $_POST['choose_hq_voice'];
					}
					$update_array['reply_content'] = serialize($voice_info);
					break;
			}
			$rs = $model->table('reply')->where(array('reply_id'=>intval($_POST['reply_id'])))->update($update_array);
			if($rs){
				showMessage(L('ar_edit_reply_succ'),'index.php?act=autoreply&op=reply_manage&wx_id='.$wx_id,'succ');
			}else{
				showMessage(L('ar_edit_reply_failed'),'','error');
			}
		}
		//调取回复信息
		$reply_id = intval($_GET['reply_id']);
		$reply_info = $model->table('reply')->where(array('reply_id'=>$reply_id))->find();
		Tpl::output('reply_info',$reply_info);
		//调取语音素材
		$voice_list = $model->table('media')->where(array('wx_id'=>$wx_id,'media_type'=>1))->select();
		Tpl::output('voice_list', $voice_list);
		Tpl::showpage('reply_edit');
	}
	/**
	 * 删除回复素材
	 */
	public function reply_delOp(){
		$model = Model();
		$wx_id = intval($_GET['wx_id']);
		$reply_id = intval($_GET['reply_id']);
		$rs = $model->table('reply')->where(array('reply_id'=>$reply_id))->delete();
		if($rs){
			if (intval($_GET['fromgc']) == 1) {
				showMessage(L('ar_del_reply_succ'),'index.php?act=store&op=gc_manage&reply_type=click_reply&wx_id='.$wx_id,'succ');
			} elseif (intval($_GET['fromg']) == 1) {
				showMessage(L('ar_del_reply_succ'),'index.php?act=store&op=goods_manage&reply_type=click_reply&wx_id='.$wx_id,'succ');
			} else {
				showMessage(L('ar_del_reply_succ'),'index.php?act=autoreply&op=reply_manage&wx_id='.$wx_id,'succ');
			}
		}else{
			showMessage(L('ar_del_reply_failed'),'','error');
		}
	}
	
	/**
	 * ajax获取回复素材
	 */
	public function ajax_get_replyOp(){
		$model = Model();
		$wx_id = intval($_GET['wx_id']);
		if ($wx_id == 0) {
			echo json_encode(array('done'=>false,'msg'=>'参数异常'));die;
		}
		$reply_type = intval($_GET['reply_type']);
		if ($reply_type != 0) {
			$where = array('wx_id'=>$wx_id,'reply_type'=>$reply_type);
		} else {
			$where = array('wx_id'=>$wx_id);
		}
		//调取回复素材列表
		$reply_list = $model->field('reply_id,reply_title')->table('reply')->where($where)->select();
		if (!empty($reply_list)) {
			echo json_encode(array('done'=>true,'reply_list'=>$reply_list));die;
		} else {
			echo json_encode(array('done'=>false,'msg'=>'未找到相关回复素材'));die;
		}
	}
}