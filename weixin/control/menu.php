<?php
/**
 * 自定义菜单管理
 * @copyright  Copyright (c) 2007-2013 ShopWWI Inc. (http://www.shopwwi1.com)
 * @license    http://www.shopwwi2.com
 * @link       http://www.shopwwi.com
 * @since      File available since Release v1.1
 */
defined('InByShopWWI') or exit('Access Invalid!');
class menuControl extends BaseHomeControl{
	public function __construct(){
		parent::__construct();
		Language::read('menu');
	}
	/**
	 * 自定义界面管理
	 */
	public function menu_manageOp(){
		$model = Model();
		$wx_id = intval($_GET['wx_id']);
		//调取公众账号的接口信息
		$wx_info = $model->field('wx_appid,wx_appsecret,wx_accesstoken,shop_data_type,store_id')->table('wxaccount')->where(array('wx_id'=>$wx_id))->find();
		$access_token = $wx_info['wx_accesstoken'];
		if($access_token == ''){
			$access_token = $this->get_accesstoken($wx_info['wx_appid'], $wx_info['wx_appsecret'], $wx_id);
		}
		//发布自定义新菜单
		if(chksubmit()){
			$data = '{"button":[';
			if(!empty($_POST['main_menu_title'])){
				foreach ($_POST['main_menu_title'] as $k=>$v){
					if($v != ''){
						$data .= '{"name":"'.$v.'",';
						switch ($_POST['main_menu_type'][$k]){
							case '':
								$data .= '"sub_button":[';
								if($k==0){ $a=0;$b=5; }
								if($k==1){ $a=5;$b=10; }
								if($k==2){ $a=10;$b=15; }
								for ($i=$a;$i<$b;$i++){
									if($_POST['child_menu_title'][$i] != ''){
										$data .= '{"name":"'.$_POST['child_menu_title'][$i].'",';
										switch ($_POST['child_menu_type'][$i]){
											case 'click':
												$data .= '"type":"click","key":"'.$_POST['child_menu_click_reply'][$i].'"},';
												break;
											case 'view':
												$data .= '"type":"view","url":"'.$_POST['child_menu_url'][$i].'"},';
												break;
										}
									}
								}
								$data = trim($data,',');
								$data .= ']},';
								break;
							case 'click':
								$data .= '"type":"click","key":"'.$_POST['main_menu_click_reply'][$k].'"},';
								break;
							case 'view':
								$data .= '"type":"view","url":"'.$_POST['main_menu_url'][$k].'"},';
								break;
							case 'menu_recgc':
								$data .= '"sub_button":[';
								$choose_gc = unserialize($_POST['choose_gc']);
								if (!empty($choose_gc)) {
									foreach ($choose_gc as $cgcv) {
										$data .= '{"name":"'.$cgcv['gc_name'].'",';
										$data .= '"type":"view","url":"'.$cgcv['url'].'"},';
									}
								}
								$data = trim($data,',');
								$data .= ']},';
								break;
							case 'home_page':
								$home_url = $wx_info['shop_data_type']==2?WAP_SITE_URL."tmpl/store.html?store_id=".$wx_info['store_id']:WAP_SITE_URL;
								$data .= '"type":"view","url":"'.$home_url.'"},';
								break;
						}
					}
				}
				$data = trim($data,',');
				$data .= ']},';
				//exit($data);
				$ch = curl_init();  
		         curl_setopt($ch, CURLOPT_URL, "https://api.weixin.qq.com/cgi-bin/menu/create?access_token=".$access_token);  
		         curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");  
		         curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);  
		         curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);  
		         curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; MSIE 5.01; Windows NT 5.0)');  
		         curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);  
		         curl_setopt($ch, CURLOPT_AUTOREFERER, 1);  
		         curl_setopt($ch, CURLOPT_POSTFIELDS, $data);  
		         curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);  
		         $tmpInfo = curl_exec($ch);  
		         if (curl_errno($ch)) {  
		          showMessage(L('menu_far_request_failed'),'','error');
		         }
		         curl_close($ch);
		         $tmpInfo = json_decode($tmpInfo,true);  
				if($tmpInfo['errcode'] == '40001' || $tmpInfo['errcode'] == '42001' || $tmpInfo['errcode'] == '42002' || $tmpInfo['errcode'] == '42003'){
					$access_token = $this->get_accesstoken($wx_info['wx_appid'], $wx_info['wx_appsecret'], $wx_id);
					 $ch = curl_init();  
			         curl_setopt($ch, CURLOPT_URL, "https://api.weixin.qq.com/cgi-bin/menu/create?access_token=".$access_token);  
			         curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");  
			         curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);  
			         curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);  
			         curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; MSIE 5.01; Windows NT 5.0)');  
			         curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);  
			         curl_setopt($ch, CURLOPT_AUTOREFERER, 1);  
			         curl_setopt($ch, CURLOPT_POSTFIELDS, $data);  
			         curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);  
			         $tmpInfo = curl_exec($ch);  
			         if (curl_errno($ch)) {  
			          showMessage(L('menu_far_request_failed'),'','error');
			         }
			         curl_close($ch);
			         $tmpInfo = json_decode($tmpInfo,true);
				}
				if($tmpInfo['errcode']==0){ 
					showMessage(L('menu_publish_succ'),'index.php?act=menu&op=menu_manage&wx_id='.$wx_id,'succ');
				}else{
					showMessage(L('menu_publish_succ').'('.$tmpInfo['errmsg'].')','','error');
				}
			}
		}
		//获取自定义菜单信息
		$url = 'https://api.weixin.qq.com/cgi-bin/menu/get?access_token='.$access_token;
		$res = @file_get_contents($url);
		$res = json_decode($res,true);
		if($res['errcode'] != '' && ($res['errcode'] == '40001' || $res['errcode'] == '42001' || $res['errcode'] == '42002' || $res['errcode'] == '42003')){
			$access_token = $this->get_accesstoken($wx_info['wx_appid'], $wx_info['wx_appsecret'], $wx_id);
			$res = @file_get_contents($url);
			$res = json_decode($res,true);
		}
		if($res['errcode'] != '' && $res['errcode'] != '46003'){
			showMessage(L('menu_cannot_get_menu_info').'('.$res['errcode'].')','','error');
		}else{
			Tpl::output('menu_list',$res['menu']['button']);
		}
		//调取点击事件回复列表
		$cr_list = $model->field('cr_title,cr_code')->table('click_reply')->where(array('wx_id'=>$wx_id))->select();
		Tpl::output('cr_list',$cr_list);
		//调取分类推荐菜单
		$mr_info = $model->table('menu_recgc')->where(array('wx_id'=>$wx_id))->find();
		Tpl::output('choose_gc', $mr_info['mr_content']);
		Tpl::showpage('menu_manage');
	}
	/**
	 * 删除自定义菜单
	 */
	public function menu_delOp(){
		$model = Model();
		$wx_id = intval($_GET['wx_id']);
		//调取公众账号的接口信息
		$wx_info = $model->field('wx_appid,wx_appsecret,wx_accesstoken')->table('wxaccount')->where(array('wx_id'=>$wx_id))->find();
		$access_token = $wx_info['wx_accesstoken'];
		if($access_token == ''){
			$access_token = $this->get_accesstoken($wx_info['wx_appid'], $wx_info['wx_appsecret'], $wx_id);
		}
		$url = 'https://api.weixin.qq.com/cgi-bin/menu/delete?access_token='.$access_token;
		$res = @file_get_contents($url);
		$res = json_decode($res,true);
		if($res['errcode'] == '40001' || $res['errcode'] == '42001' || $res['errcode'] == '42002' || $res['errcode'] == '42003'){
			$access_token = $this->get_accesstoken($wx_info['wx_appid'], $wx_info['wx_appsecret'], $wx_id);
			$url = 'https://api.weixin.qq.com/cgi-bin/menu/delete?access_token='.$access_token;
			$res = @file_get_contents($url);
			$res = json_decode($res,true);
		}
		if($res['errcode']==0){ 
			echo json_encode(array('done'=>true));
		}else{
			echo json_encode(array('done'=>false,'msg'=>$res['errmsg']));
		}
	}
	/**
	 * 点击事件回复管理
	 */
	public function click_reply_manageOp(){
		$model = Model();
		$wx_id = intval($_GET['wx_id']);
		$where_condition = array('wx_id'=>$wx_id);
		if(trim($_GET['s_title']) != ''){
			$where_condition['cr_title'] = array('like','%'.trim($_GET['s_title']).'%');
		}
		if (intval($_GET['s_type']) != 0) {
			$where_condition['cr_type'] = intval($_GET['s_type']);
		}
		$cr_list = $model->table('click_reply')->where($where_condition)->page(15)->order('cr_addtime desc')->select();
		Tpl::output('cr_list',$cr_list);
		Tpl::output('show_page',$model->showpage());
		Tpl::showpage('click_reply_manage');
	}
	/**
	 * 新增点击事件回复
	 */
	public function click_reply_addOp(){
		$model = Model();
		$wx_id = intval($_GET['wx_id']);
		if(chksubmit()){
			//表单验证
			$obj_validate = new Validate();
			$obj_validate->validateparam = array(
				array("input"=>trim($_POST['cr_title']),"require"=>"true","message"=>L('menu_subject_must_write')),
				array("input"=>trim($_POST['cr_type']),"require"=>"true","message"=>L('menu_type_must_choose')),
				array("input"=>trim($_POST['cr_code']),"require"=>"true","message"=>L('menu_code_must_write'))
			);
			$error = $obj_validate->validate();
			if ($error != ''){
				showMessage($error,'','error');
			}
			$insert_array = array();
			$insert_array['cr_title'] = trim($_POST['cr_title']);
			$insert_array['cr_note'] = trim($_POST['cr_note']);
			$insert_array['cr_type'] = intval($_POST['cr_type']);
			$insert_array['cr_addtime'] = time();
			$insert_array['cr_addstaff'] = $_SESSION['admin_id'];
			$insert_array['wx_id'] = $wx_id;
			$insert_array['cr_code'] = trim($_POST['cr_code']);
			//处理回复内容主体信息
			switch (intval($_POST['cr_type'])){
				case 1://纯文字
					$insert_array['cr_content'] = trim($_POST['reply_content']);
					break;
				case 2://图文
					$textimg_content = array();
					foreach ($_POST['reply_title'] as $k=>$v){
						if($v != ''){
							$file_name = '';
							if($_FILES['reply_file']['name'][$k] != ''){
								$name_type=substr($_FILES['reply_file']['name'][$k],-4,4);
								$file_name = md5(uniqid(rand(),true)).$name_type;
								move_uploaded_file($_FILES['reply_file']['tmp_name'][$k],BASE_UPLOAD_PATH.'/reply/'.$file_name);
							}
							$textimg_content[] = array(
								'title'=>$_POST['reply_title'][$k],
								'desc'=>$_POST['reply_desc'][$k],
								'picurl'=>$file_name!=''?UPLOAD_SITE_URL.'/reply/'.$file_name:'',
								'url'=>$_POST['reply_url'][$k]
							);
						}
					}
					$insert_array['cr_content'] = serialize($textimg_content);
					break;
			}
			$rs = $model->table('click_reply')->insert($insert_array);
			if($rs){
				showMessage(L('menu_click_reply_add_succ'),'index.php?act=menu&op=click_reply_manage&wx_id='.$wx_id,'succ');
			}else{
				showMessage(L('menu_click_reply_add_failed'),'','error');
			}
		}
		Tpl::showpage('click_reply_add');
	}
	/**
	 * 编辑点击事件回复
	 */
	public function click_reply_editOp(){
		$model = Model();
		$wx_id = intval($_GET['wx_id']);
		
		if(chksubmit()){
			//表单验证
			$obj_validate = new Validate();
			$obj_validate->validateparam = array(
				array("input"=>trim($_POST['cr_title']),"require"=>"true","message"=>L('menu_subject_must_write')),
				array("input"=>trim($_POST['cr_type']),"require"=>"true","message"=>L('menu_type_must_choose')),
				array("input"=>trim($_POST['cr_code']),"require"=>"true","message"=>L('menu_code_must_write'))
			);
			$error = $obj_validate->validate();
			if ($error != ''){
				showMessage($error,'','error');
			}
			$update_array = array();
			$update_array['cr_title'] = trim($_POST['cr_title']);
			$update_array['cr_note'] = trim($_POST['cr_note']);
			$update_array['cr_type'] = intval($_POST['cr_type']);
			$update_array['cr_code'] = trim($_POST['cr_code']);
			//处理回复内容主体信息
			switch (intval($_POST['cr_type'])){
				case 1://纯文字
					$update_array['cr_content'] = trim($_POST['reply_content']);
					break;
				case 2://图文
					$textimg_content = array();
					foreach ($_POST['reply_title'] as $k=>$v){
						if($v != ''){
							$file_name = '';
							if($_FILES['reply_file']['name'][$k] != ''){
								$name_type=substr($_FILES['reply_file']['name'][$k],-4,4);
								$file_name = md5(uniqid(rand(),true)).$name_type;
								move_uploaded_file($_FILES['reply_file']['tmp_name'][$k],BASE_UPLOAD_PATH.'/reply/'.$file_name);
							}
							$textimg_content[] = array(
								'title'=>$_POST['reply_title'][$k],
								'desc'=>$_POST['reply_desc'][$k],
								'picurl'=>$file_name!=''?UPLOAD_SITE_URL.'/reply/'.$file_name:trim($_POST['ori_reply_pic'][$k]),
								'url'=>$_POST['reply_url'][$k]
							);
						}
					}
					$update_array['cr_content'] = serialize($textimg_content);
					break;
			}
			$rs = $model->table('click_reply')->where(array('cr_id'=>intval($_POST['cr_id'])))->update($update_array);
			if($rs){
				showMessage(L('menu_click_reply_edit_succ'),'index.php?act=menu&op=click_reply_manage&wx_id='.$wx_id,'succ');
			}else{
				showMessage(L('menu_click_reply_edit_failed'),'','error');
			}
		}
		//调取回复信息
		$cr_id = intval($_GET['cr_id']);
		$cr_info = $model->table('click_reply')->where(array('cr_id'=>$cr_id))->find();
		Tpl::output('cr_info',$cr_info);
		Tpl::showpage('click_reply_edit');
	}
	/**
	 * 删除点击事件回复
	 */
	public function click_reply_delOp(){
		$model = Model();
		$wx_id = intval($_GET['wx_id']);
		$cr_id = intval($_GET['cr_id']);
		$rs = $model->table('click_reply')->where(array('cr_id'=>$cr_id))->delete();
		if($rs){
			if ($_GET['fromgc'] == 1) {
				showMessage(L('menu_click_reply_del_succ'),'index.php?act=store&op=gc_manage&reply_type=click_reply&wx_id='.$wx_id,'succ');
			} elseif (intval($_GET['fromg']) == 1) {
				showMessage(L('ar_del_reply_succ'),'index.php?act=store&op=goods_manage&reply_type=click_reply&wx_id='.$wx_id,'succ');
			} else {
				showMessage(L('menu_click_reply_del_succ'),'index.php?act=menu&op=click_reply_manage&wx_id='.$wx_id,'succ');
			}
		}else{
			showMessage(L('menu_click_reply_del_failed'),'','error');
		}
	}
}