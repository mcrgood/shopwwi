<?php
/**
 * 默认展示页面
 * @copyright  Copyright (c) 2007-2013 ShopWWI Inc. (http://www.shopwwi1.com)
 * @license    http://www.shopwwi2.com
 * @link       http://www.shopwwi.com
 * @since      File available since Release v1.1
 */
defined('InByShopWWI') or exit('Access Invalid!');
define('MYSQL_RESULT_TYPE',1);
class activityControl extends BaseHomeControl{
	
	public function __construct(){
		parent::__construct();
		Language::read('weixin_wall');
	}

	public function indexOp(){
		$this->listOp();
	}
	
	/*
	 * 活动列表
	 */
	public function listOp(){		
		$model = Model();
		$list = $model->table('activity')->order('activity_time desc')->page(12)->select();
		
		Tpl::output('list',$list);
		Tpl::output('show_page',$model->showpage());
		
		Tpl::output('sign','activity');
		Tpl::showpage('activity.list');
	}
	
	
	/*
	 * 添加活动
	 */
	public function addActivityOp(){
		if(isset($_POST) && !empty($_POST)){
			//表单验证
			$obj_validate = new Validate();
			$obj_validate->validateparam	=	array(
				array("input"=>trim($_POST['activity_name']),"require"=>"true","message"=>L('nc_weixin_wall_activity_name_is_not_null'))
			);
			
			$error = $obj_validate->validate();
			if ($error != ''){
				showMessage(Language::get('error').$error,'','error');
			}

			$params			=	array();
			$params['activity_name']	= trim($_POST['activity_name']);
			$params['activity_content']	= $_POST['activity_content'];
			$params['activity_time']	= time();
			$params['activity_wx_id']	= intval($_GET['wx_id']);
			$model = Model();
			$res = $model->table('activity')->insert($params);//添加微信活动
			
			if($res){//添加活动成功
				showMessage(L('nc_weixin_wall_add_activity_succ'),'?act=activity&op=list&wx_id='.intval($_GET['wx_id']),'succ');
			}else{//添加活动失败
				showMessage(L('nc_weixin_wall_add_activity_fail'),'?act=activity&op=list&wx_id='.intval($_GET['wx_id']),'error');
			}
		}
		Tpl::output('sign','activity');
		Tpl::showpage('activity.add');
	}

	/*
	 * 编辑活动
	 */
	public function editActivityOp(){
		if(isset($_POST) && !empty($_POST)){
			//表单验证
			$obj_validate = new Validate();
			$obj_validate->validateparam	=	array(
				array("input"=>trim($_POST['activity_name']),"require"=>"true","message"=>L('nc_weixin_wall_activity_name_is_not_null'))
			);
			
			$error = $obj_validate->validate();
			if ($error != ''){
				showMessage(Language::get('error').$error,'','error');
			}	

			$params			=	array();
			$params['activity_name']	= trim($_POST['activity_name']);
			$params['activity_content']	= $_POST['activity_content'];
			$params['activity_time']	= time();
			$params['activity_wx_id']	= intval($_GET['wx_id']);
			
			$condition	=	array('activity_id'=>intval($_POST['activity_id']));
			$model = Model();
			$res = $model->table('activity')->where($condition)->update($params);//编辑活动
			if($res){
				showMessage(L('nc_weixin_wall_edit_activity_succ'),'?act=activity&op=list&wx_id='.intval($_GET['wx_id']),'succ');
			}else{
				showMessage(L('nc_weixin_wall_edit_activity_fail'),'?act=activity&op=list&wx_id='.intval($_GET['wx_id']),'error');
			}
		}
		
		$model = Model();
		$activity = $model->table('activity')->where(array('activity_id'=>intval($_GET['activity_id'])))->find();
		
		if(empty($activity)){//活动不存在
			showMessage(L('nc_weixin_wall_activity_is_not_exists'),'','succ');
		}

		Tpl::output('activity',$activity);
		Tpl::output('sign','activity');
		Tpl::showpage('activity.edit');
	}

	/*
	 * 删除活动
	 */
	public function delActivityOp(){
		$model = Model();
		$activity = $model->table('activity')->where(array('activity_id'=>intval($_GET['activity_id'])))->find();
		
		if(empty($activity)){//活动不存在
			showMessage(L('nc_weixin_wall_activity_is_not_exists'),'','error');
		}
		
		$res = $model->table('activity')->where(array('activity_id'=>intval($_GET['activity_id'])))->delete();
		if($res){//删除活动
			showMessage(L('nc_weixin_wall_delete_activity_succ'),'','succ');
		}else{
			showMessage(L('nc_weixin_wall_delete_activity_fail'),'','error');
		}

	}


	/*
	 * 消息列表
	 */	
	public function messageOp(){
		$model = Model();
		$activity = $model->table('activity')->where(array('activity_id'=>intval($_GET['activity_id'])))->page(12)->find();
		if(empty($activity)){//检测活动是否存在
			showMessage(L('nc_weixin_wall_activity_is_not_exists'),'','error');
		}
		
		$message = $model->table('message')->where(array('activity_id'=>intval($_GET['activity_id'])))->order('message_time desc')->select();
		Tpl::output('message',$message);
		Tpl::output('show_page',$model->showpage());
		Tpl::output('sign','activity');
		Tpl::showpage('messages.list');
	}
	
	/*
	 * 消息审核上墙
	 */
	public function messageAuditOp(){		
		if(isset($_POST) && !empty($_POST)){//审核消息
			$params			=	array();
			$params['state']	= trim($_POST['state']);
			
			$condition	=	array();
			$condition['message_id']	= trim($_POST['message_id']);

			$model = Model();
			$res = $model->table('message')->where($condition)->update($params);
			if($res){
				showMessage(L('nc_weixin_wall_message_audit_succ'),'?act=activity&op=message&wx_id='.intval($_GET['wx_id']).'&activity_id='.intval($_POST['activity_id']),'succ');
			}else{
				showMessage(L('nc_weixin_wall_message_audit_fail'),'?act=activity&op=message&wx_id='.intval($_GET['wx_id']).'&activity_id='.intval($_POST['activity_id']),'error');
			}
		}
		
		$model = Model();
		$message = $model->table('message')->where(array('message_id'=>intval($_GET['message_id'])))->find();
		if(empty($message)){
			showMessage(L('nc_weixin_wall_activity_is_not_exists'),'','error');
		}
		Tpl::output('message',$message);
		Tpl::output('sign','activity');
		Tpl::showpage('message.audit');
	}

	/*
	 * 批量审核
	 */
	public function batchauditOp(){
		if(isset($_POST) && !empty($_POST)){

			$model = Model();
			$res = $model->table('message')->where(array('state'=>array('in',$_POST['message_id'])))->update(array('state'=>intval($_GET['type'])));
			
			if($res){
				showMessage(L('nc_weixin_wall_message_audit_succ'),'','succ');
			}else{
				showMessage(L('nc_weixin_wall_message_audit_fail'),'','error');
			}
			
		}else{
			showMessage(L('nc_weixin_wall_message_audit_fail'),'','error');
		}
	}
	

	/*
	 * 抽奖显示
	 */	
	public function lotterylistOp(){
		$model = Model();
		$lottery = $model->table('lottery')->where(array('wx_id'=>intval($_GET['wx_id']),'activity_id'=>intval($_GET['activity_id'])))->order('lottery_time desc')->page(12)->select();
		
		Tpl::output('lottery',$lottery);
		Tpl::output('show_page',$model->showpage());
		Tpl::output('sign','activity');
		Tpl::showpage('lottery.list');
	}
	

	/*
	 * 投票列表
	 */
	public function votelistOp(){
		$model = Model();
		$vote = $model->table('vote')->where(array('vote_wx_id'=>intval($_GET['wx_id'])))->order('vote_add_time desc')->page(12)->select();
		
		Tpl::output('vote',$vote);
		Tpl::output('show_page',$model->showpage());
		Tpl::output('sign','vote');
		Tpl::showpage('vote.list');
	}

	/*
	 * 添加投票
	 */
	public function addVoteOp(){
		if(isset($_POST) && !empty($_POST)){
			//表单验证
			$obj_validate = new Validate();
			$obj_validate->validateparam	=	array(
				array("input"=>trim($_POST['vote_name']),"require"=>"true","message"=>L('nc_weixin_wall_vote_name_is_not_null'))
			);
			
			$error = $obj_validate->validate();
			if ($error != ''){
				showMessage(Language::get('error').$error,'','error');
			}
			
			$params					= array();//参数
			$params['vote_name']	= trim($_POST['vote_name']);
			$params['vote_content']	= $_POST['vote_content'];	
			$params['vote_wx_id']	= intval($_GET['wx_id']);
			$params['vote_add_time']= time();
			
			$model = Model();
			$res = $model->table('vote')->insert($params);
			if($res){
				showMessage(L('nc_weixin_wall_vote_publish_succ'),'?act=activity&op=votelist&wx_id='.intval($_GET['wx_id']),'succ');
			}else{
				showMessage(L('nc_weixin_wall_vote_publish_fail'),'?act=activity&op=votelist&wx_id='.intval($_GET['wx_id']),'error');
			}
		}
		Tpl::output('sign','vote');
		Tpl::showpage('vote.add');
	}	

	/*
	 * 编辑投票
	*/
	public function editVoteOp(){	
		if(isset($_POST) && !empty($_POST)){
			//表单验证
			$obj_validate = new Validate();
			$obj_validate->validateparam	=	array(
				array("input"=>trim($_POST['vote_name']),"require"=>"true","message"=>L('nc_weixin_wall_vote_name_is_not_null'))
			);
			
			$error = $obj_validate->validate();
			if ($error != ''){
				showMessage(Language::get('error').$error,'','error');
			}		
						
			$params					= array();//参数
			$params['vote_name']	= trim($_POST['vote_name']);
			$params['vote_content'] = $_POST['vote_content'];
			$params['vote_wx_id']	= intval($_GET['wx_id']);
			$params['vote_add_time']= time();
			
			$condition		= array();//条件
			$condition['vote_id']	= intval($_POST['vote_id']);
			
			$model = Model();//实例化模型
			$res = $model->table('vote')->where($condition)->update($params);
			if($res){
				showMessage(L('nc_weixin_wall_vote_edit_succ'),'?act=activity&op=votelist&wx_id='.intval($_GET['wx_id']),'succ');
			}else{
				showMessage(L('nc_weixin_wall_vote_edit_fail'),'?act=activity&op=votelist&wx_id='.intval($_GET['wx_id']),'error');
			}
		}
		
		$model = Model();		
		$vote = $model->table('vote')->where(array('vote_id'=>intval($_GET['vote_id'])))->find();
		if(empty($vote)){
			showMessage(L('nc_weixin_wall_vote_activity_is_not_exists'),'','error');
		}
		Tpl::output('vote',$vote);
		Tpl::output('sign','vote');
		Tpl::showpage('vote.edit');
	}	
	
	/*
	 * 删除投票
	 */	
	public function deleteVoteOp(){
		$model = Model();
		$res   = $model->table('vote')->where(array('vote_id'=>intval($_GET['vote_id'])))->delete();
		
		if($res){
			showMessage(L('nc_weixin_wall_vote_delete_succ'),'?act=activity&op=votelist&wx_id='.intval($_GET['wx_id']),'succ');
		}else{
			showMessage(L('nc_weixin_wall_vote_delete_fail'),'?act=activity&op=votelist&wx_id='.intval($_GET['wx_id']),'fail');
		}
	}
	
	/*
	 * 投票选项
	 */
	public function voteItemOp(){
		$model = Model();
		$item = $model->table('vote_item')->where(array('vote_id'=>intval($_GET['vote_id'])))->page(12)->select();
		Tpl::output('item',$item);
		Tpl::output('sign','vote');
		Tpl::showpage('vote.item');
	}
	
	/*
	 * 增加选项
	 */
	public function addVoteItemOp(){
		if(isset($_POST) && !empty($_POST)){
			//表单验证
			$obj_validate = new Validate();
			$obj_validate->validateparam	=	array(
					array("input"=>trim($_POST['item_name']),"require"=>"true","message"=>L('nc_weixin_wall_vote_name_is_not_null'))
			);
				
			$error = $obj_validate->validate();
			if ($error != ''){
				showMessage(Language::get('error').$error,'','error');
			}
			
			$params			=	array();
			$params['vote_id']	= intval($_GET['vote_id']);
			$params['item_name']= trim($_POST['item_name']);
			$params['item_color']	= trim($_POST['item_color']);
			$params['item_content'] = $_POST['item_content'];
			
			$model = Model();
			$res = $model->table('vote_item')->insert($params);
			if($res){
				showMessage(L('nc_weixin_wall_vote_item_add_succ'),'?act=activity&op=voteItem&wx_id='.intval($_GET['wx_id']).'&vote_id='.intval($_GET['vote_id']),'succ');
			}else{
				showMessage(L('nc_weixin_wall_vote_item_add_fail'),'?act=activity&op=voteItem&wx_id='.intval($_GET['wx_id']).'&vote_id='.intval($_GET['vote_id']),'fail');
			}
		}
		Tpl::output('sign','vote');
		Tpl::showpage('vote.item.add');
	}
	
	/*
	 * 编辑选项
	 */	
	public function editVoteItemOp(){
		if(isset($_POST) && !empty($_POST)){
			//表单验证
			$obj_validate = new Validate();
			$obj_validate->validateparam	=	array(
					array("input"=>trim($_POST['item_name']),"require"=>"true","message"=>L('nc_weixin_wall_vote_name_is_not_null'))
			);
				
			$error = $obj_validate->validate();
			if ($error != ''){
				showMessage(Language::get('error').$error,'','error');
			}
			
			$params			=	array();
			$params['item_name']=	trim($_POST['item_name']);
			$params['item_color']	= trim($_POST['item_color']);
			$params['item_content'] = $_POST['item_content'];

			$condition		= array();
			$condition['item_id']	= intval($_POST['item_id']);
			
			$model = Model();
			$res = $model->table('vote_item')->where($condition)->update($params);
			if($res){
				showMessage(L('nc_weixin_wall_vote_item_edit_succ'),'?act=activity&op=voteItem&wx_id='.intval($_GET['wx_id']).'&vote_id='.intval($_GET['vote_id']),'succ');
			}else{
				showMessage(L('nc_weixin_wall_vote_item_edit_fail'),'?act=activity&op=voteItem&wx_id='.intval($_GET['wx_id']).'&vote_id='.intval($_GET['vote_id']),'fail');
			}
		}
		$model = Model();
		$item = $model->table('vote_item')->where(array('item_id'=>intval($_GET['item_id'])))->find();
		Tpl::output('item',$item);
		Tpl::output('sign','vote');
		Tpl::showpage('vote.item.edit');
	}
	
	/*
	 * 删除选项
	 */
	public function deleteVoteItemOp(){
		$model = Model();
		$res = $model->table('vote_item')->where(array('item_id'=>intval($_GET['item_id'])))->delete();
		if($res){
			showMessage(L('nc_weixin_wall_vote_item_delete_succ'),'?act=activity&op=voteItem&wx_id='.intval($_GET['wx_id']).'&vote_id='.intval($_GET['vote_id']),'succ');
		}else{
			showMessage(L('nc_weixin_wall_vote_item_delete_fail'),'?act=activity&op=voteItem&wx_id='.intval($_GET['wx_id']).'&vote_id='.intval($_GET['vote_id']),'fail');
		}
	}
}