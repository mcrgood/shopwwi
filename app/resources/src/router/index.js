import VueRouter from 'vue-router';

import WebRouter from '../modules/web/router';
import MobileRouter from '../modules/mobile/router';

export default new VueRouter({
  mode: 'history',
  routes: [WebRouter, MobileRouter]
});