import Vuex from 'vuex';
import isEmpty from 'lodash/isEmpty';
import { http, setKey, getKey } from '../utils';
import web from '../modules/web/store';
import mobile from '../modules/mobile/store';

const store = new Vuex.Store({
  state: {
    name: '优居完整家居',
    key: getKey(),
    // 站点设置
    site: {},
    // 当前用户
    user: {}
  },
  getters: {  },
  mutations: {
    setKey (state, key) {
      state.key = key;
      setKey(key);
    },

    /**
     * 更新用户信息
     * @param state
     * @param user
     */
    setUser (state, user) {
      state.user = user;
    },

  },
  actions: {
    // 加载用户数据
    loadUser ({ state, commit }) {
      if (state.key) {
        http
          .post('?app=member_index', {a: 1, b: 2})
          .then((response) => {
            commit('setUser', response.data.datas.member_info);
          });
      }
    },
    // 提交登录参数
    login ({ dispatch, state, commit }, data) {
      if (isEmpty(state.user)) {
        http
          .post('?app=login', data)
          .then((response) => {
            commit('setKey', response.data.datas.key);
            dispatch('loadUser');
          });
      }
    }
  },
  modules: {
    web,
    mobile
  }
});

export default store;
