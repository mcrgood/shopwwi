import Vue from 'vue';
import Vuex from 'vuex';
import VueRouter from 'vue-router';
import axios from 'axios';
import extend from 'lodash/extend';

window.G = extend({
  basePath: 'http://yjs5.com',
}, window.G || {});
window.G.utils = require('./utils');
window.Vue = Vue;
window.axios = axios;

Vue.use(Vuex);
Vue.use(VueRouter);
