$(function (){
    template.helper('isEmpty', function(o) {
        for (var i in o) {
            return false;
        }
        return true;
    });
    template.helper('decodeURIComponent', function(o){
        return decodeURIComponent(o);
    });
    var key = getCookie('key');
    if(!key){
        var rData = {check_out:false};
        rData.WapSiteUrl = WapSiteUrl;
        var html = template.render('zhucai-order', rData);
        $('#zhucai-order').addClass('no-login');
        $("#zhucai-order-wp").html(html);
        $('.goto-settlement,.goto-mallping').parent().hide();

    }else{
        //初始化页面数据
        function initCartList(){
             $.ajax({
                url:ApiUrl+"/index.php?app=member_cart&wwi=cart_list",
                type:"post",
                dataType:"json",
                data:{key:key},
                success:function (result){
                    if(checkLogin(result.login)){
                        if(!result.datas.error){
                            var rData = result.datas;
                            rData.WapSiteUrl = WapSiteUrl;
                            rData.check_out = true;
                            var html = template.render('zhucai-order', rData);
                            $("#zhucai-order-wp").html(html);
                        }else{
                           alert(result.datas.error);
                        }
                    }
                }
            });
        }
        initCartList();
    }
    $('#add_address_form').find('.btn').click(function(){
            var param = {};
            param.name = $('#vtrue_name').val();
            param.phone = $('#vmob_phone').val();
            param.address = $('#vaddress').val();
            param.form_submit = $('#form_submit').val();
            param.key=key;
            $.ajax({
                type:'post',
                url:ApiUrl+"/index.php?app=zhucai&wwi=order&inajax=1",
                data:param,
                dataType:'json',
                success:function(result){
                    if (result.code == '200') {
                        //$('#new-address-wrapper,#list-address-wrapper').find('.header-l > a').click();
                        if (result.datas.payment_code == 'online') {
                            toPay(result.datas.pay_sn,'member_buy','pay');
                        }
                    }else{
                        if(result.url){
                            $.sDialog({skin:"red",
                                content: result.datas.error,
                                okFn: function() {location.href = WapSiteUrl+result.url;}
                            });
                        }else{
                            $.sDialog({content:result.datas.error,okBtn:false,cancelBtn:false});
                        }
                    }
                }
            });
    });    
});