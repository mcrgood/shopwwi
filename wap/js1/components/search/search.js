define('wap.components.search', ['zepto', 'template', 'wap.common'], function($, template) {
    var init = false;
    var myDate = new Date();
    var default_data = {
        curpage: 1,
        hasmore: true,
        footer: false,
        keyword: decodeURIComponent(getQueryString('keyword')),
        gc_id: getQueryString('gc_id'),
        b_id: getQueryString('b_id'),
        key: getQueryString('key'),
        order: getQueryString('order'),
        area_id: getQueryString('area_id'),
        price_from: getQueryString('price_from'),
        price_to: getQueryString('price_to'),
        own_mall: getQueryString('own_mall'),
        gift: getQueryString('gift'),
        groupbuy: getQueryString('groupbuy'),
        xianshi: getQueryString('xianshi'),
        virtual: getQueryString('virtual'),
        ci: getQueryString('ci'),
        myDate: myDate,
        searchTimes: myDate.getTime(),

        callabck: undefined
    };

    function search_adv(config) {
        $.getJSON(ApiUrl + '/index.php?app=index&wwi=search_adv', function (result) {
            var data = result.datas;
            $('#list-items-scroll').html(template.render('search_items', data));
            if (config.area_id) {
                $('#area_id').val(config.area_id);
            }
            if (price_from) {
                $('#price_from').val(config.price_from);
            }
            if (config.price_to) {
                $('#price_to').val(config.price_to);
            }
            if (config.own_mall) {
                $('#own_mall').addClass('current');
            }
            // if (btn-2) {
            //     $('#btn-2').addClass('current');
            // }
            if (config.gift) {
                $('#gift').addClass('current');
            }
            if (config.groupbuy) {
                $('#groupbuy').addClass('current');
            }
            if (config.xianshi) {
                $('#xianshi').addClass('current');
            }
            if (config.virtual) {
                $('#virtual').addClass('current');
            }
            if (config.ci) {
                var ci_arr = config.ci.split('_');
                for (var i in ci_arr) {
                    $('a[name="ci"]').each(function () {
                        if ($(this).attr('value') == ci_arr[i]) {
                            $(this).addClass('current');
                        }
                    });
                }
            }
            $('#search_submit').click(function () {
                var queryString = '?keyword=' + config.keyword, ci = '';
                queryString += '&area_id=' + $('#area_id').val();
                if ($('#price_from').val() != '') {
                    queryString += '&price_from=' + $('#price_from').val();
                }
                if ($('#price_to').val() != '') {
                    queryString += '&price_to=' + $('#price_to').val();
                }
                if ($('#own_mall')[0].className == 'current') {
                    queryString += '&own_mall=1';
                }
                if ($('#gift')[0].className == 'current') {
                    queryString += '&gift=1';
                }
                if ($('#groupbuy')[0].className == 'current') {
                    queryString += '&groupbuy=1';
                }
                if ($('#xianshi')[0].className == 'current') {
                    queryString += '&xianshi=1';
                }
                if ($('#virtual')[0].className == 'current') {
                    queryString += '&virtual=1';
                }
                $('a[name="ci"]').each(function () {
                    if ($(this)[0].className == 'current') {
                        ci += $(this).attr('value') + '_';
                    }
                });
                if (ci != '') {
                    queryString += '&ci=' + ci;
                }
                if (config.callback) {
                    config.callback(queryString);
                } else {
                    window.location.href = WapSiteUrl + '/tmpl/product_list.html' + queryString;
                }
            });
            $('a[nctype="items"]').click(function () {
                var myDate = new Date();
                if (myDate.getTime() - config.searchTimes > 300) {
                    $(this).toggleClass('current');
                    default_data.searchTimes = myDate.getTime();
                }
            });
            $('input[nctype="price"]').on('blur', function () {
                if ($(this).val() != '' && !/^-?(?:\d+|\d{1,3}(?:,\d{3})+)?(?:\.\d+)?$/.test($(this).val())) {
                    $(this).val('');
                }
            });
            $('#reset1').click(function () {
                $('a[nctype="items"]').removeClass('current');
                $('input[nctype="price"]').val('');
                $('#area_id').val('');
            });
        });
    }

    function Search(config) {
        config = $.extend(true, {}, default_data, config);
        if (!init) {
            require(['text!../../wap/js/components/search/search.html'], function(html) {
                var $body = $('body');
                $body.append(html);
                init = true;
                var _config = {
                    valve : '#search_adv',
                    wrapper : '.nctouch-full-mask',
                    scroll : '#list-items-scroll'
                };
                $.animationLeft(_config);
                var cb = config.callback;
                config.callback = function(query) {
                    cb(query.substr(1));
                    $(_config.wrapper).find('.header-l > a').trigger('click');
                }
                search_adv(config);
            });
        } else {
            search_adv(config);
        }
    }
    return Search;
});