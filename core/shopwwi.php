<?php
/**
 * 运行框架
 *
 *
 *
 * @copyright  Copyright (c) 2007-2013 ShopWWI Inc. (http://www.shopwwi1.com)
 * @license    http://www.shopwwi2.com/
 * @link       http://www.shopwwi3.com/
 * @author	   ShopWWI Team
 * @since      File available since Release v1.1
 */

defined('InByShopWWI') or exit('Access Invalid!');
if (!@include(BASE_DATA_PATH.'/config/config.ini.php')) exit('config.ini.php isn\'t exists!');

//考虑是否需要判断admin的配置文件
if (defined('BRANCH_NAME') && BRANCH_NAME != ''){
	if (file_exists(BASE_PATH.DS.BRANCH_NAME.'/config/config.ini.php')){
		include(BASE_PATH.DS.BRANCH_NAME.'/config/config.ini.php');
	}
}else {
	if (file_exists(BASE_PATH.'/config/config.ini.php')){
		include(BASE_PATH.'/config/config.ini.php');
	}	
}
global $config;
/*$sitepath = strtolower(substr($_SERVER['PHP_SELF'], 0, strrpos($_SERVER['PHP_SELF'], '/')));
$tmp_array = explode('/',$sitepath); 
$lastdir = end($tmp_array);
if (in_array($lastdir,array(DIR_CMS,DIR_SHOP,DIR_MICROSHOP,DIR_CIRCLE,DIR_ADMIN,DIR_API))){
	unset($tmp_array[count($tmp_array)-1]);
}
$auto_site_url = strtolower('http://'.$_SERVER['HTTP_HOST'].implode('/',$tmp_array));*/

$auto_site_url = strtolower('http://'.$_SERVER['HTTP_HOST']);
define('SHOP_SITE_URL', !empty($config['shop_site_url']) ? rtrim($config['shop_site_url'],'/') : $auto_site_url.'/'.DIR_SHOP);
define('WAP_SITE_URL', $config['wap_site_url']);
define('MALL_API_URL', $config['mall_api_url']);
define('TPL_NAME',!empty($config['tpl_name'])?rtrim($config['tpl_name']):'default');

define('UPLOAD_SITE_URL',!empty($config['upload_site_url']) ? rtrim($config['upload_site_url'],'/') : SHOP_SITE_URL.'/'.DIR_UPLOAD);
define('RESOURCE_SITE_URL',(!empty($config['resource_site_url']) ? rtrim($config['resource_site_url'],'/') : SHOP_SITE_URL.'/'.DIR_RESOURCE));

define('BASE_DATA_PATH',BASE_ROOT_PATH.'/data');
define('BASE_UPLOAD_PATH',BASE_DATA_PATH.'/upload');
define('BASE_RESOURCE_PATH',BASE_DATA_PATH.'/resource');

define('CHARSET',$config['db'][1]['dbcharset']);
define('DBDRIVER',$config['dbdriver']);
define('SESSION_EXPIRE',$config['session_expire']);
define('LANG_TYPE',$config['lang_type']);
define('COOKIE_PRE',$config['cookie_pre']);
define('DBPRE',($config['db'][1]['dbname']).'`.`'.($config['tablepre']));

define('APP_KEY',$config['app_key'][APP_ID]);
//统一ACTION
$_GET['act'] = $_GET['act']?strtolower($_GET['act']):($_POST['act']?strtolower($_POST['act']):'index');
$_GET['op'] = $_GET['op']?strtolower($_GET['op']):($_POST['op']?strtolower($_POST['op']):'index');

//对GET POST接收内容进行过滤,$ignore内的下标不被过滤
$ignore = array('article_content','content','sn_content','goods_body','ref_url','adv_pic_url','adv_word_url','adv_slide_url','appcode');
if (!class_exists('Security')) require(BASE_CORE_PATH.'/framework/libraries/security.php');
$_GET = !empty($_GET) ? Security::getAddslashesForInput($_GET,$ignore) : array();
$_POST = !empty($_POST) ? Security::getAddslashesForInput($_POST,$ignore) : array();
$_REQUEST = !empty($_REQUEST) ? Security::getAddslashesForInput($_REQUEST,$ignore) : array();
$_SERVER = !empty($_SERVER) ? Security::AddSlashes($_SERVER) : array();

//启用ZIP压缩
if ($config['gzip'] == 1 && function_exists('ob_gzhandler') && $_GET['inajax'] != 1){
	ob_start('ob_gzhandler');
}else {
	ob_start();
}

require_once(BASE_CORE_PATH.'/framework/function/core.php');
require_once(BASE_CORE_PATH.'/framework/core/base.php');
require_once(BASE_CORE_PATH.'/framework/function/goods.php');

if(function_exists('spl_autoload_register')) {
	spl_autoload_register(array('Base', 'autoload'));
} else {
	function __autoload($class) {
		return Base::autoload($class);
	}
}

?>