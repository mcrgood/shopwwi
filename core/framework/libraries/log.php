<?php
/**
 * 记录日志 
 *
 * @package    library
 * @copyright  Copyright (c) 2007-2013 ShopWWI Inc. (http://www.shopwwi1.com)
 * @license    http://www.shopwwi2.com
 * @link       http://www.shopwwi.com
 * @author	   ShopWWI Team
 * @since      File available since Release v1.1
 */
defined('InByShopWWI') or exit('Access Invalid!');
class Log{

    const SQL       = 'SQL';
    const ERR       = 'ERR';
    private static $log =   array();

    public static function record($message,$level=self::ERR) {
    	$now = date('Y-m-d H:i:s');
    	self::$log[] =   "[{$now}] {$level}: {$message}\r\n";
    }

    public static function read(){
    	return self::$log;
    }
}